Clipjet::Application.routes.draw do

  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin' # Feel free to change '/admin' to any namespace you need.
  mount JasmineRails::Engine => "/specs" if defined?(JasmineRails)
  
  resources :categories, only: :index
  # resources :hit, only: [:create, :update]
  scope "/api" do
    get 'hit/create' => 'hit#create'
    get 'hit/update' => 'hit#update'
    
    # resources :video, only: :show
    get 'videos/show' => 'video#show'

  end
  
  resources :influencer_views, only: [:create, :update]

  resources :sites, only: [] do
    resource :widget, only: :show
  end

  resources :payment_notifications, only: :create

  match 'iv/:id', to: 'influencer_shared_videos#show', via: :get, as: :shared_video

  scope '(:locale)', :locale => /en|es/ do
    devise_for :publishers

    devise_for :advertisers

    devise_for :users, controllers: {registrations: "registrations"}

    root :to => "static#home"

    resources :sites, only: [:new, :create, :index, :destroy] do 
      resources :widget_installs, only: :index
      resources :views, only: :index
      resources :earnings, only: :index
    end

    resources :users, only: :update
    resources :withdrawal_requests, only: :create

    # resources :influencer_videos, only: :index

    match 'influencer/campaigns', to: 'influencer_campaigns#index', via: :get
    # match 'influencer/videos/:id', to: 'influencer_videos#show', via: :get, as: :influencer_video

    resources :influencer_videos, only: [:create, :show, :index]

    resources :finances, only: :index
    resource :campaign_completion, only: :new
    resources :campaigns, only: [:index, :new, :create, :edit, :update] do
      resources :credits_purchases, only: :new
      resources :impressions, only: :index
      member do
        get '/views' => 'campaign_views#index'
        post '/set_payment_params' => 'credits_purchases#params_encryption'
      end
    end
    resource :video_registration, only: [:new, :create]

    get 'welcome' => 'publishers#welcome', as: :publisher_root
    get "promote" => 'video#advertiser_pay', as: :advertiser_root

    get "publishers" => 'static#publishers', as: :publisher
    get "faq" => 'static#faq', as: :faq
    get "influencers" => 'static#influencers', as: :influencer
    
  end
  
  
  
  
end
