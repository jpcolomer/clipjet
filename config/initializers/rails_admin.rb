RailsAdmin.config do |config|
  config.authenticate_with do
    authenticate_or_request_with_http_basic('Log into admin') do |username, password|
      username == ENV['CLIPADMIN'] && password == ENV['ADMIN_PASSWORD']
    end
  end


  config.model Campaign do
    list do
      field :id
      field :name
      field :site_url
      field :video_url
      field :user
      field :balance
      field :video_duration
      field :category
      field :language
      field :created_at
      field :updated_at
      field :impressions_count
      field :influencer_impressions_count
      field :views_count
      field :influencer_views_count
      field :token
    end

    show do
      field :name
      field :site_url
      field :video_url
      field :user
      field :balance
      field :category
      field :language
      field :video_duration
      field :created_at
      field :updated_at
      field :thumbnail
      field :small_thumbnail
      field :impressions_count
      field :influencer_impressions_count
      field :views_count
      field :influencer_views_count
      field :token
    end

    edit do
      field :name
      field :site_url
      field :video_url
      field :user
      field :balance
      field :category
      field :language
      field :video_duration
      field :thumbnail
      field :small_thumbnail
      field :impressions_count
      field :views_count
    end
  end

  config.model Site do
    list do
      field :id
      field :url
      field :user
      field :token
      field :created_at
      field :updated_at
    end

    show do
      field :url
      field :user
      field :token
      field :created_at
      field :updated_at
    end

    edit do
      field :url
      field :user
    end
  end

end