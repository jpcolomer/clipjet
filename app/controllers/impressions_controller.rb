class ImpressionsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @campaign = Campaign.find_by_token(params[:campaign_id])
    @label = t('impressions.index.impressions')
    @stats = @campaign.impression_stats
    @country_stats = @campaign.impression_country_stats
    set_meta_tags title: t('impressions.index.impressions')
  end
end