class SitesController < ApplicationController
  before_filter :authenticate_user!
  def index
    @sites = current_user.sites
    set_meta_tags title: t('sites.index.account_stats').capitalize

  end
  
  def new
    @site = Site.new
    set_meta_tags title: t('sites.new.add_site').capitalize    
  end

  def create
    @site = current_user.sites.new(params[:site])
    if @site.save
      redirect_to site_widget_installs_path(@site)
    else
      render :new
    end
  end

  def destroy
    @site = Site.find_by_token(params[:id])
    if @site.user == current_user
      @site.destroy
      redirect_to sites_path
    else
      redirect_to sites_path, alert: t('.you_can_delete_only_your_sites')
    end
  end
end