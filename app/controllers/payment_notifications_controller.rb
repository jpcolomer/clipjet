class PaymentNotificationsController < ApplicationController

  protect_from_forgery except: :create
  def create
    PaymentNotification.create(params: params, campaign_id: params[:custom], transaction_id: params[:txn_id],
      status: params[:payment_status], amount: params[:mc_gross])
    render nothing: true
  end
end