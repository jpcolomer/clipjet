require './lib/yt_client.rb'
class InfluencerVideosController < ApplicationController
  before_filter :authenticate_user!
  def index
    @influencer_videos = current_user.influencer_videos
    set_meta_tags title: t('influencer_videos.index.shared_videos')
  end

  def show
    @influencer_video = InfluencerVideo.find(params[:id])
    @campaign = @influencer_video.campaign
    @info = YT::get_info(@campaign.video_url)
    @remain_views = @campaign.remaining_views
    set_meta_tags title: "Influencer|#{@campaign.name}"    
  end

  def create
    @influencer_video = InfluencerVideo.new(params[:influencer_video])
    if @influencer_video.save
      redirect_to influencer_video_path(@influencer_video)
    else
      @influencer_video = InfluencerVideo.where(campaign_id: params[:influencer_video][:campaign_id]).where(user_id: params[:influencer_video][:user_id]).first
      redirect_to influencer_video_path(@influencer_video) if @influencer_video
    end
  end

end