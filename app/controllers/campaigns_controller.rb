require './lib/yt_client.rb'
require './lib/paypal.rb'
class CampaignsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @campaigns = current_user.campaigns
    @url = Paypal::PAYPAL_URL
    set_meta_tags title: t('campaigns.index.your_campaigns')
  end
  
  def new
    unless session[:video_url]
      redirect_to new_video_registration_path
    else
      video_url = session.delete(:video_url)
      @info = YT::get_info(video_url)
      @campaign = Campaign.new(video_url: video_url, video_duration: @info.duration, name: @info.title, description: @info.description)
      set_meta_tags title: t('campaigns.new.new_campaign')
    end
  end

  def create
    video_duration = DurationEncryptor.decrypt(params[:video_duration])
    @campaign = current_user.campaigns.new(params[:campaign].merge(video_duration: video_duration))
    if @campaign.save
      redirect_to new_campaign_credits_purchase_path(@campaign)
      # redirect_to Paypal.url(new_campaign_completion_url(campaign: @campaign.token), payment_notifications_url(secret: @campaign.secret), @campaign)
    else
      @info = YT::get_info(@campaign.video_url)
      render :new
    end
  end

  def edit
    @campaign = Campaign.find_by_token(params[:id])
    if @campaign.user == current_user
      @info = YT::get_info(@campaign.video_url)
      set_meta_tags title: t('campaigns.edit.edit_campaign1')
    else
      redirect_to campaigns_path, alert: t('.you_cant_edit_anothers_campaign')
    end
  end

  def update
    @campaign = Campaign.find_by_token(params[:id])
    unless @campaign.user == current_user && @campaign.update_attributes(params[:campaign])
      flash[:alert] = t('campaigns.edit.you_cant_edit_anothers_campaign')
    end
    redirect_to campaigns_path
  end

end