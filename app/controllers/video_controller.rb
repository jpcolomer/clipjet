class VideoController < ApplicationController
  
  #parameters: email, category_id, country_iso
  #return: video_url, advertiser_id
  def show
    remote_ip = request.remote_ip
    @campaign = Campaign.find_video(params.slice(:category_id, :language).merge({ip: remote_ip}))
    site = Site.find_by_token(params[:site_token])
    if @campaign && site
      respond_to do |format|
        site_id = site.id
        @impression = @campaign.impressions.create(site_id: site_id, ip: remote_ip)
        json = { video_url: @campaign.video_url, campaign_token: @campaign.token }.to_json
        format.js { render js: "clipjet.insertVideo(#{json})" }
        format.json { render json: { video_url: @campaign.video_url, campaign_token: @campaign.token } }
      end
      
    else
      render js: "clipjet.success = false"
    end
  end
  

end
