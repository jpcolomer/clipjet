class CategoriesController < ApplicationController
  
  #parameters: none
  #return: categories (with id and name)
  def index
    @categories = Category.all
    render :json => @categories
  end
end
