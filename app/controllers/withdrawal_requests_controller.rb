class WithdrawalRequestsController < ApplicationController
  def create
    @wr = current_user.withdrawal_requests.new(params[:withdrawal_request])
    if @wr.save
      redirect_to finances_path, notice: t('withdrawal_request.create.succes_notice')
    else
      @user = current_user
      render 'finances/index'
    end
  end
end