class CampaignCompletionsController < ApplicationController
	before_filter :authenticate_user!
	def new
		@campaign = Campaign.find_by_token(Rack::Utils.unescape(params[:campaign]))
	end
end