class UsersController < ApplicationController
  before_filter :authenticate_user!

  def update
    @user = current_user
    if @user.update_attributes(params['user'])
      redirect_to finances_path
    else
      @wr = WithdrawalRequest.new
      render 'finances/index'
    end
  end
end