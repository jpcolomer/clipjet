class HitController < ApplicationController

  #params: hit: {site_token, article_url, campaign_token}
  #return: vi.png if success
  
  def create
    user_agent = request.user_agent
    remote_ip = request.remote_ip
    @hit = Hit.new(params[:hit].merge({ip: remote_ip, user_agent: user_agent}))
    if @hit.campaign.try(:id) != 50 && @hit.save ## DELETE @hit.campaign.try(:id) != 50 despues de sacar al wn trucho
      render js:  "clipjet.setToken('#{@hit.token}')" 
      # send_file "#{Rails.root}/public/images/vi.png", disposition: 'inline'
    else
      render js: "clipjet.success = false"
    end
  end
  

  #params: token, hit: {elapsed_time, end_of_video}
  #return: vi.png if success

  def update
    @hit = Hit.find_by_token(params[:token])
    remote_ip = request.remote_ip

    if @hit && @hit.ip == remote_ip && @hit.update_attributes(params[:hit])
      send_file "#{Rails.root}/public/images/vi.png", disposition: 'inline'
    else
      send_file "#{Rails.root}/public/images/vi.png", disposition: 'inline'
    end

  end
end
