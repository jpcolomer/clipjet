class WidgetInstallsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @site = Site.find_by_token(params[:site_id])
	set_meta_tags title: t('widget_installs.index.installation_instructions')
  end
end