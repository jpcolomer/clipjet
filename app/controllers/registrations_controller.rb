class RegistrationsController < Devise::RegistrationsController
  def new
  	session[:previous_url] = request.referer
    super
  end

  protected
  def after_sign_up_path_for(resource)
  	redirect_urls = {
  		root_url => new_video_registration_path,
  		publisher_url => new_site_path,
  		influencer_url => influencer_campaigns_path
  	}
  	
  	redirect_urls[session[:previous_url]] || after_sign_in_path_for(resource)
  end

end