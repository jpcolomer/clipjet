class ViewsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @site = Site.find_by_token(params[:site_id])
    @stats = @site.view_stats
    @label = t('views.index.views')
    @country_stats = @site.hit_country_stats
	set_meta_tags title: t('views.index.site_views').capitalize
  end
end