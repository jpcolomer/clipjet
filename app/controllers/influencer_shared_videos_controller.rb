class InfluencerSharedVideosController < ApplicationController
  layout "shared_video"
  def show
    remote_ip = request.remote_ip
    referer = request.referer
    @influencer_video = InfluencerVideo.find_by_token(params[:id])
    @campaign = @influencer_video.campaign
    @video_id = @campaign.video_id
    @site_domain = URI::parse(@campaign.site_url).host unless @campaign.site_url.blank?
    set_meta_tags og: {
      type:     'website',
      url:      @influencer_video.share_url,
      title:    "#{@campaign.name} - Video",
      site_name: 'ClipJet',
      image:   @campaign.thumbnail,
      description: @campaign.description,
    },
    fb: {
      app_id: FB_CONFIG["app_id"]
    },
    'twitter:card' => 'summary',
    site: @campaign.name
    @influencer_video.influencer_impressions.create(ip: remote_ip, referer: referer)
  end
end