class EarningsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @site = Site.find_by_token(params[:site_id])
    @stats = @site.earning_stats
    @label = t('earnings.index.earnings')
    set_meta_tags title: t('earnings.index.site_earnings').capitalize
  end
end