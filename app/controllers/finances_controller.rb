class FinancesController < ApplicationController
  before_filter :authenticate_user!
  def index
    @user = current_user
    @wr = WithdrawalRequest.new
    set_meta_tags title: t('finances.index.finances').capitalize
  end
end