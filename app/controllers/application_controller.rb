class ApplicationController < ActionController::Base
  protect_from_forgery
	before_filter :set_i18n_locale

  protected 
	def set_i18n_locale

    if params[:locale]
      if I18n.available_locales.include?(params[:locale].to_sym)
  			I18n.locale = params[:locale]
  		else
  			flash.now[:notice] = "#{params[:locale]} translation not available"
  			logger.error flash.now[:notice]
  	   end
    else
      I18n.locale = request.compatible_language_from(I18n.available_locales.map(&:to_s))
    end
	end
  	
  def default_url_options(options={})
    if controller_name == 'influencer_videos' && action_name == 'share'
      {locale: nil}
    else
      {locale: I18n.locale}
    end
  end

  def after_sign_in_path_for(resource)
    return_url = if resource.campaigns.any?
      campaigns_path
    elsif resource.influencer_videos.any?
      influencer_campaigns_path
    elsif resource.sites.any?
      sites_path
    else
      super 
    end
    stored_location_for(:user) || return_url
  end

end
