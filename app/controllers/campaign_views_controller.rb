class CampaignViewsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @campaign = Campaign.find_by_token(params[:id])
    @label = t('campaign_views.index.views')
    @stats = @campaign.hit_stats
    @country_stats = @campaign.hit_country_stats
    set_meta_tags title: t('campaign_views.index.campaign_views').capitalize
  end
end