require './lib/yt_client.rb'
class WidgetsController < ApplicationController
  layout false
  def show
    ## REMOVE ALL campaign.id == 50 after deleting the trucho guy
    category_id = params[:category] || params[:category_id]
    language = params[:lang] || params[:language]
    remote_ip = request.remote_ip
    # @campaign = Campaign.find_video(category_id: category_id, language: language, ip: remote_ip)
    @site = Site.find_by_token(params[:site_id])

    if @site.user.last_sign_in_ip == '82.158.190.57'
      @campaign = Campaign.find(50)
    else
      @campaign = Campaign.find_video(category_id: category_id, language: language, ip: remote_ip)
      while @campaign.id == 50
        @campaign = Campaign.with_credit.where("campaigns.id != 50").random.first
      end
    end

    if @campaign && @site
      @width = params[:width]
      @height = params[:height]
      impression = @campaign.impressions.create(site_id: @site.id, ip: remote_ip) unless @campaign.id == 50
      @video_id = YT::get_id(@campaign.video_url)
    end
  end
end