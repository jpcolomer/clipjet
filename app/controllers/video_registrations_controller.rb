class VideoRegistrationsController < ApplicationController
  before_filter :authenticate_user!, only: :new
  def new
    set_meta_tags title: t('video_registrations.new.add_video').capitalize
  end

  def create
    if session[:video_url] = params[:video_url].match(/.+youtube\.com\/watch\?v=.+/)
      session[:video_url] = session[:video_url][0]
      redirect_to new_campaign_path
    else
      flash[:error] = t('video_registrations.create.video_url_invalid_format')
      redirect_to new_video_registration_path
    end
  end
end