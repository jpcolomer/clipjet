class StaticController < ApplicationController
  def home
  end

  def publishers
    set_meta_tags title: 'Publishers'
  end
  
  def influencers
    set_meta_tags title: 'Influencers'
  end

  def faq
    @faqs_hash = {1 => 'One', 2 => 'Two', 3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six', 7 => 'Seven', 8 => 'Eight', 9 => 'Nine', 10 => 'Ten', 11 => 'Eleven'}
  end
end
