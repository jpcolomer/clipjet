class InfluencerViewsController < ApplicationController
  def create
    user_agent = request.user_agent
    remote_ip = request.remote_ip
    referer = request.referer
    @influencer_view = InfluencerView.new(params[:influencer_view].merge(ip: remote_ip, user_agent: user_agent, referer: referer))
    if @influencer_view.save
      render json: {success: true, token: @influencer_view.token}
    else
      render json: {success: false}
    end
  end

  def update
    remote_ip = request.remote_ip
    influencer_view = InfluencerView.find_by_token(params[:id])
    if influencer_view && influencer_view.ip == remote_ip && influencer_view.update_attributes(params[:influencer_view].slice(:elapsed_time, :end_of_video))
      render json: {success: true}
    else
      render json: {success: false}
    end
  end
end