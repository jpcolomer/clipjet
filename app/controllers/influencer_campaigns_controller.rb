class InfluencerCampaignsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @campaigns = Campaign.with_enough_balance
    @influencer_video = InfluencerVideo.new
    set_meta_tags title: t('influencer_campaigns.index.influencer_campaigns')
  end
end