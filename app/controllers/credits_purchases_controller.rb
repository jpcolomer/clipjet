class CreditsPurchasesController < ApplicationController
  def new
    @campaign = Campaign.find_by_token(params[:campaign_id])
    set_meta_tags title: t('credits_purchases.new.buy_credits')
  end

  def params_encryption
    @campaign = Campaign.find_by_token(params[:id])
    @return_url = new_campaign_completion_url(campaign: @campaign.token)

    ## move this to a helper model
    budget = params[:budget].to_f
    @hit_cost = params[:hit_cost].to_f
    if @hit_cost >= Campaign::HIT_COST
      @views = (budget / @hit_cost).to_i 
      @budget = (@views * @hit_cost * 100).ceil/100.0
      if 20 - @budget > 0
        @views += 1
        @budget = (@views * @hit_cost * 100).ceil/100.0
      end
    end
  end
end