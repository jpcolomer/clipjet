class AdminMailer < ActionMailer::Base
  default from: "withdrawal@clipjet.me"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.admin_mailer.withdraw_mailer.subject
  #
  def withdrawal_request(withdrawal)
    @withdrawal = withdrawal

    mail to: "withdrawal@clipjet.me"
  end
end
