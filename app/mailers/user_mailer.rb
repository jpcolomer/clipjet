class UserMailer < ActionMailer::Base
  default from: "withdrawal@clipjet.me"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.withdraw_request.subject
  #
  def withdrawal_request(withdrawal)
    @user = withdrawal.user
    @withdrawal = withdrawal
    mail to: @user.paypal_email, subject: "ClipJet withdrawal request"
  end
end
