$('#div_category_id').change ->
  category = $("#div_category_id option:selected").val()
  str = $('pre.widget_div').text().replace(/category=\d+/, "category=#{category}")
  $('pre.widget_div').text(str)

$('#div_language').change ->
  lang = $("#div_language option:selected").val()
  str = $('pre.widget_div').text().replace(/lang=[a-z]{2}/, "lang=#{lang}")
  $('pre.widget_div').text(str)

$("input#width").on('change keyup',  ->
  value = $(this).val()
  if value.match(/^\d{3,4}$/) && parseInt(value.replace(/px/,'')) >= 200 
    value = parseInt(value.replace(/px/,''))
    str = $('pre.widget_div').text().replace(/\?width=\d{3,4}/, "?width=#{value}")
    $('pre.widget_div').text(str)
    str = $('pre.widget_div').text().replace(/width="\d{3,4}"/, "width=\"#{value}\"")
    $('pre.widget_div').text(str)
  )

$("input#height").on('change keyup', ->
  value = $(this).val()
  if value.match(/^\d{3,4}$/) && parseInt(value.replace(/px/,'')) >= 200 
    value = parseInt(value.replace(/px/,''))
    str = $('pre.widget_div').text().replace(/&height=\d{3,4}/, "&height=#{value}")
    $('pre.widget_div').text(str)
    str = $('pre.widget_div').text().replace(/height="\d{3,4}"/, "height=\"#{value+30}\"")
    $('pre.widget_div').text(str)
  )