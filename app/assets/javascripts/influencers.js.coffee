$('.small-thumbnail .preview a').click ->
  videoId = $(this).data('videoid')
  name = $(this).data('name')
  $('.modal-body').append("<iframe id=\"player\" type=\"text/html\" width=\"530\" height=\"300\"
  src=\"http://www.youtube.com/embed/#{videoId}?autohide=1&showinfo=0\"  frameborder=\"0\"></iframe>")

  $('#previewModalLabel').text(name)

$('#previewModal').on('hidden', ->
  $('#player').remove()
)
