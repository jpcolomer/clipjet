jQuery ->
  Morris.Line
    element: 'stats_chart'
    data: $('#stats_chart').data('stats')
    xkey: 'created_at'
    ykeys: ['value']
    labels: [$('#stats_chart').data('label')]
    parseTime: true
    xLabels: 'day'

  Morris.Donut
    element: 'country_pie'
    data: $('#country_pie').data('stats')
    formatter: (y) ->
      "#{y}%"