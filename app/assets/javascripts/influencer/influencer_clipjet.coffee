window.InfluencerClipjet = class InfluencerClipjet
  constructor: ->
    @videoID = @readVideoID()
    @influencerVideo = @readInfluencerVideoToken()
    @started = false
    @ended = false

  init: ->
    @initPlayer()

  initPlayer: ->
    s = document.createElement("script")
    s.src = "#{if location.protocol == 'https:' then 'https' else 'http'}://www.youtube.com/iframe_api"
    before = document.getElementsByTagName("script")[0]
    before.parentNode.insertBefore(s, before)

  readVideoID: ->
    $('#clipjet-video').data('video')

  readInfluencerVideoToken: ->
    $('#clipjet-video').data('influencer-video')

  setMinElapsedTime: ->
    if @player.getDuration() < 30 then @minElapsedTime = @player.getDuration() else @minElapsedTime = 30

  checkVideoStatus: (event) =>
    switch event.data
      when 1
        unless @started
          @started = true
          @notifyVideoStarted()
          @setVideoUpdateInterval()
      when 0
        unless @ended
          @ended = true
          @notifyVideoUpdate()

  notifyVideoStarted: ->
    $.ajax 
      type: 'POST'
      url: '/influencer_views'
      dataType: 'json'
      data: 
        influencer_view:
          influencer_video_token: @influencerVideo
      success: @setToken

  notifyVideoUpdate: ->
    if @success
      $.ajax
        type: 'PUT'
        url: "/influencer_views/#{@token}"
        dataType: 'json'
        data:
          influencer_view:
            end_of_video: if @ended == false then 0 else 1
            elapsed_time: @player.getCurrentTime()


  setVideoUpdateInterval: ->
    @intervalID = setInterval(@checkVideoTime, 500)

  checkVideoTime: =>
    @setMinElapsedTime unless @minElapsedTime > 0
    if @player.getPlayerState() == 1 and @minElapsedTime > 0 and @player.getCurrentTime() > @minElapsedTime
      @notifyVideoUpdate()
      clearInterval(@intervalID)

  setToken: (json) =>
    @success = json.success
    @token = json.token if json.success

  onYTReady: =>
    @setMinElapsedTime()

window.onYouTubeIframeAPIReady = ->
  clipjet.player = new YT.Player('player',
      width: 700
      height: 400
      videoId: clipjet.videoID
      playerVars: 
        wmode: "opaque"
        autohide: 1 
        showinfo: 0
      events:
        onStateChange: clipjet.checkVideoStatus
        onReady: clipjet.onYTReady
    )

initClipjet = ->
  window.clipjet = new InfluencerClipjet()
  clipjet.init()


$(document).ready ->
  initClipjet()