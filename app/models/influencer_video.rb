class InfluencerVideo < ActiveRecord::Base
  include TokenHelpers
  attr_accessible :campaign_id, :token, :user_id
  belongs_to :user
  belongs_to :campaign

  has_many :influencer_views, foreign_key: 'influencer_video_token', primary_key: 'token'
  has_many :influencer_impressions

  validates :user, :campaign, presence: true
  validates :campaign_id, uniqueness: {scope: :user_id}

  delegate :url_helpers, to: 'Rails.application.routes' 
  delegate :wallet, to: :user, prefix: true

  def share_url
    url_helpers.shared_video_url(id: self.token, host: HOST_CONFIG["host"]).gsub(/\?locale.+/, '') + "/"
  end

  def increase_view_count
    InfluencerVideo.increment_counter('views_count', self.id)
  end

  def earnings
    influencer_views.with_minimum_campaign_time.sum(:view_cost)
  end
end