require './lib/geoip_wrap.rb'
class Hit < ActiveRecord::Base
  SITE_PERCENTAGE = 0.7
  include TokenHelpers
  include CountryMod
  ## ERASE ADVERTISER, AND PUBLISHER IDS AFTER INTEGRATION
  attr_accessible :advertiser_id, :publisher_id, :end_of_video, :ip, :elapsed_time, :article_url, :campaign_token, :site_token, :user_agent

  belongs_to :campaign, foreign_key: "campaign_token", primary_key: 'token'
  belongs_to :site, foreign_key: "site_token", primary_key: 'token'

  validates :campaign, presence: true
  validates :site, presence: true
  validates :token, presence: true, uniqueness: true
  validates :article_url, presence: true
  validates :ip, uniqueness: {scope: :site_token}, if: :last_hit_less_than_1_hour?, on: :create #### UNCOMMENT BEFORE DEPLOY TO PRODUCTION
  # validates :ip, uniqueness: {scope: :campaign_token}, if: :last_hit_has_minimum_time?,on: :create #### UNCOMMENT BEFORE DEPLOY TO PRODUCTION
  validates :ip, presence: true
  validates :elapsed_time, presence: true, numericality: true, on: :update
  validates :view_cost, presence: true, numericality: true

  validates_each :ip, :token, :site_token, :article_url, :campaign_token, :user_agent, unless: lambda {|hit| hit.new_record? } do |record, attr, value|
    record.errors.add(attr, "can't touch this") if record.send("#{attr}_changed?")
  end

  validates_with CampaignIPUniquenessValidator, on: :create

  before_validation :set_view_cost, if: lambda{|record| record.new_record?}
  before_save :preset_end_of_video

  after_update :charge_pay_update_count, if: lambda{|hit| hit.elapsed_time_was.nil? && hit.elapsed_time >= hit.campaign.minimum_time} ## REFACTOR THIS TO A PORO FOR SINGLE RESPONSABILITY 

  scope :with_minimum_time, lambda{|time| where('elapsed_time >= ?', time)}
  scope :valid, where('elapsed_time is not null')
  scope :with_minimum_campaign_time, joins(:campaign).where("(campaigns.video_duration >= 30 AND hits.elapsed_time >= 30) OR (hits.elapsed_time >= campaigns.video_duration)")

  def charge_pay_update_count
    update_count if charge_pay_balance
  end

  def update_count
    campaign.views_count += 1
    campaign.save
    site.views_count += 1
    site.save
  end

  def charge_pay_balance
    increase_user_credit if reduce_campaign_balance
  end

  def self.maximum(arg, options = {})
    super(arg, options) || 0
  end

  def site_owner_wallet
    site.user.wallet
  end

  def reduce_campaign_balance
    self.campaign.reduce_balance
  end

  def increase_user_credit
    site_owner_wallet.increase_balance(campaign.hit_cost*SITE_PERCENTAGE)
  end

  def last_hit_less_than_1_hour?
    Hit.with_minimum_campaign_time.where('hits.created_at > ?', 1.hour.ago).where(site_token: site_token).any?
  end

  def last_hit_has_minimum_time?
    Hit.with_minimum_campaign_time.where(campaign_token: campaign_token).any?
  end

  def preset_end_of_video
    end_of_video = end_of_video.to_i == 1
    true
  end

  private
  def set_view_cost
    self.view_cost = self.campaign.try(:hit_cost).try(:*, SITE_PERCENTAGE) 
    true
  end

  # validate :new_ip

  # def self.create(options={})
  #   publisher_email = options.delete :email
  #   advertiser_id = options.delete :advertiser_id

  #   publisher = Publisher.find_by_email(publisher_email)
  #   advertiser = Advertiser.find_by_id(advertiser_id)

  #   if advertiser && publisher
  #     options = options.select{|k,v| [:token, :article_url, :ip].include?(k.to_sym)}
  #     options.merge!(advertiser_id: advertiser.id, publisher_id: publisher.id, end_of_video: false)
  #     super(options)
  #   end
  # end

end
