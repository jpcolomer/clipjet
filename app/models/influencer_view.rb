class InfluencerView < ActiveRecord::Base
  include TokenHelpers
  include CountryMod

  INFLUENCER_PERCENTAGE = 0.7

  attr_accessible :country, :elapsed_time, :end_of_video, :influencer_video_token, :ip, :user_agent, :referer 
  validates :ip, :influencer_video, presence: true
  validates :elapsed_time, presence: true, numericality: true, on: :update
  validates :view_cost, presence: true, numericality: true

  validates_each :ip, :token, :influencer_video_token, :user_agent, :referer, unless: lambda {|record| record.new_record? } do |record, attr, value|
    record.errors.add(attr, "can't touch this") if record.send("#{attr}_changed?")
  end

  validates_with CampaignIPUniquenessValidator, on: :create

  belongs_to :influencer_video, foreign_key: "influencer_video_token", primary_key: 'token'
  has_one :campaign, through: :influencer_video

  after_update :charge_pay_update_count, if: lambda{|record| record.elapsed_time_was.nil? && (record.elapsed_time >= 30 || record.elapsed_time >= record.campaign.video_duration)} ## REFACTOR THIS TO A PORO FOR SINGLE RESPONSABILITY 
  before_validation :set_view_cost, if: lambda{|record| record.new_record?}

  scope :with_minimum_campaign_time, joins(:campaign).where("(campaigns.video_duration >= 30 AND influencer_views.elapsed_time >= 30) OR (influencer_views.elapsed_time >= campaigns.video_duration)")

  def charge_pay_update_count
    update_count if charge_pay_balance
  end

  def charge_pay_balance
    increase_influencer_balance if reduce_campaign_balance
  end


  def reduce_campaign_balance
    campaign.reduce_balance
  end

  def increase_influencer_balance
    influencer_wallet.increase_balance(view_cost)
  end

  def influencer_wallet 
    influencer_video.user_wallet
  end

  def update_count
    campaign.increase_influencer_views_count
    influencer_video.increase_view_count
  end

  private
  def set_view_cost
    self.view_cost = self.campaign.try(:hit_cost).try(:*, INFLUENCER_PERCENTAGE) 
    true
  end
end