  class Advertiser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,
  :name, :url, :video_url, :country_id, :category_id
  # attr_accessible :title, :body
  validates_presence_of :category_id
  
  validates :video_url, :format => { :with => /(^$|youtube\.com\/watch\?v=.+)/,
    :message => "Please enter a valid youtube url" }

  belongs_to :country
  belongs_to :category

  has_many :hits
  
  scope :for_country, lambda { |country|
    where(:country_id => country.id)
  }
  
  scope :for_category, lambda { |category|
    where(:category_id => category.id)
  }  
  
  scope :has_video, where('advertisers.video_url is not null AND advertisers.video_url != ?', '')

  scope :random, order("RANDOM()")

  def self.find_video(country_iso, category_id)
    return nil if country_iso.nil? || category_id.nil?
    country = Country.find_by_iso(country_iso.upcase)
    category = Category.find_by_id(category_id)
    advertiser = nil
    if country && category
      advertiser = Advertiser.has_video.for_category(category).random.first
      unless advertiser
        categories = Category.name_like(category.name)
        if categories.any?
          advertiser = Advertiser.has_video.where(category_id: categories.collect(&:id)).random.first
        end
      end
      unless advertiser
        advertiser = Advertiser.has_video.random.first
      end
    end
    return advertiser
  end
  
end
