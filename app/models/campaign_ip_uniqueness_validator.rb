class CampaignIPUniquenessValidator < ActiveModel::Validator
  def validate(record)
    if record.campaign && (record.campaign.influencer_views.where(ip: record.ip).where("elapsed_time >= 30 OR elapsed_time >= ?", record.campaign.video_duration).any? || record.campaign.hits.where(ip: record.ip).with_minimum_campaign_time.any?)
      record.errors.add(:campaign, "can't be duplicated for one ip")
    end
  end
end