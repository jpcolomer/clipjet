class Country < ActiveRecord::Base
  attr_accessible :name, :iso
  has_many :advertisers
  
  scope :name_order, order('name ASC')
end
