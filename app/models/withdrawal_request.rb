class WithdrawalRequest < ActiveRecord::Base
  attr_accessible :amount, :user_id

  belongs_to :user
  validates :user, presence: true
  validates  :amount, presence: true, numericality: {less_than_or_equal_to: lambda{|wq| wq.user.wallet_balance}, greater_than_or_equal_to: 20}

  validate :user_has_paypal_email


  after_create :deliver_notification_mails

  private
  def deliver_notification_mails
    UserMailer.withdrawal_request(self).deliver
    AdminMailer.withdrawal_request(self).deliver
  end

  def user_has_paypal_email
    unless self.user.paypal_email
      errors.add(:user, "can't withdraw without saving your paypal email")
    end
  end

end
