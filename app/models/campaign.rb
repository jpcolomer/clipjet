require './lib/yt_client.rb'
class Campaign < ActiveRecord::Base
  HIT_COST = 80.0/1000
  #
  LANGS = LANGUAGES.map{|lang| lang[1]}
  include TokenHelpers
  include StatsHelpers
  attr_accessible :category_id, :site_url, :user_id, :video_duration, :video_url, :name, :language, :thumbnail, :small_thumbnail, :yt_count, :description
  belongs_to :user
  belongs_to :category
  has_many :hits, foreign_key: 'campaign_token', primary_key: 'token'
  has_many :impressions
  has_many :influencer_videos
  has_many :influencer_views, through: :influencer_videos

  validates :name, presence: true
  validates :user, presence: true
  validates :video_url, presence: true, format: { with: /youtube\.com\/watch\?v=.+/,
    message: "Please enter a valid youtube url" }
  validates :site_url, format: URI::regexp(%w(http https)), allow_blank: true
  validates :balance, numericality: {greater_than_or_equal_to: 0}
  validates :category, presence: true
  validates :video_duration, presence: true, numericality: true
  validates :hit_cost, presence:true, numericality: {greater_than_or_equal_to: HIT_COST}
  validates :language, presence: true, inclusion: {in: LANGS}

  scope :random, order("RANDOM()")
  scope :with_enough_balance, where('campaigns.balance >= campaigns.hit_cost')
  scope :with_lang, lambda{|lang| where(language: lang)}
  scope :without_ip, lambda{|ip| where("NOT EXISTS (SELECT * FROM hits WHERE hits.campaign_token = campaigns.token AND hits.ip = ?)", ip)} #joins("LEFT OUTER JOIN hits ON campaigns.token = hits.campaign_token").where("hits.ip is null OR hits.ip != ?", ip)}

  before_validation :set_hit_cost, if: lambda {|campaign| campaign.new_record?}
  before_create :set_secret


  def set_secret
    self.secret = SecureRandom.urlsafe_base64(8)
  end

  def set_hit_cost
    self.hit_cost = HIT_COST
  end

  def minimum_time
    duration = video_duration
    duration >= 30 ? 30 : duration
  end

  def views
    hits.with_minimum_time(minimum_time).count
  end

  def reduce_balance
    Campaign.with_enough_balance.update_counters(self.id, balance: -self.hit_cost) != 0
  end

  def to_param
    token
  end

  def impression_stats
    chart_stats_for(impressions, 'impressions') do |records|
      records.count
    end
  end

  def hit_stats
    chart_stats_for(hits.with_minimum_time(minimum_time), 'hits') do |records|
      records.count
    end
  end

  def increase_influencer_views_count
    Campaign.increment_counter('influencer_views_count', self.id)
  end

  def remaining_views
    (self.balance / self.hit_cost).to_i
  end

  def influencer_payment
    "#{(InfluencerView::INFLUENCER_PERCENTAGE * hit_cost * 100).round(1)} &cent;"
  end

  def hit_country_stats
    country_stats do |o|
      o.count = views_count
      o.records = hits.with_minimum_time(minimum_time).country_stats
    end
  end

  def impression_country_stats
    country_stats do |o|
      o.count = impressions_count
      o.records = impressions.country_stats
    end
  end

  def total_impressions
    self.influencer_impressions_count + self.impressions_count
  end

  def total_views
    self.influencer_views_count + self.views_count
  end

  def video_id
    YT::get_id(video_url)
  end

  class << self
  	def find_video(args)
      category_id = args[:category_id]
      language = args[:language]
      ip = args[:ip]
  		category = Category.find_by_id(category_id)
  		campaign = category.get_campaign(language, ip) if category
  		campaign ||= Campaign.with_enough_balance.random.first
      campaign
  	end
  end


end
