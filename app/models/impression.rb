require './lib/geoip_wrap.rb'
class Impression < ActiveRecord::Base
  include CountryMod
  attr_accessible :campaign_id, :site_id, :ip, :country

  belongs_to :campaign, counter_cache: true
  belongs_to :site

  validates :campaign, :site, :ip, presence: true


end
