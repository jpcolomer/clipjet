class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :paypal_email
  # attr_accessible :title, :body
  has_many :campaigns
  has_many :sites
  has_many :withdrawal_requests
  has_many :influencer_videos
  has_one :wallet

  validates :paypal_email, format: {with: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i}, allow_nil: true, on: :update

  delegate :balance, to: :wallet, prefix: true
  before_create :build_default_wallet

  scope :publishers, where("EXISTS (SELECT * FROM sites WHERE sites.user_id = users.id)")
  scope :promoters, where("EXISTS (SELECT * FROM campaigns RIGHT OUTER JOIN payment_notifications ON campaigns.id = payment_notifications.campaign_id WHERE campaigns.user_id = users.id)")
  scope :influencers, where("EXISTS (SELECT * FROM influencer_videos WHERE influencer_videos.user_id = users.id)")

  private
  def build_default_wallet
    build_wallet
    true
  end

end
