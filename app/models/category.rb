class Category < ActiveRecord::Base
  attr_accessible :name
  has_many :campaigns
  translates :name
  scope :name_order, order('name ASC')

  def self.name_like(category_name)
  	category_name = category_name.gsub(/.+-/i).first
  	if category_name 
  		category_name = category_name.sub(/.-/,'')
  		where('name like ?', "%#{category_name}%")
  	end
  end

  def get_campaign(lang, ip)
    if campaigns.without_ip(ip).with_enough_balance.with_lang(lang).any?
      campaigns.without_ip(ip).with_enough_balance.with_lang(lang).random.first
    elsif Campaign.without_ip(ip).with_enough_balance.with_lang(lang).where(category_id: similar_categories).any?
      Campaign.without_ip(ip).with_enough_balance.with_lang(lang).where(category_id: similar_categories).random.first
    elsif Campaign.without_ip(ip).with_enough_balance.with_lang(lang).any?      
      Campaign.without_ip(ip).with_enough_balance.with_lang(lang).random.first
    elsif campaigns.without_ip(ip).with_enough_balance.any?
      campaigns.without_ip(ip).with_enough_balance.random.first
    elsif Campaign.without_ip(ip).with_enough_balance.where(category_id: similar_categories).any?
      Campaign.without_ip(ip).with_enough_balance.where(category_id: similar_categories).random.first
    else
      Campaign.without_ip(ip).with_enough_balance.random.first
    end
  end

  def similar_categories
    Category.name_like(name).pluck(:id)
  end
end
