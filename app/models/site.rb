class Site < ActiveRecord::Base
  include TokenHelpers
  include StatsHelpers
  attr_accessible :url, :user_id

  belongs_to :user
  has_many :hits, foreign_key: 'site_token', primary_key: 'token'
  has_many :impressions

  validates :url, presence: true, format: URI::regexp(%w(http https))
  validates :user_id, presence: true

  def widget_div(args)
    "<div id=\"clipjet-video\" data-lang=\"#{args[:lang]}\" data-category=\"#{args[:category]}\" width=\"#{args[:width]}px\" height=\"#{args[:height]}px\" data-site-token=\"#{token}\"></div>"
  end

  def widget_script
    '''<script type="text/javascript">
  (function() {
    var cj = document.createElement("script"); cj.type = "text/javascript"; cj.async = true;
    cj.src = "http://widget.clipjet.me/clipjet.js";
    var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(cj, s);
  })();
</script>'''
  end

  def widget_iframe(args)
    "<iframe src=\"//#{API_CONFIG['host']}/sites/#{token}/widget?width=#{args[:width]}&height=#{args[:height]}&lang=#{args[:lang]}&category=#{args[:category]}\" width=\"#{args[:width]}\" height=\"#{args[:height].to_i+30}\" frameborder=\"0\" scrolling=\"no\"></iframe>"
  end

  def to_param
    token
  end

  def views
    hits.valid.with_minimum_campaign_time.count
  end

  def balance
    hits.valid.with_minimum_campaign_time.sum("campaigns.hit_cost").to_f*Hit::SITE_PERCENTAGE
  end

  def earnings
    (hits.with_minimum_campaign_time.sum(:view_cost)).to_f.round(3)
  end

  def view_stats
    chart_stats_for(hits.with_minimum_campaign_time, 'hits') do |records|
      records.count
    end
  end

  def earning_stats
    chart_stats_for(hits.with_minimum_campaign_time, 'hits') do |records|
      records.sum(:view_cost)
    end
  end

  def hit_country_stats
    country_stats do |o|
      o.count = views_count
      o.records = hits.with_minimum_campaign_time.country_stats
    end
  end
end
