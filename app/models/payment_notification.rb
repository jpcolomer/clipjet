class PaymentNotification < ActiveRecord::Base

  attr_accessible :campaign_id, :params, :status, :transaction_id, :amount
  serialize :params
  belongs_to :campaign
  validates :campaign, presence: true
  validates :status, presence: true
  validates :params, presence: true
  validates :transaction_id, presence: true
  validates :amount, presence: true

  after_create :increase_campaign_balance

  def increase_campaign_balance
    if status == 'Completed' && params[:secret] == campaign.secret && amount >= 20
      campaign.balance += amount
      campaign.hit_cost = params[:hit_cost] || campaign.hit_cost if params[:hit_cost].to_f >= 0.08
      campaign.save!
    end
  end
end
