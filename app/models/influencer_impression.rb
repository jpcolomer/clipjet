require './lib/geoip_wrap.rb'
class InfluencerImpression < ActiveRecord::Base
  include CountryMod
  attr_accessible :country, :influencer_video_id, :ip, :referer

  belongs_to :influencer_video, counter_cache: :impressions_count
  has_one :campaign, through: :influencer_video

  validates :influencer_video, :ip, presence: true

  after_create :increment_campaign_counter


  def increment_campaign_counter
    Campaign.increment_counter('influencer_impressions_count', self.campaign.id)
  end
end
