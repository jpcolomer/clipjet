class Wallet < ActiveRecord::Base
  attr_accessible :user_id

  belongs_to :user

  validates :user_id, presence: true
  validates :balance, presence: true, numericality: {greater_than_or_equal_to: 0}

  def increase_balance(amount)
    Wallet.update_counters(self.id, balance: amount)
    # self.balance += amount
    # save
  end
end
