module ApplicationHelper
  def current_controller?(c)
    controller.controller_name == c
  end

  def promoter_controller?
    ['campaigns', 'video_registrations', 'campaign_completions', 'credits_purchases', 'impressions', 'campaign_views'].include? controller.controller_name
  end

  def publisher_controller?
    ['sites', 'widget_installs', 'finances', 'views', 'earnings'].include? controller.controller_name
  end

  def influencer_controller?
    ['influencer_videos', 'influencer_campaigns'].include? controller.controller_name
  end
end
