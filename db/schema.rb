# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130814162430) do

  create_table "advertisers", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "name"
    t.string   "url"
    t.string   "video_url"
    t.integer  "country_id"
    t.integer  "category_id"
  end

  add_index "advertisers", ["category_id"], :name => "index_advertisers_on_category_id"
  add_index "advertisers", ["country_id"], :name => "index_advertisers_on_country_id"
  add_index "advertisers", ["email"], :name => "index_advertisers_on_email", :unique => true
  add_index "advertisers", ["reset_password_token"], :name => "index_advertisers_on_reset_password_token", :unique => true

  create_table "articles", :force => true do |t|
    t.string   "url"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "articles", ["user_id"], :name => "index_articles_on_user_id"

  create_table "campaigns", :force => true do |t|
    t.string   "site_url"
    t.string   "video_url"
    t.integer  "credit"
    t.integer  "video_duration"
    t.integer  "category_id"
    t.integer  "user_id"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.string   "token"
    t.decimal  "hit_cost"
    t.string   "name"
    t.string   "secret"
    t.string   "thumbnail"
    t.string   "small_thumbnail"
    t.string   "language"
    t.integer  "impressions_count",            :default => 0
    t.integer  "views_count",                  :default => 0
    t.decimal  "balance",                      :default => 0.0
    t.integer  "yt_count"
    t.text     "description"
    t.integer  "influencer_views_count",       :default => 0
    t.integer  "influencer_impressions_count", :default => 0
  end

  add_index "campaigns", ["category_id"], :name => "index_campaigns_on_category_id"
  add_index "campaigns", ["language"], :name => "index_campaigns_on_language"
  add_index "campaigns", ["token"], :name => "index_campaigns_on_token"
  add_index "campaigns", ["user_id"], :name => "index_campaigns_on_user_id"
  add_index "campaigns", ["video_duration"], :name => "index_campaigns_on_video_duration"

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "category_translations", :force => true do |t|
    t.integer  "category_id"
    t.string   "locale"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "category_translations", ["category_id"], :name => "index_category_translations_on_category_id"
  add_index "category_translations", ["locale"], :name => "index_category_translations_on_locale"

  create_table "countries", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "iso"
  end

  add_index "countries", ["iso"], :name => "index_countries_on_iso"

  create_table "hits", :force => true do |t|
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.boolean  "end_of_video"
    t.string   "token"
    t.decimal  "elapsed_time"
    t.string   "ip"
    t.string   "article_url"
    t.string   "site_token"
    t.string   "campaign_token"
    t.string   "country"
    t.decimal  "view_cost"
    t.string   "user_agent"
  end

  add_index "hits", ["campaign_token"], :name => "index_hits_on_campaign_token"
  add_index "hits", ["elapsed_time"], :name => "index_hits_on_elapsed_time"
  add_index "hits", ["site_token"], :name => "index_hits_on_blog_token"

  create_table "impressions", :force => true do |t|
    t.integer  "campaign_id"
    t.integer  "site_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "ip"
    t.string   "country"
  end

  add_index "impressions", ["campaign_id"], :name => "index_impressions_on_campaign_id"
  add_index "impressions", ["site_id"], :name => "index_impressions_on_site_id"

  create_table "influencer_impressions", :force => true do |t|
    t.string   "ip"
    t.string   "country"
    t.integer  "influencer_video_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "referer"
  end

  add_index "influencer_impressions", ["influencer_video_id"], :name => "index_influencer_impressions_on_influencer_video_id"

  create_table "influencer_videos", :force => true do |t|
    t.string   "token"
    t.integer  "user_id"
    t.integer  "campaign_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.integer  "views_count",       :default => 0
    t.integer  "impressions_count", :default => 0
  end

  add_index "influencer_videos", ["campaign_id"], :name => "index_influencer_videos_on_campaign_id"
  add_index "influencer_videos", ["token"], :name => "index_influencer_videos_on_token"
  add_index "influencer_videos", ["user_id"], :name => "index_influencer_videos_on_user_id"

  create_table "influencer_views", :force => true do |t|
    t.string   "ip"
    t.string   "token"
    t.string   "country"
    t.string   "influencer_video_token"
    t.decimal  "elapsed_time"
    t.boolean  "end_of_video"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.decimal  "view_cost"
    t.string   "user_agent"
    t.string   "referer"
  end

  add_index "influencer_views", ["influencer_video_token"], :name => "index_influencer_views_on_influencer_video_token"
  add_index "influencer_views", ["token"], :name => "index_influencer_views_on_token"

  create_table "payment_notifications", :force => true do |t|
    t.text     "params"
    t.string   "status"
    t.string   "transaction_id"
    t.integer  "campaign_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.decimal  "amount"
  end

  create_table "publishers", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "blog_url"
  end

  add_index "publishers", ["email"], :name => "index_publishers_on_email", :unique => true
  add_index "publishers", ["reset_password_token"], :name => "index_publishers_on_reset_password_token", :unique => true

  create_table "sites", :force => true do |t|
    t.string   "url"
    t.integer  "user_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.string   "token"
    t.integer  "views_count", :default => 0
  end

  add_index "sites", ["token"], :name => "index_blogs_on_token"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "paypal_email"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "wallets", :force => true do |t|
    t.decimal  "balance",    :default => 0.0
    t.integer  "user_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  add_index "wallets", ["user_id"], :name => "index_wallets_on_user_id"

  create_table "withdrawal_requests", :force => true do |t|
    t.integer  "user_id"
    t.decimal  "amount"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "withdrawal_requests", ["user_id"], :name => "index_withdrawal_requests_on_user_id"

end
