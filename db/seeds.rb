# -*- coding: utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
# #   Mayor.create(name: 'Emanuel', city: cities.first)
# Country.delete_all
# Category.delete_all

countries = {
  :AD => "Andorra",
  :AF => "Afghanistan",
  :AG => "Antigua and Barbuda",
  :AI => "Anguilla",
  :AL => "Albania",
  :AM => "Armenia",
  :AN => "Netherlands Antilles",
  :AO => "Angola",
  :AQ => "Antarctica",
  :AR => "Argentina",
  :AS => "American Samoa",
  :AT => "Austria",
  :AU => "Australia",
  :AW => "Aruba",
  :AX => "Aland Islands",
  :AZ => "Azerbaijan",
  :BA => "Bosnia and Herzegovina",
  :BB => "Barbados",
  :BD => "Bangladesh",
  :BE => "Belgium",
  :BF => "Burkina Faso",
  :BG => "Bulgaria",
  :BH => "Bahrain",
  :BI => "Burundi",
  :BJ => "Benin",
  :BL => "Saint Barthélemy",
  :BM => "Bermuda",
  :BN => "Brunei",
  :BO => "Bolivia",
  :BQ => "British Antarctic Territory",
  :BR => "Brazil",
  :BS => "Bahamas",
  :BT => "Bhutan",
  :BV => "Bouvet Island",
  :BW => "Botswana",
  :BY => "Belarus",
  :BZ => "Belize",
  :CA => "Canada",
  :CC => "Cocos Islands",
  :CD => "Congo - Kinshasa",
  :CF => "Central African Republic",
  :CG => "Congo - Brazzaville",
  :CH => "Switzerland",
  :CI => "Ivory Coast",
  :CK => "Cook Islands",
  :CL => "Chile",
  :CM => "Cameroon",
  :CN => "China",
  :CO => "Colombia",
  :CR => "Costa Rica",
  :CS => "Serbia and Montenegro",
  :CT => "Canton and Enderbury Islands",
  :CU => "Cuba",
  :CV => "Cape Verde",
  :CX => "Christmas Island",
  :CY => "Cyprus",
  :CZ => "Czech Republic",
  :DD => "East Germany",
  :DE => "Germany",
  :DJ => "Djibouti",
  :DK => "Denmark",
  :DM => "Dominica",
  :DO => "Dominican Republic",
  :DZ => "Algeria",
  :EC => "Ecuador",
  :EE => "Estonia",
  :EG => "Egypt",
  :EH => "Western Sahara",
  :ER => "Eritrea",
  :ES => "Spain",
  :ET => "Ethiopia",
  :FI => "Finland",
  :FJ => "Fiji",
  :FK => "Falkland Islands",
  :FM => "Micronesia",
  :FO => "Faroe Islands",
  :FR => "France",
  :GA => "Gabon",
  :GB => "United Kingdom",
  :GD => "Grenada",
  :GE => "Georgia",
  :GF => "French Guiana",
  :GG => "Guernsey",
  :GH => "Ghana",
  :GI => "Gibraltar",
  :GL => "Greenland",
  :GM => "Gambia",
  :GN => "Guinea",
  :GP => "Guadeloupe",
  :GQ => "Equatorial Guinea",
  :GR => "Greece",
  :GS => "South Georgia and the South Sandwich Islands",
  :GT => "Guatemala",
  :GU => "Guam",
  :GW => "Guinea-Bissau",
  :GY => "Guyana",
  :HK => "Hong Kong SAR China",
  :HK => "Hong Kong",
  :HN => "Honduras",
  :HR => "Croatia",
  :HT => "Haiti",
  :HU => "Hungary",
  :ID => "Indonesia",
  :IE => "Ireland",
  :IL => "Israel",
  :IM => "Isle of Man",
  :IN => "India",
  :IQ => "Iraq",
  :IS => "Iceland",
  :IT => "Italy",
  :JE => "Jersey",
  :JM => "Jamaica",
  :JO => "Jordan",
  :JP => "Japan",
  :JT => "Johnston Island",
  :KE => "Kenya",
  :KG => "Kyrgyzstan",
  :KH => "Cambodia",
  :KI => "Kiribati",
  :KM => "Comoros",
  :KN => "Saint Kitts and Nevis",
  :KR => "South Korea",
  :KW => "Kuwait",
  :KY => "Cayman Islands",
  :KZ => "Kazakhstan",
  :LA => "Laos",
  :LB => "Lebanon",
  :LC => "Saint Lucia",
  :LI => "Liechtenstein",
  :LK => "Sri Lanka",
  :LR => "Liberia",
  :LS => "Lesotho",
  :LT => "Lithuania",
  :LU => "Luxembourg",
  :LV => "Latvia",
  :LY => "Libya",
  :MA => "Morocco",
  :MC => "Monaco",
  :MD => "Moldova",
  :ME => "Montenegro",
  :MF => "Saint Martin",
  :MG => "Madagascar",
  :MH => "Marshall Islands",
  :MI => "Midway Islands",
  :MK => "Macedonia",
  :ML => "Mali",
  :MM => "Myanmar",
  :MN => "Mongolia",
  :MO => "Macau SAR China",
  :MO => "Macau",
  :MP => "Northern Mariana Islands",
  :MQ => "Martinique",
  :MR => "Mauritania",
  :MS => "Montserrat",
  :MT => "Malta",
  :MU => "Mauritius",
  :MV => "Maldives",
  :MW => "Malawi",
  :MX => "Mexico",
  :MY => "Malaysia",
  :MZ => "Mozambique",
  :NA => "Namibia",
  :NC => "New Caledonia",
  :NE => "Niger",
  :NF => "Norfolk Island",
  :NG => "Nigeria",
  :NI => "Nicaragua",
  :NL => "Netherlands",
  :NO => "Norway",
  :NP => "Nepal",
  :NQ => "Dronning Maud Land",
  :NR => "Nauru",
  :NT => "Neutral Zone",
  :NU => "Niue",
  :NZ => "New Zealand",
  :OM => "Oman",
  :PA => "Panama",
  :PC => "Pacific Islands Trust Territory",
  :PE => "Peru",
  :PF => "French Polynesia",
  :PG => "Papua New Guinea",
  :PH => "Philippines",
  :PK => "Pakistan",
  :PL => "Poland",
  :PM => "Saint Pierre and Miquelon",
  :PN => "Pitcairn",
  :PR => "Puerto Rico",
  :PS => "Palestinian Territory",
  :PT => "Portugal",
  :PU => "U.S. Miscellaneous Pacific Islands",
  :PW => "Palau",
  :PY => "Paraguay",
  :PZ => "Panama Canal Zone",
  :QA => "Qatar",
  :QO => "Outlying Oceania",
  :QU => "European Union",
  :RE => "Reunion",
  :RO => "Romania",
  :RS => "Serbia",
  :RU => "Russia",
  :RW => "Rwanda",
  :SA => "Saudi Arabia",
  :SB => "Solomon Islands",
  :SC => "Seychelles",
  :SD => "Sudan",
  :SE => "Sweden",
  :SG => "Singapore",
  :SH => "Saint Helena",
  :SI => "Slovenia",
  :SJ => "Svalbard and Jan Mayen",
  :SK => "Slovakia",
  :SL => "Sierra Leone",
  :SM => "San Marino",
  :SN => "Senegal",
  :SO => "Somalia",
  :SR => "Suriname",
  :ST => "Sao Tome and Principe",
  :SU => "Union of Soviet Socialist Republics",
  :SV => "El Salvador",
  :SY => "Syria",
  :SZ => "Swaziland",
  :TC => "Turks and Caicos Islands",
  :TD => "Chad",
  :TF => "French Southern Territories",
  :TG => "Togo",
  :TH => "Thailand",
  :TJ => "Tajikistan",
  :TK => "Tokelau",
  :TL => "East Timor",
  :TM => "Turkmenistan",
  :TN => "Tunisia",
  :TO => "Tonga",
  :TR => "Turkey",
  :TT => "Trinidad and Tobago",
  :TV => "Tuvalu",
  :TW => "Taiwan",
  :TZ => "Tanzania",
  :UA => "Ukraine",
  :UG => "Uganda",
  :UM => "United States Minor Outlying Islands",
  :US => "United States",
  :UY => "Uruguay",
  :UZ => "Uzbekistan",
  :VA => "Vatican",
  :VC => "Saint Vincent and the Grenadines",
  :VD => "North Vietnam",
  :VE => "Venezuela",
  :VG => "British Virgin Islands",
  :VI => "U.S. Virgin Islands",
  :VN => "Vietnam",
  :VU => "Vanuatu",
  :WF => "Wallis and Futuna",
  :WK => "Wake Island",
  :WS => "Samoa",
  :YD => "People's Democratic Republic of Yemen",
  :YE => "Yemen",
  :YT => "Mayotte",
  :ZA => "South Africa",
  :ZM => "Zambia",
  :ZW => "Zimbabwe",
  :AE => "United Arab Emirates",
}

# countries.each do |key, value|
#   Country.create!(:name => value, :iso => key)
# end


# categories = [
#   'Youth',
#   'Health and Beauty',
#   'Food and Wine',
#   'Education',
#   'Sports',
#   'Art & Culture',
#   'Home & Decor',
#   'Men',
#   'Women',
#   'Cars/motorcycles/transportation',
#   'Finance/Banks/Insurance',
#   'Entertainment/Videogames',
#   'Fashion & Style',
#   'Tech',
#   'Web 2.0 / Internet'
# ]

# categories = [
#   'Art - Acting',
#   'Art - Architecture',
#   'Art - Crafts',
#   'Art - Dance',
#   'Art - Design',
#   'Art - Digital Art',
#   'Art - Drawing',
#   'Art - Graffiti',
#   'Art - Graphic Design',
#   'Art - Painting',
#   'Art - Sculpture',
#   'Art - Visual Arts',
#   'Business - Business Design',
#   'Business - Economics',
#   'Business - Emerging Markets',
#   'Business - Finance',
#   'Business - Get a Job',
#   'Business - Innovation',
#   'Business - International Business',
#   'Business - Investing',
#   'Business - Loans',
#   'Business - M&A Buying and Selling a Business',
#   'Business - Marketing',
#   'Business - Public Relations',
#   'Business - Real Estate',
#   'Business - Sales',
#   'Business - Small Business',
#   'Business - Startups',
#   'Business - Web 3.0',
#   'Foodie Blogs - BBQ & Grilling',
#   'Foodie Blogs - Beers',
#   'Foodie Blogs - Chefs & Cooks',
#   'Foodie Blogs - Chocolates',
#   'Foodie Blogs - Cocktails',
#   'Foodie Blogs - Drinks',
#   'Foodie Blogs - Home Cooking  ',
#   'Foodie Blogs - Recipes',
#   'Foodie Blogs - Restaurants',
#   'Foodie Blogs - Vegetarian',
#   'Foodie Blogs - Wine',
#   'Foodie Blogs - World Cuisine',
#   'Education - E-learning',
#   'Education - University',
#   'Education - Middle School',
#   'Education - High School',
#   'Education - Primary School',
#   'Education - Kindergarten',
#   'Entertainment',
#   'Fashion',
#   'Gaming Blogs - Computer & Video Games',
#   'Gaming Blogs - Gaming',
#   'Gaming Blogs - Toys',
#   'Health Blogs - Alternative Medicine',
#   'Health Blogs - Beauty',
#   'Health Blogs - Fitness',
#   'Health Blogs - Medicine',
#   'Health Blogs - Natural Health',
#   'Health Blogs - Nutrition',
#   'Health Blogs - Sexual Health',
#   'Movies & TV',
#   'Animals and Pets',
#   'Music',
#   'Photography - ',
#   'Transportation - Buying and Selling Autos',
#   'Transportation - Car Reviews ',
#   'Transportation - Classic & Collectible ',
#   'Transportation - Green Transportation',
#   'Transportation - Motorcycles ',
#   'Transportation - Trucking',
#   'Tech Blogs - Augmented Reality ',
#   'Tech Blogs - Content Marketing',
#   'Tech Blogs - Digital Design',
#   'Tech Blogs - Digital Marketing',
#   'Tech Blogs - EdTech',
#   'Tech Blogs - Gadgets',
#   'Tech Blogs - GreenTech ',
#   'Tech Blogs - Mobile',
#   'Tech Blogs - Robotics',
#   'Tech Blogs - Software Development  ',
#   'Tech Blogs - Virtual Reality',
#   'Social Media - Social Networking',
#   'Travel',
#   'Sport Blogs - Air Sports',
#   'Sport Blogs - Baseball',
#   'Sport Blogs - Basketball',
#   'Sport Blogs - Cycling',
#   'Sport Blogs - Extreme',
#   'Sport Blogs - Fishing',
#   'Sport Blogs - Football',
#   'Sport Blogs - Golf',
#   'Sport Blogs - Hockey  ',
#   'Sport Blogs - Racing',
#   'Sport Blogs - Running',
#   'Sport Blogs - Sailing',
#   'Sport Blogs - Scuba Diving',
#   'Sport Blogs - Snow Sports',
#   'Sport Blogs - Soccer',
#   'Sport Blogs - Sports Coaching',
#   'Sport Blogs - Surfing',
#   'Sport Blogs - Tennis',
#   'Sport Blogs - Urban Sports',
#   'Sport Blogs - Water Sports',
#   'Science - Earth Science',
#   'Science - Engineering',
#   'Science - Science & Exploration'
# ]
categories = [
  "Autos & Vehicles",
  "Comedy",
  "Education",
  "Entertainment",
  "Film & Animation",
  "Gaming",
  "Howto & Style",
  "Music",
  "News & Politics",
  "Nonprofits & Activism",
  "People & Blog",
  "Pets & Animals",
  "Science & Technology",
  "Sports",
  "Travel & Events"
]


categories_sp = [
  "Automovilismo",
  "Comedia",
  "Educación",
  "Entretenimiento",
  "Cine y animaciones",
  "Juegos",
  "Instructivos y estilo",
  "Música",
  "Noticias y política",
  "Activismo y ONG",
  "Gente y blogs",
  "Mascotas y animales",
  "Ciencia y tecnología",
  "Deportes",
  "Viajes y eventos"
]

I18n.locale = :en

  categories.each do |name|
    category = Category.create!(:name => name)
  end




