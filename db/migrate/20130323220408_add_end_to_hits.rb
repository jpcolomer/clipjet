class AddEndToHits < ActiveRecord::Migration
  def change
    add_column :hits, :end_of_video, :boolean
  end
end
