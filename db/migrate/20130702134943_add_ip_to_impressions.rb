class AddIpToImpressions < ActiveRecord::Migration
  def change
    add_column :impressions, :ip, :string
  end
end
