class AddInfluencerImpressionsCountToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :influencer_impressions_count, :integer, default: 0
  end
end
