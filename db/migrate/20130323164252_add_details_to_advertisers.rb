class AddDetailsToAdvertisers < ActiveRecord::Migration
  def change
    add_column :advertisers, :name, :string
    add_column :advertisers, :url, :string
    add_column :advertisers, :video_url, :string
    add_column :advertisers, :country_id, :integer
    add_column :advertisers, :category_id, :integer
	add_index :advertisers, :country_id
	add_index :advertisers, :category_id
  end

end
