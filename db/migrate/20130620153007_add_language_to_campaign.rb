class AddLanguageToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :language, :string
    add_index :campaigns, :language
  end
end
