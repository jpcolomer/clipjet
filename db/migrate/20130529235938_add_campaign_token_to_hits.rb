class AddCampaignTokenToHits < ActiveRecord::Migration
  def up
    add_column :hits, :campaign_token, :string
    add_index :hits, :campaign_token
    remove_column :hits, :campaign_id
  end
  def down
    remove_column :hits, :campaign_token, :string
    add_column :hits, :campaign_id, :integer
    add_index :hits, :campaign_id
  end
end
