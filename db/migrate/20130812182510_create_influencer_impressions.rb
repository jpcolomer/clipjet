class CreateInfluencerImpressions < ActiveRecord::Migration
  def change
    create_table :influencer_impressions do |t|
      t.string :ip
      t.string :country
      t.integer :influencer_video_id

      t.timestamps
    end
    add_index :influencer_impressions, :influencer_video_id
  end
end
