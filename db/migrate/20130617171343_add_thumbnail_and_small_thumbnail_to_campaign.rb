class AddThumbnailAndSmallThumbnailToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :thumbnail, :string
    add_column :campaigns, :small_thumbnail, :string
  end
end
