class AddIndexesToInfluencerVideosAndViews < ActiveRecord::Migration
  def change
    add_index :influencer_views, :token
    add_index :influencer_views, :influencer_video_token
    add_index :influencer_videos, :token
    add_index :influencer_videos, :campaign_id
    add_index :influencer_videos, :user_id
  end
end
