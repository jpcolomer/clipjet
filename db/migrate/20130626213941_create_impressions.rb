class CreateImpressions < ActiveRecord::Migration
  def change
    create_table :impressions do |t|
      t.integer :campaign_id
      t.integer :site_id
      t.timestamps
    end
    add_index :impressions, :campaign_id
    add_index :impressions, :site_id
  end
end
