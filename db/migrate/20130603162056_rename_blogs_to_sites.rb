class RenameBlogsToSites < ActiveRecord::Migration
  def up
    rename_table :blogs, :sites
    rename_column :hits, :blog_token, :site_token
  end

  def down
    rename_table :sites, :blogs
    rename_column :hits, :site_token, :blog_token
  end
end
