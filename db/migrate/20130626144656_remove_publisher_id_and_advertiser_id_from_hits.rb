class RemovePublisherIdAndAdvertiserIdFromHits < ActiveRecord::Migration
  def up
    remove_column :hits, :publisher_id
    remove_column :hits, :advertiser_id
  end

  def down
    add_column :hits, :advertiser_id, :integer
    add_column :hits, :publisher_id, :integer
  end
end
