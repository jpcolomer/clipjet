class CreateHits < ActiveRecord::Migration
  def change
    create_table :hits do |t|
      t.integer :publiser_id
      t.integer :advertiser_id

      t.timestamps
    end
    add_index :hits, :publiser_id
    add_index :hits, :advertiser_id
  end
end
