class ChangeElapsedTimeTypeOfHits < ActiveRecord::Migration
  def up
    change_column :hits, :elapsed_time, :decimal
  end

  def down
    change_column :hits, :elapsed_time, :integer
  end
end
