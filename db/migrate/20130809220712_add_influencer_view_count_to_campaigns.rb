class AddInfluencerViewCountToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :influencer_views_count, :integer, default: 0
  end
end
