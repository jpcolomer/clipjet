class AddBalanceToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :balance, :decimal, default: 0
  end
end
