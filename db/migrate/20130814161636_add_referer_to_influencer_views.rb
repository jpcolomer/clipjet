class AddRefererToInfluencerViews < ActiveRecord::Migration
  def change
    add_column :influencer_views, :referer, :string
  end
end
