class AddImpressionsCountToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :impressions_count, :integer, default: 0
  end
end
