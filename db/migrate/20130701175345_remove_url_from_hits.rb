class RemoveUrlFromHits < ActiveRecord::Migration
  def up
    remove_column :hits, :url
  end

  def down
    add_column :hits, :url, :string
  end
end
