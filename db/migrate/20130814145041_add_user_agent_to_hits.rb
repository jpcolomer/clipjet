class AddUserAgentToHits < ActiveRecord::Migration
  def change
    add_column :hits, :user_agent, :string
  end
end
