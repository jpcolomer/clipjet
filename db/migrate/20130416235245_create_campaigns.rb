class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string :site_url
      t.string :video_url
      t.integer :credit
      t.integer :video_duration
      t.integer :category_id
      t.integer :user_id

      t.timestamps
    end
    add_index :campaigns, :user_id
    add_index :campaigns, :category_id
  end
end
