class AddSecretToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :secret, :string
  end
end
