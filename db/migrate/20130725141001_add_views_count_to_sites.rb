class AddViewsCountToSites < ActiveRecord::Migration
  def up
    add_column :sites, :views_count, :integer, default: 0
    Site.all.each do |site|
      site.views_count = site.views
      site.save
    end
  end

  def down
    remove_column :sites, :views_count
  end
end
