class SetDefaultBalanceToWallets < ActiveRecord::Migration
  def up
    change_column :wallets, :balance, :decimal, default: 0.0
  end

  def down
    change_column :wallets, :balance, :decimal
  end
end
