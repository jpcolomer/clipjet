class AddViewCostToHits < ActiveRecord::Migration
  def change
    add_column :hits, :view_cost, :decimal
  end
end
