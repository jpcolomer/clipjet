class AddArticleUrlToHits < ActiveRecord::Migration
  def change
    add_column :hits, :article_url, :string
  end
end
