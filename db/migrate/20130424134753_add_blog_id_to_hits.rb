class AddBlogIdToHits < ActiveRecord::Migration
  def up
  	remove_column :hits, :article_id
    add_column :hits, :blog_id, :integer
    add_column :hits, :url, :string
  end

  def down
  	remove_column :hits, :blog_id
    remove_column :hits, :url
    add_column :hits, :article_id, :integer
  end
end
