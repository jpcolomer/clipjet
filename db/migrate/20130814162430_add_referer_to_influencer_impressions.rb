class AddRefererToInfluencerImpressions < ActiveRecord::Migration
  def change
    add_column :influencer_impressions, :referer, :string
  end
end
