class AddCountryToHits < ActiveRecord::Migration
  def change
    add_column :hits, :country, :string
  end
end
