class AddCampaignIdAndUserIdToHits < ActiveRecord::Migration
  def change
    add_column :hits, :campaign_id, :integer
    add_column :hits, :article_id, :integer
    add_index :hits, :article_id
    add_index :hits, :campaign_id
  end
end
