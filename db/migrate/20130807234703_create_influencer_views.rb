class CreateInfluencerViews < ActiveRecord::Migration
  def change
    create_table :influencer_views do |t|
      t.string :ip
      t.string :token
      t.string :country
      t.string :influencer_video_token
      t.decimal :elapsed_time
      t.boolean :end_of_video

      t.timestamps
    end
  end
end
