class AddTimeAndIpToHits < ActiveRecord::Migration
  def change
    add_column :hits, :elapsed_time, :integer
    add_column :hits, :ip, :string
  end
end
