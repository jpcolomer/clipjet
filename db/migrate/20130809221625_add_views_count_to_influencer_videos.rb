class AddViewsCountToInfluencerVideos < ActiveRecord::Migration
  def change
    add_column :influencer_videos, :views_count, :integer, default: 0
  end
end
