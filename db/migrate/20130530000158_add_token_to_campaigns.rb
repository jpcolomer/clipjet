class AddTokenToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :token, :string
    add_index :campaigns, :token
  end
end
