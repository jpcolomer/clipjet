class AddCountryIsoToCountries < ActiveRecord::Migration
  def change
    add_column :countries, :iso, :string
    add_index :countries, :iso
  end
end
