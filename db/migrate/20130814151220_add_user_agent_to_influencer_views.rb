class AddUserAgentToInfluencerViews < ActiveRecord::Migration
  def change
    add_column :influencer_views, :user_agent, :string
  end
end
