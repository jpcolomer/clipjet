class AddHitCostToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :hit_cost, :decimal
  end
end
