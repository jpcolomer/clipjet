class CreateInfluencerVideos < ActiveRecord::Migration
  def change
    create_table :influencer_videos do |t|
      t.string :token
      t.integer :user_id
      t.integer :campaign_id

      t.timestamps
    end
  end
end
