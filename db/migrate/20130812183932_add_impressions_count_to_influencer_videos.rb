class AddImpressionsCountToInfluencerVideos < ActiveRecord::Migration
  def change
    add_column :influencer_videos, :impressions_count, :integer, default: 0
  end
end
