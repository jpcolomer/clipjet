class AddBlogTokenToHits < ActiveRecord::Migration
  def up
    add_column :hits, :blog_token, :string
    remove_column :hits, :blog_id
    add_index :hits, :blog_token
  end
  def down
    remove_column :hits, :blog_token
    add_column :hits, :blog_id, :integer
  end
end
