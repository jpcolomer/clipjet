class AddViewCostToInfluencerViews < ActiveRecord::Migration
  def change
    add_column :influencer_views, :view_cost, :decimal
  end
end
