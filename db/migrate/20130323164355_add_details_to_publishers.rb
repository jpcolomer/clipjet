class AddDetailsToPublishers < ActiveRecord::Migration
  def change
    add_column :publishers, :blog_url, :string
  end
end
