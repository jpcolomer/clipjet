class CreateWithdrawalRequests < ActiveRecord::Migration
  def change
    create_table :withdrawal_requests do |t|
      t.integer :user_id
      t.decimal :amount

      t.timestamps
    end
    add_index :withdrawal_requests, :user_id
  end
end
