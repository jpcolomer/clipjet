class AddTokenToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :token, :string
    add_index :blogs, :token
  end
end
