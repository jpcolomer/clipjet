class AddElapsedTimeAndVideoDurationIndex < ActiveRecord::Migration
  def up
    add_index :hits, :elapsed_time
    add_index :campaigns, :video_duration
  end

  def down
    remove_index :hits, :elapsed_time
    remove_index :campaigns, :video_duration
  end
end
