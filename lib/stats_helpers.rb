module StatsHelpers
  def chart_stats_for(records, record_name)
    stats = records.order("date(#{record_name}.created_at)").group("date(#{record_name}.created_at)")
    stats = yield stats
    unless stats.empty?
      (stats.first[0].to_date..Time.zone.now.to_date).map do |date|
        {
          created_at: date,
          value: stats[date.to_s].to_f || 0
        }
      end
    end
  end

  def country_stats
    object = OpenStruct.new
    yield object
    _count = object.count
    _records = object.records

    if _count > 0
      other_total = 0
      array = _records.inject([]) do |result, c| 
        _total = (c.total.to_i/_count.to_f*100)

        if _total > 1 && c.country
          result << {
            label: c.country, 
            value: (c.total.to_i/_count.to_f*100).round(1)
          }
        else
          other_total += _total
        end
        result
      end
      if other_total > 0
        array << {label: 'Other', value: other_total.round(1)}
      end
      array
    end    
  end
end