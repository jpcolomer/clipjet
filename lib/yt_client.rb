require 'rest-client'
module YT
  class << self
    def get_info(video)
      id = get_id(video)
      url = api_url(id)
      json = api_call(url)
      json_to_struct(json['data'])
    end

    def json_to_struct(json)
      hash = json.slice('title', 'viewCount', 'description')
      hash['id'] = json['id']
      hash['thumbnail'] = json['thumbnail']['hqDefault']
      hash['small_thumbnail'] = json['thumbnail']['sqDefault']
      hash['duration'] = json['duration'].to_i
      OpenStruct.new(hash)
    end

    def api_call(url)
      JSON.parse(RestClient.get(url))
    end

    def get_id(video)
      params = Rack::Utils.parse_nested_query(video,'/?&;')
      params['v']
    end

    def api_url(id)
      "https://gdata.youtube.com/feeds/api/videos/#{id}?v=2&alt=jsonc"
    end
  end
end