module TokenHelpers
  
  def self.included(base)
    base.extend ClassMethods
    base.class_eval do
      validates :token, presence: true, uniqueness: true
      before_validation :set_token, if: lambda{|record| record.new_record?}
    end
  end

  def set_token
    maximum_id = "#{self.class.maximum('id') + 1}"
    self.token = SecureRandom.urlsafe_base64(3) + Base64.urlsafe_encode64(maximum_id).gsub('=', '_')
    true
  end

  module ClassMethods
    def maximum(arg, options = {})
      super(arg, options) || 0
    end
  end

end