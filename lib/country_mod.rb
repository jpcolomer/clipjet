module CountryMod
  module ClassMethods
    def country_stats
      group("country").select("count(*) as total, country").order("country").order("total DESC")    
    end
  end
  
  module InstanceMethods
    def set_country
      self.country = GeoIPWrap.get_country(ip)
      true
    end
  end
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
    receiver.class_eval do
      before_create :set_country
    end
  end
end