namespace :hit do
  
  desc "Set hit view_cost"
  task :fill_view_cost => :environment do |t, args|
    Hit.skip_callback(:update, :after, :charge_pay_update_count)
    Hit.where(view_cost: nil).each do |hit|
      hit.send(:set_view_cost)
      hit.save(validate: false)
    end
    Hit.set_callback(:update, :after, :charge_pay_update_count, if: lambda{|hit| hit.elapsed_time_was.nil? && hit.elapsed_time >= hit.campaign.minimum_time})    
  end

end