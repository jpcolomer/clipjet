desc "Convert old advertisers to users"
task :advertisers_to_users => :environment do

  Advertiser.all.each do |a|
    u = User.create!(email: a.email, password: '12345678a')
    u.encrypted_password = a.encrypted_password
    u.reset_password_token = a.reset_password_token
    u.remember_created_at = a.remember_created_at
    u.sign_in_count = a.sign_in_count
    u.current_sign_in_at = a.current_sign_in_at
    u.last_sign_in_at = a.last_sign_in_at
    u.current_sign_in_ip = a.current_sign_in_ip
    u.last_sign_in_ip = a.last_sign_in_ip
    u.save
  end


end


# Publisher.all.each do |p|
#   user = User.create!(
#     email: p.email,
#     encrypted_password:  p.encrypted_password,
#     reset_password_token: p.reset_password_token,
#     reset_password_sent_at: p.reset_password_sent_at,
#     remember_created_at: p.remember_created_at,
#     sign_in_count: p.sign_in_count,
#     current_sign_in_at: p.current_sign_in_at,
#     last_sign_in_at: p.last_sign_in_at,
#     current_sign_in_ip: p.current_sign_in_ip,
#     last_sign_in_ip: p.last_sign_in_ip
#   )
  
#   user.sites.create!(:url => p.site_url)
# end


# Advertiser.all.each do |a|
#   user = User.new(
#     email: a.email,
#     encrypted_password:  a.encrypted_password,
#     reset_password_token: a.reset_password_token,
#     reset_password_sent_at: a.reset_password_sent_at,
#     remember_created_at: a.remember_created_at,
#     sign_in_count: a.sign_in_count,
#     current_sign_in_at: a.current_sign_in_at,
#     last_sign_in_at: a.last_sign_in_at,
#     current_sign_in_ip: a.current_sign_in_ip,
#     last_sign_in_ip: a.last_sign_in_ip
#   )

# user = User.new
# user.email = a.email
# user.encrypted_password = a.encrypted_password
# user.reset_password_token = a.reset_password_token
# user.reset_password_sent_at = a.reset_password_sent_at
# user.remember_created_at = a.remember_created_at
# user.sign_in_count = a.sign_in_count
# user.current_sign_in_at = a.current_sign_in_at
# user.last_sign_in_at = a.last_sign_in_at
# user.current_sign_in_ip = a.current_sign_in_ip
# user.last_sign_in_ip = a.last_sign_in_ip
# user.save!
  
#   user.campaigns.create!(
#     video_url: a.video_url,
#     category_id: a.category_id,
#     site: a.url
#   )
# end