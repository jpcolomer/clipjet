namespace :hit do
  
  desc "Set hit country from ip"
  task :ip_to_country => :environment do |t, args|
    Hit.skip_callback(:update, :after, :charge_pay_update_count)
    Hit.where(country: nil).where("ip is not null").each do |hit|
      hit.set_country
      hit.save(validate: false)
    end
    Hit.set_callback(:update, :after, :charge_pay_update_count, if: lambda{|hit| hit.elapsed_time_was.nil? && hit.elapsed_time >= hit.campaign.minimum_time})
  end

end