namespace :impression do
  
  desc "Set impression country from ip"
  task :ip_to_country, [:arg1, :arg2] => :environment do |t, args|

    Impression.where(country: nil).where("ip is not null").where("id > ?", args[:arg1]).where("id <= ?", args[:arg2]).each do |impression|
      impression.set_country
      impression.save(validate: false)
    end

  end

end