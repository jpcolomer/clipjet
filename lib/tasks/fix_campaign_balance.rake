namespace :campaign do
  
  desc "Fix old records to use campaign balance"
  task :balance_record_fix => :environment do

    Campaign.all.each do |c|
      c.balance = c.credit * c.hit_cost
      c.save(validate: false)
    end

  end

end