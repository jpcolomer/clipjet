module GeoIPWrap
  def self.get_geoip
    @geoip ||= GeoIP.new(Rails.root.join('db', 'GeoIP.dat'))
  end

  def self.get_country(ip)
    unless ["127.0.0.1", "192.168.0.15", nil].include?(ip)
      self.get_geoip.country(ip).country_name
    end
  end
end