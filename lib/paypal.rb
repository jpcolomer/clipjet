module Paypal
  PAYPAL_URL = APP_CONFIG['paypal_url']
	class << self

    def set_values(return_url, notify_url, campaign, amount)
      @values = {
        business: APP_CONFIG['paypal_email'],
        cmd: '_xclick',
        upload: 1,
        return: return_url,
        custom: campaign.id,
        amount: amount,
        item_name: "ClipJet Views",
        quantity: 1,
        notify_url: notify_url,
        rm: 1,
        no_shipping: 1,
        cert_id: APP_CONFIG['paypal_cert_id']
      }
    end

    def encrypted_values(return_url, notify_url, campaign, amount)
      set_values(return_url, notify_url, campaign, amount)
      encrypt(@values)
    end

    def url(return_url, notify_url, campaign, amount)
      set_values(return_url, notify_url, campaign, amount)
      api_url = APP_CONFIG['paypal_url']
      PAYPAL_URL + {cmd: '_s-xclick', encrypted: encrypt(@values)}.to_query
    end

    PAYPAL_CERT_PEM = File.read("#{Rails.root}/certs/#{APP_CONFIG['paypal_cert']}")
    APP_CERT_PEM = File.read("#{Rails.root}/certs/app_cert.pem")
    APP_KEY_PEM = File.read("#{Rails.root}/certs/app_key.pem")
    def encrypt(values)
        signed = OpenSSL::PKCS7::sign(OpenSSL::X509::Certificate.new(APP_CERT_PEM),        OpenSSL::PKey::RSA.new(APP_KEY_PEM, ''), values.map { |k, v| "#{k}=#{v}" }.join("\n"), [], OpenSSL::PKCS7::BINARY)
        OpenSSL::PKCS7::encrypt([OpenSSL::X509::Certificate.new(PAYPAL_CERT_PEM)], signed.to_der, OpenSSL::Cipher::Cipher::new("DES3"),        OpenSSL::PKCS7::BINARY).to_s.gsub("\n", "")
    end
  end
end