module DurationEncryptor
  KEY = ENCRYPT_CONFIG['encrypt_key']
  class << self
    def encrypt(duration, key=KEY)
      e = OpenSSL::Cipher::Cipher.new 'DES-EDE3-CBC'
      e.encrypt
      e.pkcs5_keyivgen(key)
      s = e.update duration.to_s
      s << e.final
      Base64.encode64(s).encode('utf-8')
    end

    def decrypt(duration, key=KEY)
      e = OpenSSL::Cipher::Cipher.new 'DES-EDE3-CBC'
      e.decrypt
      e.pkcs5_keyivgen(key)
      s = Base64.decode64 duration.encode('ascii-8bit')
      s = e.update s
      s << e.final
      s
    end
  end
end