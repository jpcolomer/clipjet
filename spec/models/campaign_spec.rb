require 'spec_helper'
require './lib/yt_client.rb'

describe Campaign do
  it { should allow_mass_assignment_of :thumbnail }
  it { should allow_mass_assignment_of :small_thumbnail }
  it { should allow_mass_assignment_of :language }
  it { should allow_mass_assignment_of :yt_count }
  it { should allow_mass_assignment_of :description }

  it { should_not allow_mass_assignment_of :credit }
  it {should belong_to(:user)}
  it {should belong_to(:category)}
  it {should have_many(:hits)}
  it {should have_many(:impressions)}
  it {should have_many(:influencer_videos)}
  it {should have_many(:influencer_views).through(:influencer_videos)}

  it {should validate_presence_of(:language)}
  it {should validate_presence_of(:name)}
  it {should validate_presence_of(:user)}
  it {should validate_presence_of(:video_duration)}
  it {should validate_presence_of(:category)}
  it {should validate_presence_of(:video_url)}

  it { should_not allow_value("").for(:video_url) }
  it { should_not allow_value("test").for(:video_url) }
  it { should_not allow_value("http://test.com").for(:video_url) }

  it { should allow_value("http://www.youtube.com/watch?v=v-YrhySY2RM").for(:video_url) }

  it { should_not allow_value("test").for(:site_url) }
  it { should allow_value("http://test").for(:site_url) }
  it { should allow_value("https://test").for(:site_url) }

 it { should ensure_inclusion_of(:language).in_array(Campaign::LANGS) }

  it { should validate_numericality_of(:balance) }

  it 'responds to hit_cost' do
    should respond_to(:hit_cost)
  end

  it 'is not valid without hit_cost' do
    Campaign.any_instance.stub(:set_hit_cost)
    expect(build(:campaign)).to have_at_least(1).errors_on(:hit_cost)
  end

  it 'has error if balance < 0' do
    campaign = build(:campaign)
    campaign.balance = -1
    expect(campaign).to have(1).errors_on(:balance)
  end

  it 'is valid' do
    expect(build(:campaign)).to be_valid
  end

  it "doesn't allow 2 campaigns with same token" do
    Campaign.any_instance.stub(:set_token).and_return(true)
    create(:campaign, token: '12345')
    expect(build(:campaign, token: '12345')).to_not be_valid
  end

  it "doesn't allow a campaign without a token" do
    Campaign.any_instance.stub(:set_token).and_return(true)
    expect(build(:campaign)).to_not be_valid
  end

  it "doesn't change token after update" do
    campaign = create(:campaign)
    token = campaign.token
    campaign.update_attributes(site_url: 'http://colomer.jp')
    expect(campaign.token).to eq token
  end

  describe '.find_video' do
    let(:category_id) { Category.first.id }
    let(:ip) { '1234' }
    it "returns campaign with language en" do
      campaign = create(:campaign_with_balance, category_id: category_id, language: 'en')
      create(:campaign_with_balance, category_id: category_id, language: 'es')
      expect(Campaign.find_video(category_id: category_id, language: 'en', ip: ip)).to eq campaign
    end

    it "returns campaign with language es" do
      create(:campaign_with_balance, category_id: category_id, language: 'en')
      campaign = create(:campaign_with_balance, category_id: category_id, language: 'es')
      expect(Campaign.find_video(category_id: category_id, language: 'es')).to eq campaign
    end

    context "given a language" do

      context "given an ip" do
        it "runs find_video without campaign that was already seen" do
          campaign = create(:campaign_with_balance, category_id: category_id, language: 'es')
          campaign2 = create(:campaign_with_balance, category_id: category_id, language: 'es')
          create(:campaign_with_balance, category_id: category_id, language: 'en')
          create(:hit, campaign: campaign, ip: ip)
          expect(Campaign.find_video(category_id: category_id, language: 'es', ip: ip)).to eq campaign2
        end
      end

      it "returns campaign of similar category if there is no campaign on category 1" do
        campaign = create(:campaign_with_balance, category_id: category_id+1, language: 'es')
        expect(Campaign.find_video(category_id: category_id, language: 'es', ip: ip)).to eq campaign
      end

      it "returns random campaign with language es if there is no campaign on category 1 or similar category in any language" do
        campaign = create(:campaign_with_balance, category_id: category_id+2, language: 'es')
        create(:campaign_with_balance, category_id: category_id+2, language: 'en')
        expect(Campaign.find_video(category_id: category_id, language: 'es', ip: ip)).to eq campaign
      end

      it "returns random campaign of same category if there is no campaign on es" do
        campaign = create(:campaign_with_balance, category_id: category_id, language: 'en')
        create(:campaign_with_balance, category_id: category_id+1, language: 'en')
        expect(Campaign.find_video(category_id: category_id, language: 'es', ip: ip)).to eq campaign
      end

      it "returns random campaign of similar category if there is no campaign on es" do
        campaign = create(:campaign_with_balance, category_id: category_id+2, language: 'en')
        expect(Campaign.find_video(category_id: category_id, language: 'es', ip: ip)).to eq campaign
      end

      it "returns random campaign if no similar category in any language" do
        campaign = create(:campaign_with_balance, category_id: category_id+2, language: 'en')
        expect(Campaign.find_video(category_id: category_id, language: 'es', ip: ip)).to eq campaign
      end
    end


    it "doesn't return campaign without credit" do
      campaign = create(:campaign, category_id: category_id, language: 'en')
      expect(Campaign.find_video(category_id: category_id, language: 'en', ip: ip)).to_not eq campaign
    end
  end


  describe ".with_enough_balance" do
    it "doesn't return campaigns with balance = 0" do
      campaign = create(:campaign, balance: 0)
      create(:campaign)
      expect(Campaign.with_enough_balance).to_not include campaign
    end

    it "only returns campaigns with balance = 0" do
      create(:campaign, balance: 0)
      campaign = create(:campaign_with_balance)
      expect(Campaign.with_enough_balance).to match_array [campaign]
    end

  end


  describe ".without_ip" do
    it "doesnt return campaign that has already been seen" do
      campaign = create(:campaign)
      create(:hit, ip: '1234', campaign: campaign)
      create(:hit, campaign:  campaign)
      expect(Campaign.without_ip('1234')).to_not include campaign
    end

    it "return campaign that hasnt been seen" do
      campaign = create(:campaign)
      create(:hit, ip: '1234', campaign: campaign)
      campaign2 = create(:campaign_with_balance)
      expect(Campaign.without_ip('1234')).to match_array [campaign2]
    end

    it "return campaign that has been seen for another ip" do
      campaign = create(:campaign)
      create(:hit, ip: '1234', campaign: campaign)
      campaign2 = create(:campaign)
      create(:hit, campaign: campaign2)
      expect(Campaign.without_ip('1234')).to match_array [campaign2]
    end
  end


  describe "#reduce_balance" do
    it "reduces balance by 1" do
      campaign = create(:campaign_with_balance)
      campaign.stub(hit_cost: 1)
      expect{campaign.reduce_balance; campaign.reload}.to change { campaign.balance }.by(-1)
    end

    it "reduces balance by 2.5" do
      campaign = create(:campaign_with_balance)
      campaign.stub(hit_cost: 1.25)
      expect{2.times {campaign.reduce_balance}; campaign.reload}.to change { campaign.balance }.by(-2.5)
    end

    it "doesn't reduce balance if is < hit_cost" do
      campaign = create(:campaign)
      campaign.balance = 0.02
      campaign.save
      expect(campaign.reduce_balance).to be false
      expect{
        campaign.reload
      }.to_not change { campaign.balance }
    end
  end

  it "calls set_secret before creation" do
    campaign = build(:campaign)
    campaign.should_receive(:set_secret)
    campaign.save
  end

  describe "#set_secret" do
    it "sets random secret" do
      campaign = build(:campaign)
      campaign.save
      expect(campaign.secret).to be_a String
      expect(campaign.secret.length).to eq (8*4/3.0).round 
    end
  end

  describe '#minimum_time' do
    it 'returns 0 for video_duration 10' do
      campaign = build_stubbed(:campaign, video_duration: 10)
      expect(campaign.minimum_time).to eq 10
    end
    it 'returns 20 for video_duration 20' do
      campaign = build_stubbed(:campaign, video_duration: 20)
      expect(campaign.minimum_time).to eq 20
    end
    it 'returns 30 for video_duration 200' do
      campaign = build_stubbed(:campaign, video_duration: 200)
      expect(campaign.minimum_time).to eq 30
    end
  end


  describe '#views' do
    it 'returns 1 for 1 hit' do
      campaign = create(:campaign_with_balance)
      create(:hit, campaign: campaign, elapsed_time: 30)
      expect(campaign.views).to eq 1
    end

    it 'returns 2 for 2 hits' do
      campaign = create(:campaign_with_balance)
      create_list(:hit, 2, campaign: campaign, elapsed_time: 30)
      expect(campaign.views).to eq 2
    end

    it 'returns 0 for 0 hits' do
      campaign = create(:campaign_with_balance, video_duration: 50)
      create_list(:hit, 2, campaign: campaign, elapsed_time: 1)
      expect(campaign.views).to eq 0
    end      
  end


  describe "#total_views" do
    let(:campaign) { build_stubbed(:campaign) }
    it "return 1 if only hit view" do
      campaign.stub(views_count: 1)
      expect(campaign.total_views).to eq 1
    end

    it "return 1 if only influencer view" do
      campaign.stub(influencer_views_count: 1)
      expect(campaign.total_views).to eq 1
    end

    it "returns 2 if there is a hit view and an influencer view" do
      campaign.stub(views_count: 1)
      campaign.stub(influencer_views_count: 1)
      expect(campaign.total_views).to eq 2
    end
  end


  describe "#total_impressions" do
    let(:campaign) { build_stubbed(:campaign) }
    it "return 1 if only hit impression" do
      campaign.stub(impressions_count: 1)
      expect(campaign.total_impressions).to eq 1
    end

    it "return 1 if only influencer impression" do
      campaign.stub(influencer_impressions_count: 1)
      expect(campaign.total_impressions).to eq 1
    end

    it "returns 2 if there is a hit impression and an influencer impression" do
      campaign.stub(impressions_count: 1)
      campaign.stub(influencer_impressions_count: 1)
      expect(campaign.total_impressions).to eq 2
    end
  end

  describe '#impression_stats' do
    let(:campaign) {create(:campaign_with_balance)}
    it "returns json of 1 impression" do
      create(:impression, campaign: campaign)
      expect(campaign.impression_stats).to match_array [{created_at: Time.zone.now.to_date, value: 1}]
    end

    it "returns json of 2 impression with different created dates" do
      create(:impression, campaign: campaign)
      create_list(:impression, 2, campaign: campaign, created_at: 1.day.ago)
      expect(campaign.impression_stats).to match_array [{created_at: 1.day.ago.to_date, value: 2}, {created_at: Time.zone.now.to_date, value: 1}]
    end

    it "returns json of 2 impression with different created dates and 0 for intermediate date" do
      create(:impression, campaign: campaign)
      create_list(:impression, 2, campaign: campaign, created_at: 2.day.ago)
      expect(campaign.impression_stats).to match_array [{created_at: 2.days.ago.to_date, value: 2},{created_at: 1.day.ago.to_date, value: 0}, {created_at: Time.zone.now.to_date, value: 1}]
    end
  end

  describe '#hit_stats' do
    let(:campaign) {create(:campaign_with_balance)}
    it "returns json of 1 hit" do
      create(:hit, campaign: campaign, elapsed_time: 1)
      create(:hit_with_end_video, campaign: campaign)
      expect(campaign.hit_stats).to match_array [{created_at: Time.zone.now.to_date, value: 1}]
    end

    it "returns json of 2 hit with different created dates" do
      create(:hit, campaign: campaign, elapsed_time: 1)
      create(:hit_with_end_video, campaign: campaign)
      create_list(:hit_with_end_video, 2, campaign: campaign, created_at: 1.day.ago)
      expect(campaign.hit_stats).to match_array [{created_at: 1.day.ago.to_date, value: 2}, {created_at: Time.zone.now.to_date, value: 1}]
    end

    it "returns json of 2 hit with different created dates and 0 for intermediate date" do
      create(:hit, campaign: campaign, elapsed_time: 1)
      hit=create(:hit_with_end_video, campaign: campaign)
      create_list(:hit_with_end_video, 2, campaign: campaign, created_at: 2.day.ago)
      expect(campaign.hit_stats).to match_array [{created_at: 2.days.ago.to_date, value: 2},{created_at: 1.day.ago.to_date, value: 0}, {created_at: Time.zone.now.to_date, value: 1}]
    end
  end

  describe "#hit_country_stats" do
    let(:campaign) {create(:campaign_with_balance)}
    it "calls hits.country_stats" do
      create(:hit_with_end_video, campaign: campaign)
      campaign.hits.should_receive(:with_minimum_time) { campaign.hits }
      campaign.hits.should_receive(:country_stats) { [] }
      campaign.hit_country_stats
    end

    it "returns hash for 0 Country" do
      campaign.hits.stub(with_minimum_time: campaign.hits)
      campaign.hits.stub(:country_stats).and_return([])
      expect(campaign.hit_country_stats).to be_nil
    end

    it "returns hash for 1 Country" do
      campaign.hits.stub(with_minimum_time: campaign.hits)
      campaign.hits.stub(:country_stats).and_return([OpenStruct.new(country: "Chile", total: '1')])
      campaign.stub(views_count: 1)
      create(:hit_with_end_video, campaign: campaign)
      expect(campaign.hit_country_stats).to match_array [{label: "Chile", value: 100.0}]
    end

    it "returns hash for 2 Countries" do
      campaign.hits.stub(with_minimum_time: campaign.hits)
      campaign.hits.stub(:country_stats).and_return([OpenStruct.new(country: "Chile", total: '2'), OpenStruct.new(country: "Spain", total: '2')])
      campaign.stub(views_count: 4)
      create(:hit_with_end_video, campaign: campaign)
      expect(campaign.hit_country_stats).to match_array [{label: "Chile", value: 50.0}, {label: "Spain", value: 50.0}]
    end

    it "returns hash for 2 Countries, one with less 1%" do
      campaign.hits.stub(with_minimum_time: campaign.hits)
      campaign.hits.stub(:country_stats).and_return([OpenStruct.new(country: "Chile", total: '300'), OpenStruct.new(country: "Spain", total: '1'), OpenStruct.new(country: "USA", total: '1')])
      campaign.stub(views_count: 302)
      create(:hit_with_end_video, campaign: campaign)
      expect(campaign.hit_country_stats).to match_array [{label: "Chile", value: (300/302.0*100).round(1)}, {label: "Other", value: (2/302.0*100).round(1)}]
    end
  end


  describe "#impression_country_stats" do
    let(:campaign) {create(:campaign_with_balance)}
    it "calls impressions.country_stats" do
      create(:impression, campaign: campaign)
      campaign.reload
      campaign.impressions.should_receive(:country_stats) { [] }
      campaign.impression_country_stats
    end

    it "returns hash for 0 Country" do
      campaign.impressions.stub(:country_stats).and_return([])
      expect(campaign.impression_country_stats).to be_nil
    end

    it "returns hash for 1 Country" do
      campaign.impressions.stub(:country_stats).and_return([OpenStruct.new(country: "Chile", total: '1')])
      campaign.stub(impressions_count: 1)
      create(:impression, campaign: campaign)
      expect(campaign.impression_country_stats).to match_array [{label: "Chile", value: 100.0}]
    end

    it "returns hash for 2 Countries" do
      campaign.impressions.stub(:country_stats).and_return([OpenStruct.new(country: "Chile", total: '2'), OpenStruct.new(country: "Spain", total: '2')])
      campaign.stub(impressions_count: 4)
      create(:impression, campaign: campaign)
      expect(campaign.impression_country_stats).to match_array [{label: "Chile", value: 50.0}, {label: "Spain", value: 50.0}]
    end

    it "returns hash for 2 Countries, one with less 1%" do
      campaign.impressions.stub(:country_stats).and_return([OpenStruct.new(country: "Chile", total: '300'), OpenStruct.new(country: "Spain", total: '1'), OpenStruct.new(country: "USA", total: '1')])
      campaign.stub(impressions_count: 302)
      create(:impression, campaign: campaign)
      expect(campaign.impression_country_stats).to match_array [{label: "Chile", value: (300/302.0*100).round(1)}, {label: "Other", value: (2/302.0*100).round(1)}]
    end
  end

  describe "#video_id" do 
    it "calls YT::get_id with campaign.url" do
      campaign = build_stubbed(:campaign)
      YT.should_receive(:get_id).with(campaign.video_url)
      campaign.video_id
    end
  end

  describe "#increase_influencer_views_count" do
    it "increase influencer views count" do
      campaign = create(:campaign)
      expect{ campaign.increase_influencer_views_count; campaign.reload }.to change{ campaign.influencer_views_count }.by(1)
    end
  end


  describe "#remaining_views" do
    let(:campaign) { build_stubbed(:campaign) }
    before(:each) do
      campaign.stub(hit_cost: 0.08)
    end
    it "returns 1 if balance equals hit_cost" do
      campaign.balance = 0.08
      expect(campaign.remaining_views).to eq 1
    end

    it "returns 2 if balance is twice hit_cost" do
      campaign.balance = 0.17
      expect(campaign.remaining_views).to eq 2
    end
  end

  describe "#influencer_payment" do
    let(:campaign) { build_stubbed(:campaign) }

    it "returns 5.6 for hit_cost 0.08" do
      campaign.stub(hit_cost: 0.08)
      expect(campaign.influencer_payment).to eq "5.6 &cent;"
    end    

    it "returns 6.3 for hit_cost 0.09" do
      campaign.stub(hit_cost: 0.09)
      expect(campaign.influencer_payment).to eq "6.3 &cent;"
    end    
  end
end
