require 'spec_helper'

describe InfluencerVideo do
  it {should belong_to(:campaign) }
  it {should belong_to(:user) }
  it {should validate_presence_of(:user)}
  it {should validate_presence_of(:campaign)}
  it { should validate_uniqueness_of(:campaign_id).scoped_to(:user_id) }
  it {should respond_to(:user_wallet)}
  it {should have_many(:influencer_impressions)}
  it {should have_many(:influencer_views)}


  it {InfluencerVideo.any_instance.stub(set_token: true); should validate_presence_of(:token)}

  describe "#share_url" do
    it "returns url without locale" do
      iv = build_stubbed(:influencer_video, token: 'abcd')
      expect(iv.share_url).to eq "http://www.example.com/iv/abcd/"
    end

    it "returns url2 without locale" do
      iv = build_stubbed(:influencer_video, token: 'abcdf')
      expect(iv.share_url).to eq "http://www.example.com/iv/abcdf/"
    end
  end

  describe "#increase_view_count" do
    it "increase view count" do
      influencer_video = create(:influencer_video)
      expect{ influencer_video.increase_view_count; influencer_video.reload }.to change{ influencer_video.views_count }.by(1)
    end    
  end

  describe "#earnings" do
    it "returns 0.08 for 1 influencer view" do
      influencer_video = create(:influencer_video)
      create(:influencer_view, influencer_video: influencer_video, elapsed_time: 30)
      expect(influencer_video.earnings).to eq (0.08*InfluencerView::INFLUENCER_PERCENTAGE).round(3)
    end

    it "returns 0.16 for 2 influencer view" do
      influencer_video = create(:influencer_video)
      create_list(:influencer_view, 2, influencer_video: influencer_video, elapsed_time: 30)
      expect(influencer_video.earnings).to eq (0.16*InfluencerView::INFLUENCER_PERCENTAGE).round(3)
    end
  end


end
