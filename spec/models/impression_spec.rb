require 'spec_helper'

describe Impression do
  it {should belong_to :campaign}
  it {should belong_to :site}
  it {should validate_presence_of :campaign}
  it {should validate_presence_of :site}
  it {should validate_presence_of :ip}
  it {should allow_mass_assignment_of(:country)}

  it "calls set_country before_create" do
    Impression.any_instance.should_receive(:set_country)
    create(:impression)
  end

  describe "#set_country" do
    it "sets country to Chile for ip 190.163.101.3" do
      GeoIPWrap.stub(get_country: "Chile")
      impression = create(:impression, ip: "190.163.101.3")
      expect(impression.country).to eq "Chile"
    end

    it "sets country to Chile for ip 85.55.48.148" do
      GeoIPWrap.stub(get_country: "Spain")
      impression = create(:impression, ip: "85.55.48.148")
      expect(impression.country).to eq "Spain"
    end
  end

  describe ".country_stats" do
    it "returns impression with country Chile and total = 100" do
      GeoIPWrap.stub(get_country: "Chile")
      create_list(:impression, 2)
      expect(Impression.country_stats.first.country).to eq "Chile"
      expect(Impression.country_stats.first.total).to eq "2"
    end

    it "returns impression with country Spain and total = 50 and Chile and total = 50" do
      GeoIPWrap.stub(get_country: "Spain")
      create(:impression)
      GeoIPWrap.stub(get_country: "Chile")
      create(:impression)
      expect(Impression.country_stats.first.country).to eq "Chile"
      expect(Impression.country_stats.first.total).to eq "1"
      expect(Impression.country_stats[1].country).to eq "Spain"
      expect(Impression.country_stats[1].total).to eq "1"
    end
  end
end
