require 'spec_helper'

describe PaymentNotification do
  it {should belong_to(:campaign)}
  it {should validate_presence_of(:transaction_id)}
  it {should validate_presence_of(:campaign)}
  it {should validate_presence_of(:params)}
  it {should validate_presence_of(:status)}
  it {should allow_mass_assignment_of(:amount)}
  it {should validate_presence_of(:amount)}

  it "calls increase_campaign_balance" do
  	campaign = create(:campaign)
  	PaymentNotification.any_instance.should_receive(:increase_campaign_balance)
  	PaymentNotification.create(params: 'blabla', transaction_id: 'asd', 
  		campaign_id: campaign.id, status: 'Completed', amount: 80)
  end

  describe '#increase_campaign_balance' do
  	it "increases balance by 1000" do
  		campaign = create(:campaign)
  		payment_notification = build_stubbed(:payment_notification, campaign: campaign, amount: 1000, params: {hit_cost: 0.07})
  		expect{payment_notification.increase_campaign_balance}.to change { campaign.balance }.by(1000)
  	end
    it "increases balance by 20" do
      campaign = create(:campaign)
      payment_notification = build_stubbed(:payment_notification, campaign: campaign, amount: 20, params: {hit_cost: 0.07})
      expect{payment_notification.increase_campaign_balance}.to change { campaign.balance }.by(20)
    end
    it "increases balance by 270" do
      campaign = create(:campaign)
      payment_notification = build_stubbed(:payment_notification, campaign: campaign, amount: 270, params: {hit_cost: 0.07})
      expect{payment_notification.increase_campaign_balance}.to change { campaign.balance }.by(270)
    end

    it "doesn't increase if values < 20" do
      campaign = create(:campaign)
      payment_notification = build_stubbed(:payment_notification, campaign: campaign, amount: 10, params: {hit_cost: 0.07})
      expect{payment_notification.increase_campaign_balance}.to_not change { campaign.balance }
    end

    it "doesn't increase if hit_cost < 0.06" do
      campaign = create(:campaign)
      payment_notification = build_stubbed(:payment_notification, campaign: campaign, amount: 30, params: {hit_cost: 0.04})
      expect{payment_notification.increase_campaign_balance}.to_not change { campaign.hit_cost }
    end

    it "save hit_cost" do
      campaign = create(:campaign)
      payment_notification = build_stubbed(:payment_notification, campaign: campaign, amount: 40, params: {hit_cost: 0.09})
      expect{payment_notification.increase_campaign_balance}.to change { campaign.hit_cost }.to(0.09)
    end

  end
end
