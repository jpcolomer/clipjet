require 'spec_helper'

describe Site do

  it {should belong_to(:user)}
  it {should have_many(:hits)}
  it {should have_many(:impressions)}

  it { should_not allow_value("test").for(:url) }
  it { should_not allow_value(nil).for(:url) }

  it {should validate_presence_of(:user_id)}

  it 'is valid' do
    expect(build(:site)).to be_valid
  end

  it "doesn't allow 2 sites with same token" do
    Site.any_instance.stub(:set_token).and_return(true)
    create(:site, token: '12345')
    expect(build(:site, token: '12345')).to_not be_valid
  end

  it "doesn't allow a site without a token" do
    Site.any_instance.stub(:set_token).and_return(true)
    expect(build(:site)).to_not be_valid
  end

  it "doesn't change token after update" do
    site = create(:site)
    token = site.token
    site.update_attributes(url: 'http://colomer.jp')
    expect(site.token).to eq token
  end

  describe '#widget_div' do
    it "returns div" do
      site = build_stubbed(:site)
      widget_string = "<div id=\"clipjet-video\" data-lang=\"en\" data-category=\"1\" width=\"200px\" height=\"200px\" data-site-token=\"#{site.token}\"></div>"
      expect(site.widget_div(lang: 'en', category: 1, width: 200, height: 200)).to eq widget_string
    end

    it "returns div with width 300 height 300 and category 2" do
      site = build_stubbed(:site)
      widget_string = "<div id=\"clipjet-video\" data-lang=\"es\" data-category=\"2\" width=\"300px\" height=\"300px\" data-site-token=\"#{site.token}\"></div>"
      expect(site.widget_div(lang: 'es', category: 2, width: 300, height: 300)).to eq widget_string
    end
  end

  describe '#widget_script' do
    it "returns script" do
      site = build_stubbed(:site)
      widget_script =  
    '''<script type="text/javascript">
  (function() {
    var cj = document.createElement("script"); cj.type = "text/javascript"; cj.async = true;
    cj.src = "http://widget.clipjet.me/clipjet.js";
    var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(cj, s);
  })();
</script>'''
      expect(site.widget_script).to eq widget_script
    end
  end

  describe '#widget_iframe' do
    it "returns iframe with width 300 and height 300 and category 2 and lang en" do
      site = build_stubbed(:site)
      iframe = "<iframe src=\"//#{API_CONFIG['host']}/sites/#{site.token}/widget?width=300&height=300&lang=en&category=2\" width=\"300\" height=\"330\" frameborder=\"0\" scrolling=\"no\"></iframe>"
      expect(site.widget_iframe(lang: 'en', category: 2, width: 300, height: 300)).to eq iframe
    end

    it "returns iframe with width 400 and height 400 and category 1 and lang es" do
      site = build_stubbed(:site)
      iframe = "<iframe src=\"//#{API_CONFIG['host']}/sites/#{site.token}/widget?width=400&height=400&lang=es&category=1\" width=\"400\" height=\"430\" frameborder=\"0\" scrolling=\"no\"></iframe>"
      expect(site.widget_iframe(lang: 'es', category: 1, width: 400, height: 400)).to eq iframe
    end
  end

  describe '#views' do
    it "returns 2 for site" do
      site = create(:site)
      create_list(:hit_with_end_video, 2, site: site)
      expect(site.views).to eq 2
    end

    it "returns 3 for site" do
      site = create(:site)
      create(:hit, elapsed_time: 3, site: site)
      campaign = create(:campaign, video_duration: 18)
      create(:hit, campaign: campaign, elapsed_time: 18, site: site)
      campaign = create(:campaign, video_duration: 100)
      create(:hit, campaign: campaign, elapsed_time: 31, site: site)
      create(:hit_with_end_video, site: site)
      expect(site.views).to eq 3
    end
  end

  describe '#balance' do

    it "returns SITE_PERCENTAGE with one hit" do
      site = create(:site)
      campaign = create(:campaign_with_balance)
      campaign.hit_cost = 0.08; campaign.save
      create(:hit_with_end_video, site: site, campaign: campaign)
      expect(site.balance).to eq (0.08*Hit::SITE_PERCENTAGE)
    end

    it "returns 2*SITE_PERCENTAGE with two hits" do
      site = create(:site)
      campaign = create(:campaign_with_balance)
      campaign.hit_cost = 0.08; campaign.save
      create_list(:hit_with_end_video, 2, site: site, campaign: campaign)
      expect(site.balance).to eq (2*0.08*Hit::SITE_PERCENTAGE)
    end

    it "returns 0 if no valid hit" do
      site = create(:site)
      create(:hit, site: site)
      expect(site.balance).to eq 0      
    end
  end


  describe '#earnings' do

    it "returns SITE_PERCENTAGE with one hit" do
      site = create(:site)
      campaign = create(:campaign_with_balance)
      campaign.hit_cost = 0.08; campaign.save
      create(:hit_with_end_video, site: site, campaign: campaign)
      expect(site.earnings).to eq (0.08*Hit::SITE_PERCENTAGE).round(3)
    end

    it "returns 2*SITE_PERCENTAGE with two hits" do
      site = create(:site)
      campaign = create(:campaign_with_balance)
      create(:hit_with_end_video, site: site, campaign: campaign)
      campaign = create(:campaign_with_balance)
      campaign.stub(hit_cost: 0.10)
      create(:hit_with_end_video, site: site, campaign: campaign)
      expect(site.earnings).to eq ((0.08 + 0.10)*Hit::SITE_PERCENTAGE).round(3)
    end

    it "returns 0 if no valid hit" do
      site = create(:site)
      create(:hit, site: site)
      expect(site.earnings).to eq 0      
    end
  end


  describe '#view_stats' do
    let(:site) {create(:site)}
    it "returns json of 1 view" do
      create(:hit, site: site)
      hit = create(:hit_with_end_video, site: site, created_at: Time.zone.now)
      expect(site.view_stats).to match_array [{created_at: hit.created_at.to_date, value: 1}]
    end

    it "returns json of 2 views with different created dates" do
      create(:hit, site: site)
      hit = create(:hit_with_end_video, site: site,created_at: Time.zone.now)
      hit2 = create(:hit_with_end_video, site: site, created_at: 1.day.ago)
      create(:hit_with_end_video, site: site, created_at: 1.day.ago)
      expect(site.view_stats).to match_array [{created_at: hit2.created_at.to_date, value: 2}, {created_at: hit.created_at.to_date, value: 1}]
    end

    it "returns json of 2 views with different created dates and 0 for intermediate date" do
      create(:hit, site: site)
      hit = create(:hit_with_end_video, site: site, created_at: Time.zone.now)
      hit2 = create(:hit_with_end_video, site: site, created_at: 2.day.ago)
      create(:hit_with_end_video, site: site, created_at: 2.day.ago)
      expect(site.view_stats).to match_array [{created_at: 2.day.ago.to_date, value: 2},{created_at: 1.day.ago.to_date, value: 0} ,{created_at: hit.created_at.to_date, value: 1}]
    end
  end

  describe '#earning_stats' do
    let(:site) {create(:site)}

    it "returns json of 1 view" do
      create(:hit, site: site)
      hit = create(:hit_with_end_video, site: site, created_at: Time.zone.now)
      expect(site.earning_stats).to match_array [{created_at: Time.zone.now.to_date, value: (1*Hit::SITE_PERCENTAGE*Campaign::HIT_COST).round(3)}]
    end

    it "returns json of 2 views with different created dates" do
      create(:hit, site: site)
      hit = create(:hit_with_end_video, site: site,created_at: Time.zone.now)
      hit2 = create(:hit_with_end_video, site: site, created_at: 1.day.ago)
      create(:hit_with_end_video, site: site, created_at: 1.day.ago)
      expect(site.earning_stats).to match_array [{created_at: 1.day.ago.to_date, value: (2*Hit::SITE_PERCENTAGE*Campaign::HIT_COST).round(3)}, {created_at: Time.zone.now.to_date, value: (1*Hit::SITE_PERCENTAGE*Campaign::HIT_COST).round(3)}]
    end

    it "returns json of 2 views with different created dates and 0 for intermediate date" do
      create(:hit, site: site)
      hit = create(:hit_with_end_video, site: site, created_at: Time.zone.now)
      hit2 = create(:hit_with_end_video, site: site, created_at: 2.day.ago)
      create(:hit_with_end_video, site: site, created_at: 2.day.ago)
      expect(site.earning_stats).to match_array [{created_at: 2.days.ago.to_date, value: (2*Hit::SITE_PERCENTAGE*Campaign::HIT_COST).round(3)}, {created_at: 1.day.ago.to_date, value: 0}, {created_at: Time.zone.now.to_date, value: (1*Hit::SITE_PERCENTAGE*Campaign::HIT_COST).round(3)}]
    end
  end

  describe "#hit_country_stats" do
    let(:site) {create(:site)}
    it "calls hits.country_stats" do
      create(:hit_with_end_video, site: site)
      site.hits.should_receive(:with_minimum_campaign_time) { site.hits }
      site.hits.should_receive(:country_stats) { [] }
      site.hit_country_stats
    end

    it "returns hash for 0 Country" do
      site.hits.stub(with_minimum_campaign_time: site.hits)
      site.hits.stub(:country_stats).and_return([])
      create(:hit_with_end_video, site: site)
      expect(site.hit_country_stats).to match_array []
    end

    it "returns hash for 1 Country" do
      site.hits.stub(with_minimum_campaign_time: site.hits)
      site.hits.stub(:country_stats).and_return([OpenStruct.new(country: "Chile", total: '1')])
      site.stub(views_count: 1)
      create(:hit_with_end_video, site: site)
      expect(site.hit_country_stats).to match_array [{label: "Chile", value: 100}]
    end

    it "returns hash for 2 Countries" do
      site.hits.stub(with_minimum_campaign_time: site.hits)
      site.hits.stub(:country_stats).and_return([OpenStruct.new(country: "Chile", total: '2'), OpenStruct.new(country: "Spain", total: '2')])
      site.stub(views_count: 4)
      create(:hit_with_end_video, site: site)
      expect(site.hit_country_stats).to match_array [{label: "Chile", value: 50}, {label: "Spain", value: 50}]
    end

    it "returns hash for 2 Countries, one with less 1%" do
      site.hits.stub(with_minimum_campaign_time: site.hits)
      site.hits.stub(:country_stats).and_return([OpenStruct.new(country: "Chile", total: '300'), OpenStruct.new(country: "Spain", total: '1'), OpenStruct.new(country: "USA", total: '1')])
      site.stub(views_count: 302)
      create(:hit_with_end_video, site: site)
      expect(site.hit_country_stats).to match_array [{label: "Chile", value: (300/302.0*100).round(1)}, {label: "Other", value: (2/302.0*100).round(1)}]
    end
  end
end