require 'spec_helper'

describe Hit do
  it {should belong_to(:campaign)}
  it {should belong_to(:site)}
  it {should validate_presence_of(:campaign)}
  it {should validate_presence_of(:site)}
  it {should validate_presence_of(:article_url)}
  it {should allow_value('2.25').for(:elapsed_time)}
  it {should allow_mass_assignment_of(:user_agent)}


  context "stub set_view_cost" do
    before(:each) do 
      Hit.any_instance.stub(set_view_cost: true)
    end
    it { should validate_presence_of(:view_cost)}
    it { should validate_numericality_of(:view_cost)}
  end


  it "doesn't allow 2 hits with same token" do
    Hit.any_instance.stub(:set_token).and_return(true)
    create(:hit, token: '12345')
    expect(build(:hit, token: '12345')).to_not be_valid
  end

  it "doesn't allow a hit without a token" do
    Hit.any_instance.stub(:set_token).and_return(true)
    expect(build(:hit)).to_not be_valid
  end

  it "doesn't change token after update" do
    hit = create(:hit)
    token = hit.token
    hit.update_attributes(article_url: 'http://colomer.jp')
    expect(hit.token).to eq token
  end

  it 'sets token' do
    hit = create(:hit)
    expect(hit.token).to be_a String
  end

  it 'saves to the DB' do
    expect{create(:hit)}.to change{ Hit.count }.by(1)
  end

  it 'should have same site token' do
    site = create(:site)
    hit = create(:hit, site: site)
    expect(hit.site_token).to eq site.token
    expect(hit.site).to eq site
    expect(site.hits).to include hit
  end


  it 'should have same campaign token' do
    campaign = create(:campaign)
    hit = create(:hit, campaign: campaign)
    expect(hit.campaign_token).to eq campaign.token
    expect(hit.campaign).to eq campaign
    expect(campaign.hits).to include hit
  end

  it "can't change ip after creation" do
    hit = create(:hit)
    hit.update_attributes(ip: '123')
    expect(hit).to have(1).error_on(:ip)
  end

  it "can't change user_agent after creation" do
    hit = create(:hit)
    hit.update_attributes(user_agent: '123')
    expect(hit).to have(1).error_on(:user_agent)
  end

  it "can't change site_token after creation" do
    hit = create(:hit)
    hit.update_attributes(site_token: '123')
    expect(hit).to have(1).error_on(:site_token)
  end

  it "can't change article_url after creation" do
    hit = create(:hit)
    hit.update_attributes(article_url: '123')
    expect(hit).to have(1).error_on(:article_url)
  end

  it "can't change campaign_token after creation" do
    hit = create(:hit)
    hit.update_attributes(campaign_token: '123')
    expect(hit).to have(1).error_on(:campaign_token)
  end

  it "accepts 2.25 as elapsed_time" do
    hit = create(:hit)
    expect{hit.update_attributes(elapsed_time: 2.25)}.to change { hit.elapsed_time }.from(nil).to(2.25)
  end

  context "given same ip and same site_token" do
    context "with time < 1 hour ago & elapsed_time < campaign.minimum_time" do
      let(:hit) { create(:hit, created_at: 1.hour.ago + 10, elapsed_time: 1) }
      subject { build(:hit, ip: hit.ip, site_token: hit.site_token ) }

      it { should be_valid } 
    end

    context "with time < 1 hour ago & elapsed_time > campaign.minimum_time" do
      let(:hit) { create(:hit_with_end_video, created_at: 1.hour.ago + 10) }
      subject { build(:hit, ip: hit.ip, site_token: hit.site_token ) }

      it { should_not be_valid }      
    end

    context "with time > 1 hour" do
      let(:hit) { create(:hit, created_at: 1.hour.ago - 10) }
      subject { build(:hit, ip: hit.ip, site_token: hit.site_token ) }

      it { should be_valid }               
    end
  end

  it "is not valid for same ip and same campaign twice if last hit is valid" do
    hit = create(:hit, elapsed_time: 30)
    hit2 = build(:hit, ip: hit.ip, campaign_token: hit.campaign_token )
    expect(hit2).to_not be_valid
  end

  it "is valid for different ip and same campaign twice if last hit is valid" do
    hit = create(:hit, elapsed_time: 30)
    hit2 = build(:hit, campaign_token: hit.campaign_token )
    expect(hit2).to be_valid
  end

  it "is not valid for same ip and same campaign twice on influencer view" do
    i_video1 = create(:influencer_video)
    iv = create(:influencer_view, influencer_video: i_video1, elapsed_time: 31)
    hit = build(:hit, ip: iv.ip, campaign: i_video1.campaign )
    expect(hit).to_not be_valid
  end

  it "is valid for different ip and same campaign twice on influencer view" do
    i_video1 = create(:influencer_video)
    iv = create(:influencer_view, influencer_video: i_video1, elapsed_time: 31)
    hit = build(:hit, campaign: i_video1.campaign )
    expect(hit).to be_valid
  end


  it "is valid for same ip and same campaign twice on influencer view" do
    i_video1 = create(:influencer_video)
    iv = create(:influencer_view, influencer_video: i_video1, elapsed_time: 0.1)
    hit = build(:hit, ip: iv.ip, campaign: i_video1.campaign )
    expect(hit).to be_valid
  end

  it "calls once increase_user_credit after hit update" do
    hit = create(:hit)
    expect(hit.elapsed_time).to be_nil
    hit.stub(reduce_campaign_balance: true)
    hit.should_receive(:increase_user_credit).once
    hit.update_attributes(elapsed_time: 30)
    hit.update_attributes(elapsed_time: 35)
  end

  it "calls reduce_campaign_balance after hit update" do
    hit = create(:hit)
    hit.should_receive(:reduce_campaign_balance).once
    hit.update_attributes(elapsed_time: 30)
    hit.update_attributes(elapsed_time: 35)
  end



  it "calls set_country before_create" do
    Hit.any_instance.should_receive(:set_country)
    create(:hit)
  end

  describe "#set_country" do
    it "sets country to Chile for ip 190.163.101.3" do
      GeoIPWrap.stub(get_country: "Chile")
      hit = create(:hit, ip: "190.163.101.3")
      expect(hit.country).to eq "Chile"
    end

    it "sets country to Chile for ip 85.55.48.148" do
      GeoIPWrap.stub(get_country: "Spain")
      hit = create(:hit, ip: "85.55.48.148")
      expect(hit.country).to eq "Spain"
    end
  end

  describe "#site_owner_wallet" do

    it "is same Wallet as site owner" do
      hit = build_stubbed(:hit)
      wallet = double('wallet')
      user = double('user')
      user.stub(wallet: wallet)
      site = double('site')
      site.stub(user: user)
      hit.stub(site: site)

      expect(hit.site_owner_wallet).to be hit.site.user.wallet
    end
  end

  describe "#increase_user_credit" do
    let(:hit) { build_stubbed(:hit) }
    let(:wallet) { double('wallet', increase_balance: true) }

    before :each do
      hit.stub(:site_owner_wallet).and_return(wallet)
    end

    it "calls site_owner_wallet.increase_user_credit with 10*SITE_PERCENTAGE" do
      hit.campaign.stub(hit_cost: 10)
      hit.site_owner_wallet.should_receive(:increase_balance).with(10*Hit::SITE_PERCENTAGE)
      hit.increase_user_credit
    end

    it "calls site_owner_wallet.increase_user_credit with 20*SITE_PERCENTAGE" do
      hit.campaign.stub(hit_cost: 20)
      hit.site_owner_wallet.should_receive(:increase_balance).with(20*Hit::SITE_PERCENTAGE)
      hit.increase_user_credit
    end
  end


  describe "#update_count" do
    let(:hit) {create(:hit)}
    it "increases campaign count" do
      expect{hit.update_count}.to change{hit.campaign.views_count}.by(1)
    end
    it "increases site count" do
      expect{hit.update_count}.to change{hit.site.views_count}.by(1)
    end
  end

  describe "#charge_pay_update_count" do
    let(:hit) {build_stubbed(:hit)}
    it "calls charge_pay_balance" do
      hit.should_receive(:charge_pay_balance)
      hit.charge_pay_update_count
    end

    it "calls update_count" do
      hit.stub(charge_pay_balance: true)
      hit.should_receive(:update_count)
      hit.charge_pay_update_count
    end

    it "doesn't call update_count if charge_pay_balance returns false" do
      hit.stub(charge_pay_balance: false)
      hit.should_not_receive(:update_count)
      hit.charge_pay_update_count
    end
  end


  describe '#charge_pay_balance' do
    let(:hit) { build_stubbed(:hit) }
    it "calls reduce_campaign_balance" do
      hit.should_receive(:reduce_campaign_balance)
      hit.charge_pay_balance
    end

    it "calls increase_user_credit if reduce_campaign_balance is true" do
      hit.stub(reduce_campaign_balance: true)
      hit.should_receive(:increase_user_credit)
      hit.charge_pay_balance
    end

    it "doesn't call increase_user_credit if reduce_campaign_balance is false" do
      hit.stub(reduce_campaign_balance: false)
      hit.should_not_receive(:increase_user_credit)
      hit.charge_pay_balance
    end
  end

  describe "#reduce_campaign_balance" do
    let(:hit) { build_stubbed(:hit) }
    it "calls campaign.reduce_balance" do
      campaign = double("campaign")
      campaign.should_receive(:reduce_balance) { true }
      hit.stub(campaign: campaign)
      hit.reduce_campaign_balance
    end
  end

  describe "#set_view_cost" do
    it "sets view_cost to campaign hit cost * SITE_PERCENTAGE" do
      Campaign::SITE_PERCENTAGE = 0.7
      campaign = build_stubbed(:campaign)
      campaign.stub(hit_cost: 1)
      hit = build_stubbed(:hit)
      hit.stub(campaign: campaign)
      hit.send(:set_view_cost)
      expect(hit.view_cost).to eq 0.7
    end
  end

  describe '.with_minimum_time' do
    it "returns hit with elapsed time > 10" do
      hit = create(:hit, elapsed_time: 11)
      create(:hit, elapsed_time: 5)
      expect(Hit.with_minimum_time(10)).to match_array [hit]
    end

    it "returns hit with elapsed time >= 20" do
      hit = create(:hit, elapsed_time: 20)
      create(:hit, elapsed_time: 5)
      expect(Hit.with_minimum_time(20)).to match_array [hit]
    end

    it "returns empty array" do
      expect(Hit.with_minimum_time(10)).to match_array []
    end
  end

  describe '.valid' do
    it "returns hit with elapsed time" do
      hit = create(:hit_with_elapsed_time)
      expect(Hit.valid).to match_array [hit]
    end

    it "doesn't return hit without elapsed time" do
      create(:hit_with_elapsed_time)
      hit = create(:hit)
      expect(Hit.valid).to_not include hit
    end
  end

  describe ".with_minimum_campaign_time" do
    it "returns hit with elapsed_time > 30 & elapsed_time > campaign.minimum_time" do
      hit = create(:hit, elapsed_time: 30)
      campaign = create(:campaign_with_balance)
      hit2 = create(:hit, elapsed_time: campaign.minimum_time)
      create(:hit)
      expect(Hit.with_minimum_campaign_time).to match_array [hit, hit2]
    end
    it "doesn't return hit without elapsed time and with elapsed time < minimum_time " do
      create(:hit_with_end_video)
      campaign = create(:campaign_with_balance)
      hit = create(:hit)
      hit2 = create(:hit, elapsed_time: campaign.minimum_time-1)
      expect(Hit.with_minimum_campaign_time).to_not include hit
      expect(Hit.with_minimum_campaign_time).to_not include hit2
    end
  end

  describe ".country_stats" do
    it "returns hit with country Chile and total = 100" do
      GeoIPWrap.stub(get_country: "Chile")
      create_list(:hit, 2)
      expect(Hit.country_stats.first.country).to eq "Chile"
      expect(Hit.country_stats.first.total).to eq "2"
    end

    it "returns hit with country Spain and total = 50 and Chile and total = 50" do
      GeoIPWrap.stub(get_country: "Spain")
      create(:hit)
      GeoIPWrap.stub(get_country: "Chile")
      create(:hit)
      expect(Hit.country_stats.first.country).to eq "Chile"
      expect(Hit.country_stats.first.total).to eq "1"
      expect(Hit.country_stats[1].country).to eq "Spain"
      expect(Hit.country_stats[1].total).to eq "1"
    end
  end
end
