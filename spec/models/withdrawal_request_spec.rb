require 'spec_helper'

describe WithdrawalRequest do
  it { should allow_mass_assignment_of :amount }

  it { should belong_to(:user)}

  context 'stub paypal_email' do
    before :each do
      WithdrawalRequest.any_instance.stub(user_has_paypal_email: true);
    end

    it { should validate_presence_of(:user)}
    it { should validate_presence_of(:amount)}
    it { should validate_numericality_of(:amount)}
  end

  it "is invalid with amount > user.wallet_balance" do
    wq = build(:withdrawal_request, amount: 101)
    expect(wq).to have_at_least(1).errors_on(:amount)
  end

  it "is invalid if user hasn't paypal_email" do
    user = create(:user)
    expect(build(:withdrawal_request, user: user)).to have_at_least(1).errors_on(:user)
  end

  it "is invalid with amount < 20" do
    wq = build(:withdrawal_request, amount: 19)
    expect(wq).to have_at_least(1).errors_on(:amount)
  end

  it "is valid" do
    wq = build(:withdrawal_request, amount: 20)
    expect(wq).to be_valid
  end

  it "calls deliver_notification_mails after creation" do
    WithdrawalRequest.any_instance.should_receive(:deliver_notification_mails)
    create(:withdrawal_request)
  end

  describe "#deliver_notification_mails" do
    it "calls UserMailer.withdrawal_request" do
      UserMailer.any_instance.should_receive(:withdrawal_request)
      wr = build_stubbed(:withdrawal_request)
      wr.send(:deliver_notification_mails)
    end

    it "calls Admin.withdrawal_request" do
      UserMailer.any_instance.should_receive(:withdrawal_request)
      AdminMailer.any_instance.should_receive(:withdrawal_request)
      wr = build_stubbed(:withdrawal_request)
      wr.send(:deliver_notification_mails)
    end
  end
end
