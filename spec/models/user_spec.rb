require 'spec_helper'

describe User do
  it {should have_many(:campaigns)}
  it {should have_many(:sites)}
  it {should have_one(:wallet)}
  it { should allow_mass_assignment_of :paypal_email }
  it {should have_many(:withdrawal_requests)}
  it 'creates wallet after creation' do
    user = create(:user)
    expect(user.wallet).to be_an_instance_of(Wallet)
  end

  describe '.wallet_balance' do
    it {should respond_to(:wallet_balance)}
    it "returns wallet balance" do
      user = create(:user)
      expect(user.wallet_balance).to eq user.wallet.balance
    end
  end

  describe '.publishers' do
    it "includes 1 user" do
      user = create(:user)
      create(:site, user: user)
      expect(User.publishers).to include user
    end
    it "includes 2 users" do
      create(:user)
      user = create(:user)
      create_list(:site, 2, user: user)
      site = create(:site)
      expect(User.publishers).to match_array [user, site.user]
    end
  end

  describe '.promoters' do
    it "includes 1 user" do
      user = create(:user)
      campaign = create(:campaign, user: user)
      create(:payment_notification, campaign: campaign)
      create(:campaign)
      expect(User.promoters).to match_array [user]
    end
    it "includes 2 users" do
      create(:campaign)
      create(:user)
      user = create(:user)
      campaigns = create_list(:campaign, 2, user: user)
      campaigns.each {|c| create(:payment_notification, campaign: c)}
      campaign = create(:campaign)
      create(:payment_notification, campaign: campaign)
      expect(User.promoters).to match_array [user, campaign.user]
    end
  end


  describe '.influencers' do
    it "includes 1 user" do
      user = create(:user)
      create(:influencer_video, user: user)
      expect(User.influencers).to include user
    end
    it "includes 2 users" do
      create(:user)
      user = create(:user)
      create_list(:influencer_video, 2, user: user)
      influencer_video = create(:influencer_video)
      expect(User.influencers).to match_array [user, influencer_video.user]
    end
  end


end
