require 'spec_helper'

describe Wallet do
  it {should belong_to(:user)}
  it {should validate_presence_of(:user_id)}
  it {should validate_presence_of(:balance)}
  it {should validate_numericality_of(:balance)}
  
  it 'has error for balance < 0' do
    expect(build(:wallet, balance: -1)).to have(1).errors_on(:balance)
  end

  it {should respond_to(:increase_balance)}
  describe "#increase_balance" do
    it "increases balance by 1" do
      wallet = create(:wallet)
      expect{wallet.increase_balance(1); wallet.reload}.to change{ wallet.balance }.by(1)
    end

    it "increases balance by 2" do
      wallet = create(:wallet)
      expect{wallet.increase_balance(2); wallet.reload}.to change{ wallet.balance }.by(2)
    end
  end
end
