require 'spec_helper'

describe InfluencerView do
  it { should belong_to(:influencer_video) }  
  it { should have_one(:campaign).through(:influencer_video) }

  it { should validate_presence_of(:ip)}
  it { should validate_presence_of(:influencer_video)}
  it {should allow_mass_assignment_of(:user_agent)}
  it {should allow_mass_assignment_of(:referer)}

  context "stub set_view_cost" do
    before(:each) do 
      InfluencerView.any_instance.stub(set_view_cost: true)
    end
    it { should validate_presence_of(:view_cost)}
    it { should validate_numericality_of(:view_cost)}
  end

  it "doesn't allow 2 influencer views with same token" do
    InfluencerView.any_instance.stub(:set_token).and_return(true)
    create(:influencer_view, token: '12345')
    expect(build(:influencer_view, token: '12345')).to_not be_valid
  end

  it "doesn't allow a influencer_view without a token" do
    InfluencerView.any_instance.stub(:set_token).and_return(true)
    expect(build(:influencer_view)).to_not be_valid
  end

  it "doesn't allow a influencer_view without an ip" do
    expect(build(:influencer_view, ip: nil)).to_not be_valid
  end

  it "doesn't change token after update" do
    influencer_view = create(:influencer_view)
    token = influencer_view.token
    influencer_view.update_attributes(ip: 'http://colomer.jp')
    expect(influencer_view.token).to eq token
  end

  it "can't change ip after creation" do
    influencer_view = create(:influencer_view)
    influencer_view.update_attributes(ip: '123')
    expect(influencer_view).to have(1).errors_on(:ip)
  end

  it "can't change user_agent after creation" do
    influencer_view = create(:influencer_view)
    influencer_view.update_attributes(user_agent: '123')
    expect(influencer_view).to have(1).errors_on(:user_agent)
  end

  it "can't change referer after creation" do
    influencer_view = create(:influencer_view)
    influencer_view.update_attributes(referer: '123')
    expect(influencer_view).to have(1).errors_on(:referer)
  end


  it "can't change influencer_video_token after creation" do
    influencer_view = create(:influencer_view)
    influencer_view.update_attributes(influencer_video_token: '123')
    expect(influencer_view).to have(1).errors_on(:influencer_video_token)
  end

  it "is not valid for same ip and same campaign twice on influencer view" do
    i_video1 = create(:influencer_video)
    i_video2 = create(:influencer_video, campaign: i_video1.campaign)
    influencer_view = create(:influencer_view, elapsed_time: 31, influencer_video: i_video1)
    influencer_view2 = build(:influencer_view, ip: influencer_view.ip, influencer_video: i_video2 )
    expect(influencer_view2).to_not be_valid
  end

  it "is valid for different ip and same campaign twice on influencer view" do
    i_video1 = create(:influencer_video)
    i_video2 = create(:influencer_video, campaign: i_video1.campaign)
    influencer_view = create(:influencer_view, elapsed_time: 31, influencer_video: i_video1)
    influencer_view2 = build(:influencer_view, influencer_video: i_video2 )
    expect(influencer_view2).to be_valid
  end

  it "is not valid for same ip and same campaign twice on hit" do
    i_video1 = create(:influencer_video)
    hit = create(:hit, campaign: i_video1.campaign, elapsed_time: 31)
    influencer_view2 = build(:influencer_view, ip: hit.ip, influencer_video_token: i_video1.token )
    expect(influencer_view2).to_not be_valid
  end

  it "is valid for different ip and same campaign twice on hit" do
    i_video1 = create(:influencer_video)
    hit = create(:hit, campaign: i_video1.campaign, elapsed_time: 31)
    influencer_view2 = build(:influencer_view, influencer_video_token: i_video1.token )
    expect(influencer_view2).to be_valid
  end


  it "calls set_country before_create" do
    InfluencerView.any_instance.should_receive(:set_country)
    create(:influencer_view)
  end

  describe "#set_country" do
    it "sets country to Chile for ip 190.163.101.3" do
      GeoIPWrap.stub(get_country: "Chile")
      influencer_view = create(:influencer_view, ip: "190.163.101.3")
      expect(influencer_view.country).to eq "Chile"
    end

    it "sets country to Chile for ip 85.55.48.148" do
      GeoIPWrap.stub(get_country: "Spain")
      influencer_view = create(:influencer_view, ip: "85.55.48.148")
      expect(influencer_view.country).to eq "Spain"
    end
  end

  it "calls charge_pay_update_count after update" do
    iv = create(:influencer_view)
    iv.should_receive(:charge_pay_update_count)
    iv.update_attributes(elapsed_time: 30)
  end

  it "calls charge_pay_update_count after update if elapsed_time is video_duration" do
    campaign = create(:campaign_with_balance, video_duration: 25)
    i_video = create(:influencer_video, campaign: campaign)
    iv = create(:influencer_view, influencer_video: i_video)
    iv.should_receive(:charge_pay_update_count)
    iv.update_attributes(elapsed_time: 25)
  end

  it "doesn't call charge_pay_update_count after update if elapsed_time is not initially nil" do
    iv = create(:influencer_view, elapsed_time: 1)
    iv.should_not_receive(:charge_pay_update_count)
    iv.update_attributes(elapsed_time: 30)
  end

  it "doesn't call charge_pay_update_count after update if elapsed_time is less than minimum time" do
    campaign = create(:campaign_with_balance, video_duration: 50)
    i_video = create(:influencer_video, campaign: campaign)
    iv = create(:influencer_view, influencer_video: i_video)
    iv.should_not_receive(:charge_pay_update_count)
    iv.update_attributes(elapsed_time: 20)
  end

  it "doesn't call charge_pay_update_count after update if elapsed_time is less than minimum time" do
    campaign = create(:campaign_with_balance, video_duration: 25)
    i_video = create(:influencer_video, campaign: campaign)
    iv = create(:influencer_view, influencer_video: i_video)
    iv.should_not_receive(:charge_pay_update_count)
    iv.update_attributes(elapsed_time: 20)
  end

  describe "#charge_pay_update_count" do
    let(:influencer_view) {build_stubbed(:influencer_view)}
    it "calls charge_pay_balance" do
      influencer_view.should_receive(:charge_pay_balance)
      influencer_view.charge_pay_update_count
    end

    it "calls update_counts" do
      influencer_view.stub(charge_pay_balance: true)
      influencer_view.should_receive(:update_count)
      influencer_view.charge_pay_update_count
    end

    it "doesn't call update_count if charge_pay_balance returns false" do
      influencer_view.stub(charge_pay_balance: false)
      influencer_view.should_not_receive(:update_count)
      influencer_view.charge_pay_update_count
    end
  end

  describe "#charge_pay_balance" do
    let(:influencer_view) { build_stubbed(:influencer_view) }
    it "calls reduce_campaign_balance" do
      influencer_view.should_receive(:reduce_campaign_balance)
      influencer_view.charge_pay_balance
    end

    it "calls increase_influencer_balance if reduce_campaign_balance is true" do
      influencer_view.stub(reduce_campaign_balance: true)
      influencer_view.should_receive(:increase_influencer_balance)
      influencer_view.charge_pay_balance
    end

    it "doesn't call increase_influencer_balance if reduce_campaign_balance is false" do
      influencer_view.stub(reduce_campaign_balance: false)
      influencer_view.should_not_receive(:increase_influencer_balance)
      influencer_view.charge_pay_balance
    end    
  end

  describe "#reduce_campaign_balance" do
    let(:influencer_view) { build_stubbed(:influencer_view) }
    it "calls campaign.reduce_balance" do
      campaign = double("campaign")
      campaign.should_receive(:reduce_balance) { true }
      influencer_view.stub(campaign: campaign)
      influencer_view.reduce_campaign_balance
    end  
  end

  describe "#increase_influencer_balance" do
    let(:influencer_view) { build_stubbed(:influencer_view) }
    let(:wallet) { double('wallet', increase_balance: true) }

    before :each do
      influencer_view.stub(:influencer_wallet).and_return(wallet)
    end

    it "calls influencer_wallet.increase_influencer_balance with 10" do
      influencer_view.stub(view_cost: 10)
      influencer_view.influencer_wallet.should_receive(:increase_balance).with(10)
      influencer_view.increase_influencer_balance
    end

    it "calls influencer_wallet.increase_influencer_balance with 20" do
      influencer_view.stub(view_cost: 20)
      influencer_view.influencer_wallet.should_receive(:increase_balance).with(20)
      influencer_view.increase_influencer_balance
    end
  end

  describe "#influencer_wallet" do
    it "returns influencer wallet" do
      wallet = double "wallet"
      influencer_video = build_stubbed(:influencer_video)
      influencer_video.stub(user_wallet: wallet)
      influencer_view = build_stubbed(:influencer_view, influencer_video: influencer_video)
      expect(influencer_view.influencer_wallet).to eq wallet
    end
  end


  describe "#update_count" do
    let(:influencer_view) { build_stubbed(:influencer_view) }
    let(:campaign) { double "campaign" }
    let(:influencer_video) { double "influencer_video" }

    before(:each) do
      influencer_view.stub(campaign: campaign)
      influencer_view.stub(influencer_video: influencer_video)
    end

    it "call increase campaign influencer view count" do
      campaign.should_receive(:increase_influencer_views_count)
      influencer_video.stub(:increase_view_count)
      influencer_view.update_count
    end

    it "calls influencer increase_video count" do
      campaign.stub(:increase_influencer_views_count)
      influencer_video.should_receive(:increase_view_count)
      influencer_view.update_count
    end
  end


  describe "#set_view_cost" do
    it "sets view_cost to campaign hit cost * INFLUENCER_PERCENTAGE" do
      Campaign::INFLUENCER_PERCENTAGE = 0.7
      campaign = build_stubbed(:campaign)
      campaign.stub(hit_cost: 1)
      influencer_view = build_stubbed(:influencer_view)
      influencer_view.stub(campaign: campaign)
      influencer_view.send(:set_view_cost)
      expect(influencer_view.view_cost).to eq 0.7
    end
  end

  describe ".with_minimum_campaign_time" do
    it "returns influencer_view with elapsed_time > 30 & elapsed_time > campaign.video_duration" do
      influencer_view = create(:influencer_view, elapsed_time: 30)
      campaign = create(:campaign_with_balance, video_duration: 20)
      influencer_video = create(:influencer_video, campaign: campaign)
      influencer_view2 = create(:influencer_view, elapsed_time: 20, influencer_video: influencer_video)
      create(:influencer_view)
      expect(InfluencerView.with_minimum_campaign_time).to match_array [influencer_view, influencer_view2]
    end
    it "doesn't return influencer_view without elapsed time and with elapsed time < video_duration " do
      create(:influencer_view)
      campaign = create(:campaign_with_balance, video_duration: 20)
      influencer_video = create(:influencer_video, campaign: campaign)
      influencer_view = create(:influencer_view)
      influencer_view2 = create(:influencer_view, influencer_video: influencer_video, elapsed_time: influencer_video.campaign.video_duration-1)
      expect(InfluencerView.with_minimum_campaign_time).to_not include influencer_view
      expect(InfluencerView.with_minimum_campaign_time).to_not include influencer_view2
    end
  end
end
