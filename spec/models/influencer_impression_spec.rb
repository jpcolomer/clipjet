require 'spec_helper'

describe InfluencerImpression do
  it {should belong_to :influencer_video}
  it {should have_one(:campaign).through(:influencer_video)}
  it {should validate_presence_of :influencer_video}
  it {should validate_presence_of :ip}
  it {should allow_mass_assignment_of(:country)}
  it {should allow_mass_assignment_of(:referer)}

  it "calls set_country before_create" do
    InfluencerImpression.any_instance.should_receive(:set_country)
    create(:influencer_impression)
  end

  it "calls increment_campaign_counter after create" do
    impression = build(:influencer_impression)
    impression.should_receive(:increment_campaign_counter)
    impression.save
  end

  describe "#set_country" do
    it "sets country to Chile for ip 190.163.101.3" do
      GeoIPWrap.stub(get_country: "Chile")
      influencer_impression = create(:influencer_impression, ip: "190.163.101.3")
      expect(influencer_impression.country).to eq "Chile"
    end

    it "sets country to Chile for ip 85.55.48.148" do
      GeoIPWrap.stub(get_country: "Spain")
      influencer_impression = create(:influencer_impression, ip: "85.55.48.148")
      expect(influencer_impression.country).to eq "Spain"
    end
  end

  describe ".country_stats" do
    it "returns influencer_impression with country Chile and total = 100" do
      GeoIPWrap.stub(get_country: "Chile")
      create_list(:influencer_impression, 2)
      expect(InfluencerImpression.country_stats.first.country).to eq "Chile"
      expect(InfluencerImpression.country_stats.first.total).to eq "2"
    end

    it "returns influencer_impression with country Spain and total = 50 and Chile and total = 50" do
      GeoIPWrap.stub(get_country: "Spain")
      create(:influencer_impression)
      GeoIPWrap.stub(get_country: "Chile")
      create(:influencer_impression)
      expect(InfluencerImpression.country_stats.first.country).to eq "Chile"
      expect(InfluencerImpression.country_stats.first.total).to eq "1"
      expect(InfluencerImpression.country_stats[1].country).to eq "Spain"
      expect(InfluencerImpression.country_stats[1].total).to eq "1"
    end
  end

  describe "#increment_campaign_counter" do
    it "calls Campaign.increment_counter" do
      impression = create(:influencer_impression)
      Campaign.should_receive(:increment_counter).with('influencer_impressions_count', impression.influencer_video.campaign.id)
      impression.increment_campaign_counter
    end

    it "increments counter" do
      InfluencerImpression.skip_callback(:create, :after, :increment_campaign_counter)
      impression = create(:influencer_impression)
      expect {
        impression.increment_campaign_counter
        impression.campaign.reload
      }.to change{ impression.campaign.influencer_impressions_count }.by(1)
    end
  end
end
