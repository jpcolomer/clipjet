require 'spec_helper'

describe Category do
  it {should have_many(:campaigns)}

  describe ".name_like" do
    it "returns Art related categories for category Art Acting" do
      category = Category.first

      # Category.name_like(category.name).pluck(:name).each do |name|
      #   expect(name).to match /^Art/
      # end  

      expect(Category.name_like(category.name)).to be_nil
    end

    it "returns nil if there is no related Category" do
      expect(Category.name_like('Travel')).to be_nil
    end
  end

  describe "#similar_categories" do
    let (:category) { Category.first }
    
    it "returns array of similar categories' ids" do
      array_similar_category = [double("category"), double("category")]
      array_similar_category.stub(pluck: [2, 3])
      Category.stub(name_like: array_similar_category)
      expect(category.similar_categories).to match_array [2,3]
    end

    it "returns nil if no similar_category" do
      array_similar_category = nil
      Category.stub(name_like: nil)
      expect(category.similar_categories).to eq nil
    end
  end

  describe '#get_campaign' do
    let(:category) { Category.first }
    let(:ip) { '1234' }
    it "returns campaign" do
      campaign = create(:campaign_with_balance, category_id: category.id, language: 'en')
      expect(category.get_campaign('en', ip)).to eq campaign
    end

    it "returns campaign of similar category" do
      category_id = category.id+1
      campaign = create(:campaign_with_balance, category_id: category_id, language: 'en')
      category.stub(similar_categories: [category_id])
      expect(category.get_campaign('en', ip)).to eq campaign
    end

    it "returns nil if there is no campaign" do
      category.stub(similar_categories: nil)
      expect(category.get_campaign('en', ip)).to be_nil
    end

    it "doesn't get videos with balance < hit_cost" do
      campaign = create(:campaign, category_id: category.id, language: 'en')
      campaign.balance = 0.04
      campaign.save
      expect(category.get_campaign('en', ip)).to_not eq campaign
    end

    it "only gets videos with balance > hit_cost" do
      create(:campaign, category_id: category.id)
      campaign = create(:campaign_with_balance, language: 'en')
      expect(category.get_campaign('en', ip)).to eq campaign
       
    end

    it "returns campaign with language en" do
      campaign = create(:campaign_with_balance, category_id: category.id, language: 'en')
      create(:campaign_with_balance, category_id: category.id, language: 'es')
      expect(category.get_campaign('en', ip)).to eq campaign
    end

    it "returns campaign with language es" do
      create(:campaign_with_balance, category_id: category.id, language: 'en')
      campaign = create(:campaign_with_balance, category_id: category.id, language: 'es')
      expect(category.get_campaign('es', ip)).to eq campaign
    end

    context "given a language" do

      context "given an ip" do
        it "runs get_campaign without campaign that was already seen" do
          campaign = create(:campaign_with_balance, category_id: category.id, language: 'es')
          campaign2 = create(:campaign_with_balance, category_id: category.id, language: 'es')
          create(:hit, campaign: campaign, ip: ip)
          expect(category.get_campaign('es', ip)).to eq campaign2
        end

        it "runs get_campaign without campaign that was already seen and returns campaign with other ip hit" do
          campaign = create(:campaign_with_balance, category_id: category.id, language: 'es')
          create(:campaign_with_balance, category_id: category.id+1, language: 'es')
          campaign2 = create(:campaign_with_balance, category_id: category.id, language: 'es')
          create(:hit, campaign: campaign, ip: ip)
          create(:hit, campaign: campaign2)
          expect(category.get_campaign('es', ip)).to eq campaign2
        end
      end


      it "returns campaign of similar category if there is no campaign on category 1" do
        campaign = create(:campaign_with_balance, category_id: category.id+1, language: 'es')
        # create(:campaign_with_balance, category_id: category.id+2, language: 'es')
        expect(category.get_campaign('es', ip)).to eq campaign
      end

      it "returns random campaign with language es if there is no campaign on category 1 or similar category in any language" do
        campaign = create(:campaign_with_balance, category_id: category.id+2, language: 'es')
        create(:campaign_with_balance, category_id: category.id+2, language: 'en')
        expect(category.get_campaign('es', ip)).to eq campaign
      end

      it "returns random campaign of same category if there is no campaign on es" do
        campaign = create(:campaign_with_balance, category_id: category.id, language: 'en')
        create(:campaign_with_balance, category_id: category.id+2, language: 'en')
        expect(category.get_campaign('es', ip)).to eq campaign
      end

      it "returns random campaign of similar category if there is no campaign on es" do
        campaign = create(:campaign_with_balance, category_id: category.id+1, language: 'en')
        # create(:campaign_with_balance, category_id: category.id+2, language: 'en')
        expect(category.get_campaign('es', ip)).to eq campaign
      end

      it "returns random campaign if no similar category in any language" do
        campaign = create(:campaign_with_balance, category_id: category.id+2, language: 'en')
        expect(category.get_campaign('es', ip)).to eq campaign
      end
    end

  end
end