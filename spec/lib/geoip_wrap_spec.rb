require 'spec_helper'
require './lib/geoip_wrap.rb'

describe GeoIPWrap do
  it "returns Chile for ip 190.163.101.3" do
    expect(GeoIPWrap.get_country("190.163.101.3")).to eq "Chile"
  end

  it "returns Spain for ip 85.55.48.148" do
    expect(GeoIPWrap.get_country("85.55.48.148")).to eq "Spain"
  end

  it "returns nil if ip 127.0.0.1" do
    expect(GeoIPWrap.get_country("127.0.0.1")).to be_nil
  end

  it "returns nil if ip 192.168.0.15" do
    expect(GeoIPWrap.get_country("192.168.0.15")).to be_nil
  end

  it "returns nil if ip nil" do
    expect(GeoIPWrap.get_country(nil)).to be_nil
  end
end