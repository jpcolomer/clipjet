require 'json'
require 'ostruct'
require 'active_support/all'
require 'rack'
require './lib/yt_client.rb'

describe YT do

  let(:hash) { {'data' => {'duration' => '100', 'viewCount' => '10', 'title' => 'test', 'description' => 'test desc',
        'thumbnail' => {'hqDefault' => 'test.jpg'} }} }
  describe '.get_info' do
    before(:each) do
      YT.stub(api_call: hash)
      YT.stub(json_to_struct: true)
    end
    it "calls get_id" do
      YT.should_receive(:get_id).with('http://www.youtube.com/watch?v=asdf')
      YT.get_info('http://www.youtube.com/watch?v=asdf')
    end

    it "calls api_url" do
      YT.should_receive(:api_url).with('asdf')
      YT.get_info('http://www.youtube.com/watch?v=asdf')
    end

    it "calls api_call" do
      YT.should_receive(:api_call).with('https://gdata.youtube.com/feeds/api/videos/asdf?v=2&alt=jsonc')
      YT.get_info('http://www.youtube.com/watch?v=asdf')
    end

    it "calls json_to_struct" do
      YT.should_receive(:json_to_struct)
      YT.get_info('http://www.youtube.com/watch?v=asdf')
    end

  end


  describe '.json_to_struct' do
    it "returns struct" do
      hash = {'title' => 'hola', 'description' => 'test desc', 'duration' => '100', 'viewCount' => '10', 'id' => 'abcd',
       'thumbnail' => {'hqDefault' => 'hq.jpg', 'sqDefault' => 'sq.jpg'}}
      expect(YT.json_to_struct(hash.merge({'blabla' => 'bleble'}))).to eq OpenStruct.new({'title' => 'hola', 'description' => 'test desc', 'duration' => 100,
       'id' => 'abcd','viewCount' => '10', 'thumbnail' => 'hq.jpg', 'small_thumbnail' => 'sq.jpg'})
    end

    it "returns struct2" do
      hash = {'title' => 'hola2', 'description' => 'test desc2', 'duration' => '1002', 'viewCount' => '102', 'id' => 'abcdf',
       'thumbnail' => {'hqDefault' => 'hq2.jpg', 'sqDefault' => 'sq2.jpg'}}
      expect(YT.json_to_struct(hash.merge({'blabla' => 'bleble'}))).to eq OpenStruct.new({'title' => 'hola2', 'description' => 'test desc2', 'duration' => 1002,
       'id' => 'abcdf','viewCount' => '102', 'thumbnail' => 'hq2.jpg', 'small_thumbnail' => 'sq2.jpg'})
    end
  end

  describe '.api_call' do
    it "returns hash with api info" do
      RestClient.stub(:get).and_return(hash.to_json)
      call = YT.api_call('https://gdata.youtube.com/feeds/api/videos/Dz2xATNazL0?v=2&alt=jsonc')
      expect(call).to have_key('data')
      expect(call['data']).to have_key('title')
      expect(call['data']).to have_key('description')
      expect(call['data']).to have_key('duration')
      expect(call['data']).to have_key('viewCount')
      expect(call['data']).to have_key('thumbnail')
      expect(call['data']['thumbnail']).to have_key('hqDefault')
    end
  end

  describe '.get_id' do
    it "returns asdf for http://www.youtube.com/watch?v=asdf" do
      expect(YT.get_id('http://www.youtube.com/watch?v=asdf')).to eq 'asdf'
    end

    it "returns qwerty for http://www.youtube.com/watch?v=qwerty" do
      expect(YT.get_id('http://www.youtube.com/watch?v=qwerty')).to eq 'qwerty'
    end

    it "returns qwerty for http://youtube.com/watch?v=qwerty" do
      expect(YT.get_id('http://youtube.com/watch?v=qwerty')).to eq 'qwerty'
    end

    it "returns qwerty for http://youtube.com/watch?v=qwerty&blablabla" do
      expect(YT.get_id('http://youtube.com/watch?v=qwerty&blablabla')).to eq 'qwerty'
    end

    it "returns UJxTzV-RIf0 for http://www.youtube.com/watch?v=UJxTzV-RIf0" do
      expect(YT.get_id('http://www.youtube.com/watch?v=UJxTzV-RIf0')).to eq 'UJxTzV-RIf0'
    end
  end

  describe '.api_url' do
    it "returns https://gdata.youtube.com/feeds/api/videos/asdf?v=2&alt=jsonc for asdf" do
      expect(YT.api_url('asdf')).to eq 'https://gdata.youtube.com/feeds/api/videos/asdf?v=2&alt=jsonc'
    end

    it "returns https://gdata.youtube.com/feeds/api/videos/asdf1?v=2&alt=jsonc for asdf1" do
      expect(YT.api_url('asdf1')).to eq 'https://gdata.youtube.com/feeds/api/videos/asdf1?v=2&alt=jsonc'
    end

    it "returns https://gdata.youtube.com/feeds/api/videos/UJxTzV-RIf0?v=2&alt=jsonc for UJxTzV-RIf0" do
      expect(YT.api_url('UJxTzV-RIf0')).to eq 'https://gdata.youtube.com/feeds/api/videos/UJxTzV-RIf0?v=2&alt=jsonc'
    end
  end
end