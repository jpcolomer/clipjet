require 'spec_helper'
require 'rake'

describe 'hit:fill_view_cost' do
  describe do
    before(:all) do
      load File.join(Rails.root, 'lib', 'tasks', 'fill_existed_hit_view_cost.rake')
      Rake::Task.define_task(:environment)
      Hit.skip_callback(:validation, :before, :set_view_cost)
    end

    after(:all) do
      Hit.set_callback(:validation, :before, :set_view_cost)
    end

    it "sets view cost" do
      hit = build(:hit)
      hit.save(validate: false)
      expect{ 
        Rake::Task["hit:fill_view_cost"].invoke
        hit.reload
      }.to change{ hit.view_cost }.from(nil).to(0.056)
    end

    it "doesn't set view cost if view_cost is not nil" do
      hit = build(:hit)
      hit.view_cost = 0.056
      hit.save
      Hit.any_instance.should_not_receive :set_view_cost
      Rake::Task["hit:fill_view_cost"].invoke
    end
  end
end