require './lib/duration_encryptor.rb'
require 'openssl'
require 'base64'

describe DurationEncryptor do
  describe ".encrypted_video_duration" do
    ## need to stub methods
    it "returns KtXh765SKTo=\n for duration 10" do
      expect(DurationEncryptor.encrypt(10,'blah')).to eq "KtXh765SKTo=\n"
    end

    it "returns SYQYi6JycSk=\n for duration 11" do
      expect(DurationEncryptor.encrypt(11,'blah')).to eq "SYQYi6JycSk=\n"
    end
  end

  describe ".decrypt_video_duration" do
    ## need to stub methods
    it "returns 10 for encrypt KtXh765SKTo=\n" do
      expect(DurationEncryptor.decrypt("KtXh765SKTo=\n", 'blah')).to eq '10'
    end

    it "returns 11 for encrpy SYQYi6JycSk=\n)" do
      expect(DurationEncryptor.decrypt("SYQYi6JycSk=\n", 'blah')).to eq '11'
    end
  end
end