require 'spec_helper'
require 'rake'

describe 'campaign:balance_record_fix' do
  describe do
    before(:all) do
      load File.join(Rails.root, 'lib', 'tasks', 'fix_campaign_balance.rake')
      Rake::Task.define_task(:environment)
    end

    it "sets campaign balance" do
      c = create(:campaign)
      c.credit = 100 
      c.save
      c1 = create(:campaign)
      c1.credit = 1000
      c1.hit_cost = 0.09
      c1.save
      Rake::Task["campaign:balance_record_fix"].invoke
      c.reload; c1.reload
      expect(c.balance.to_i).to eq 8
      expect(c1.balance.to_i).to eq 90
    end
  end
end