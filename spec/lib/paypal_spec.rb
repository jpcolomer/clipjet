require 'spec_helper'
require 'ostruct'
require './lib/paypal.rb'

describe Paypal do
  describe '.set_values' do
    it "returns hash of values" do
      campaign = OpenStruct.new(id: 1)
      hash = {
      business: 'clipjet-test@clipjet.me',
      cmd: '_xclick',
      upload: 1,
      return: 'a',
      custom: 1,
      rm: 1,
      no_shipping: 1,
      amount: 20,
      item_name: 'ClipJet Views',
      quantity: 1,
      notify_url: 'b',
      cert_id: 'AGLFPQWLHT9JW'
    }
      expect(Paypal.set_values('a', 'b', campaign, 20)).to eq hash
    end

    it "returns hash of values2" do
      campaign = OpenStruct.new(id: 2)
      hash = {
      business: 'clipjet-test@clipjet.me',
      cmd: '_xclick',
      upload: 1,
      return: 'a1',
      custom: 2,
      amount: 80,
      rm: 1,
      no_shipping: 1,
      item_name: 'ClipJet Views',
      quantity: 1,
      notify_url: 'b1',
      cert_id: 'AGLFPQWLHT9JW'
    }
      expect(Paypal.set_values('a1', 'b1', campaign, 80)).to eq hash
    end
  end

  describe '.encrypted_values' do
    it "returns values encrypted" do
      Paypal.stub(encrypt: 'blabla')
      campaign = OpenStruct.new(id: 2)
      expect(Paypal.encrypted_values('a1', 'b1', campaign, 80)).to eq 'blabla'
    end

    it "calls values encrypt" do
      campaign = OpenStruct.new(id: 2)
      hash = {
        business: 'clipjet-test@clipjet.me',
        cmd: '_xclick',
        upload: 1,
        return: 'a1',
        custom: 2,
        amount: 200,
        rm: 1,
        no_shipping: 1,
        item_name: 'ClipJet Views',
        quantity: 1,
        notify_url: 'b1',
        cert_id: 'AGLFPQWLHT9JW'
      }
      Paypal.should_receive(:encrypt).with(hash)
      Paypal.encrypted_values('a1', 'b1', campaign, 200)
    end
  end

  describe '.url' do
    it "calls encrypt" do
      campaign = OpenStruct.new(id: 2)
      hash = {
        business: 'clipjet-test@clipjet.me',
        cmd: '_xclick',
        upload: 1,
        return: 'a1',
        custom: 2,
        amount: 20,
        rm: 1,
        no_shipping: 1,
        item_name: 'ClipJet Views',
        quantity: 1,
        notify_url: 'b1',
        cert_id: 'AGLFPQWLHT9JW'
      }
      Paypal.should_receive(:encrypt).with(hash)
      Paypal.url('a1', 'b1', campaign, 20)
    end

    it "returns  https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_s-xclick&encrypted=b" do
      Paypal.stub(set_values: true)
      Paypal.stub(encrypt: 'b')
      expect(Paypal.url(nil,nil,nil,nil)).to eq "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_s-xclick&encrypted=b"
    end

    it "returns  https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_s-xclick&encrypted=c" do
      Paypal.stub(set_values: true)
      Paypal.stub(encrypt: 'c')
      expect(Paypal.url(nil,nil,nil,nil)).to eq "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_s-xclick&encrypted=c"
    end
  end
end