require 'spec_helper'
require 'rake'

describe 'impression:ip_to_country' do

  before do
    load File.join(Rails.root, 'lib', 'tasks', 'set_country_from_impression_ip.rake')
    Rake::Task.define_task(:environment)
    Impression.skip_callback(:create, :before, :set_country)
  end

  after do
    Impression.set_callback(:create, :before, :set_country)
  end

  it "sets impression country" do
    impression = create(:impression, ip: '190.163.101.3')
    Impression.any_instance.should_receive :set_country
    Rake::Task["impression:ip_to_country"].invoke(0,10000)
  end

  it "doesn't set impression country if country is not nil" do
    impression = create(:impression, country: 'Suazilandia')
    Impression.any_instance.should_not_receive :set_country
    Rake::Task["impression:ip_to_country"].invoke(0,10000)
  end

end