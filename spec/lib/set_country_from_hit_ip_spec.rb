require 'spec_helper'
require 'rake'

describe 'hit:ip_to_country' do
  describe do
    before(:all) do
      load File.join(Rails.root, 'lib', 'tasks', 'set_country_from_hit_ip.rake')
      Rake::Task.define_task(:environment)
      Hit.skip_callback(:create, :before, :set_country)
    end

    after(:all) do
      Hit.set_callback(:create, :before, :set_country)
    end

    it "sets hit country" do
      hit = create(:hit, ip: '190.163.101.3')
      Hit.any_instance.should_receive :set_country
      Rake::Task["hit:ip_to_country"].invoke
    end

    it "doesn't set hit country if country is not nil" do
      hit = create(:hit, country: 'Suazilandia')
      Hit.any_instance.should_not_receive :set_country
      Rake::Task["hit:ip_to_country"].invoke
    end
  end
end