module PromoteMacros
  def goes_to_promote_video
    click_link 'PROFILE'
    click_link 'Promoter'
    click_link 'Promote Video'    
  end

  def registrate_video(video_url)
    expect(current_path).to eq new_video_registration_path(locale: :en)
    expect(page).to have_css 'a.active', text: 'Promote Video'
    expect(page).to have_css '.hero', text: 'PROMOTE VIDEO'
    expect(page).to have_css  'legend.active', text: '1'
    fill_in "video_url", with: video_url
    click_button 'ADD VIDEO'
  end

  def check_video_field(video)
    expect(current_path).to eq new_campaign_path(locale: :en)
    expect(find('#campaign_video_url').value).to eq video
  end
  
  def enter_video_url(video)
    fill_in "video_url", with: video
    click_on "GO VIRAL"    
  end

  def fill_campaign_details(name, url, language, category, video_duration, description='')
    expect(page).to have_css 'h2', text: 'Design your Campaign'
    within '#new_campaign' do
      expect(find('#video_duration').value).to eq DurationEncryptor.encrypt(video_duration)
      fill_in "Campaign name", with: name
      fill_in "Link URL", with: url
      fill_in "Description", with: description
      select language, from: 'Please select one language'
      select category, from: 'Please select one category'
      expect{
        click_on 'CREATE CAMPAIGN'
      }.to change(Campaign, :count).by(1)
    end    
  end


  def goes_to_manage_campaigns
    click_link 'PROFILE'
    click_link 'Promoter'
    click_link 'Manage Campaigns'
    expect(current_path).to eq campaigns_path(locale: :en)    
  end

  def user_see_campaign(campaign)
    expect(page).to have_css '.active', text: 'Manage Campaigns'
    expect(page).to have_css '.hero', text: 'YOUR CAMPAIGNS'
    within "#campaign_#{campaign.id}" do
      find(".small-thumbnail img[src='/assets/sq.jpg']")
      expect(page).to have_css '.name', text: 'test'
      expect(page).to have_css '.balance', text: '999'
      expect(page).to have_css '.impressions', text: '1'
      expect(page).to have_css '.views', text: '1'
    end
  end

end