module AuthenticationMacros
  def sign_in_as(user)
    visit root_path(locale: :en)
    click_link 'LOGIN'
    fill_in "Your Email", with: user.email
    fill_in "Your Password", with: user.password
    click_button 'LET ME IN!'
  end

  def sign_in
    user = create(:user, email: 'person@example.com', password: '12345678')
    sign_in_as(user)
  end

  def log_in(user)
    fill_in "Your Email", with: user.email
    fill_in "Your Password", with: user.password
    click_button 'LET ME IN!'
  end
  
  
end