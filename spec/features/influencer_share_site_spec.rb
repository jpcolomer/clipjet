require 'spec_helper'

feature "Influencer Share Site" do
  scenario "User watch video" do
    campaign = create(:campaign_with_balance, name: 'bla', description: 'desc test', site_url: 'http://example.com')
    iv = create(:influencer_video, campaign: campaign)
    expect {
        visit shared_video_path(id: iv.token)
    }.to change(InfluencerImpression, :count).by(1)
    expect(page).to have_css 'h1', text: 'bla'
    expect(page).to have_css 'p', text: 'desc test'
    expect(find("meta[property='og:type']", visible: false)[:content]).to eq 'website'
    expect(find("meta[property='og:url']", visible: false)[:content]).to eq iv.share_url
    expect(find("meta[property='og:image']", visible: false)[:content]).to eq campaign.thumbnail
    expect(find("meta[property='og:title']", visible: false)[:content]).to eq "#{campaign.name} - Video"
    expect(find("meta[property='og:description']", visible: false)[:content]).to eq campaign.description
    expect(find("meta[name='twitter:card']", visible: false)[:content]).to eq "summary"
    expect(find('#clipjet-video')[:'data-video']).to eq campaign.video_id
    expect(find('#clipjet-video')[:'data-influencer-video']).to eq iv.token
    expect(page).to have_css '.information', text: "For more information visit example.com"
  end
end