require 'spec_helper'

feature "View homepage" do
  scenario "User sees relevant information" do
    visit root_path
    expect(page).to have_css '.hero h1', text: 'THE VIRAL BUTTON FOR YOUR VIDEOS'
  end

  scenario "User goes to homepage" do
    visit publisher_path
    within 'header' do
      click_link 'Promote Your Videos'
    end
    expect(current_path).to eq root_path(locale: :en)
  end

  scenario "User goes to homepage by clicking on footer link" do
    visit publisher_path
    within 'footer' do
      click_link 'Promote Your Videos'
    end
    expect(current_path).to eq root_path(locale: :en)
  end

end