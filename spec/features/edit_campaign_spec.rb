require 'spec_helper'
require './lib/yt_client.rb'

feature "Edit Campaigns" do
  scenario "User edits his campaign" do
    hash = {'data' => {'duration' => '100', 'viewCount' => '10', 'title' => 'test', 
      'thumbnail' => {'hqDefault' => 'test.jpg', 'sqDefault' => 'small_test.jpg'} }}
    YT.stub(api_call: hash)
    user = create(:user)
    campaign = create(:campaign_with_balance, name: 'test', small_thumbnail: 'sq.jpg' ,user: user)
    sign_in_as(user)
    goes_to_manage_campaigns
    within "#campaign_#{campaign.id} .edit" do
      click_link 'EDIT'
    end
    expect(current_path).to eq edit_campaign_path(:en, campaign)
    expect(page).to have_css 'h2', text: 'Edit your Campaign'
    within '.edit_campaign' do
      fill_in "Campaign name", with: 'New Name'
      fill_in "Link URL", with: Faker::Internet.url
      select 'English', from: 'Please select one language'
      select Category.last.name, from: 'Please select one category'
      expect{
       click_button 'EDIT CAMPAIGN'
       campaign.reload
      }.to change{campaign.name}

    end
  end

  scenario "User try to edit anothers campaign" do
    user = create(:user)
    campaign = create(:campaign)
    sign_in_as(user)
    visit edit_campaign_path(:en, campaign)
    expect(current_path).to eq campaigns_path(:en)
  end

end