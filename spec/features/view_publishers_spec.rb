require 'spec_helper'

feature "View publishers site" do
  scenario "User goes to publishers sell site" do
    visit root_path
    within 'header' do
      click_link 'Become a Publisher'
    end
    # expect(page).to have_css 'li.active a', text: 'Become a Publisher'
    expect(page).to have_css '.hero h1', text: 'ROCKET YOUR EARNINGS'
  end

  scenario "User goes to publishers sell site by clicking link on footer" do
    visit root_path
    within 'footer' do
      click_link 'Become a Publisher'
    end
    # expect(page).to have_css 'li.active a', text: 'Become a Publisher'
    expect(page).to have_css '.hero h1', text: 'ROCKET YOUR EARNINGS'
  end
end