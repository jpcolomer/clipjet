require 'spec_helper'
require 'faker'
require './lib/yt_client.rb'

feature "Influencer Distribute Video" do
  before(:each) do
    hash = {'data' => {'duration' => '100', 'viewCount' => '10', 'title' => 'test', 
      'thumbnail' => {'hqDefault' => 'test.jpg', 'sqDefault' => 'small_test.jpg'} }}
    YT.stub(api_call: hash)
  end
  scenario "User picks a video" do
    campaign = create(:campaign_with_balance)
    sign_in
    click_link 'PROFILE'
    click_link 'Influencer'
    expect(current_path).to eq influencer_campaigns_path(:en)
    within 'header' do
      click_link 'Pick a Clip'
      expect(page).to have_css 'a.active', text: 'Pick a Clip'
    end    
    within "#campaign_#{campaign.id}" do
      expect(page).to have_css '.name', text: campaign.name
      expect(page).to have_css '.payment', text: "#{(Hit::SITE_PERCENTAGE*campaign.hit_cost*100).floor}"
      expect {
        click_button 'CHOOSE VIDEO'
      }.to change{ InfluencerVideo.count }.by(1)
    end
    expect(current_path).to eq influencer_video_path(:en, InfluencerVideo.last)

  end

  scenario "User picks a video already chosen by him" do
    user = create(:user)
    campaign = create(:campaign_with_balance)
    create(:influencer_video, campaign: campaign, user: user)
    sign_in_as(user)
    click_link 'PROFILE'
    click_link 'Influencer'
    within "#campaign_#{campaign.id}" do
      expect(page).to have_css '.name', text: campaign.name
      expect(page).to have_css '.payment', text: "#{(Hit::SITE_PERCENTAGE*campaign.hit_cost*100).floor}"
      expect {
        click_button 'CHOOSE VIDEO'
      }.to_not change{ InfluencerVideo.count }
    end
    expect(current_path).to eq influencer_video_path(:en, InfluencerVideo.last)

  end


  scenario "User can't see videos if not logged in" do
    visit influencer_campaigns_path
    expect(current_path).to eq new_user_session_path
  end
end