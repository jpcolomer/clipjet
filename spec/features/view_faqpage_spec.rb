require 'spec_helper'

feature "View FAQ" do
  scenario "User sees relevant information" do
    visit root_path(:en)
    within 'footer' do
      click_link 'FAQ'
    end
    expect(current_path).to eq faq_path(:en)
    expect(page).to have_css '.hero h1', text: 'FAQS'

  end

end