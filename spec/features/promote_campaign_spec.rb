require 'spec_helper'
require 'faker'
require './lib/yt_client.rb'

feature "Promote campaign" do
  before(:each) do
    hash = {'data' => {'duration' => '100', 'viewCount' => '10', 'title' => 'test', 'description' => 'test',
      'thumbnail' => {'hqDefault' => 'test.jpg', 'sqDefault' => 'small_test.jpg'} }}
    YT.stub(api_call: hash)
  end

  before(:all) do
    Paypal.define_singleton_method(:url) do |a,b,c,d|
      a
    end
  end

  after(:all) do
    Paypal.define_singleton_method(:url) do |return_url, notify_url, campaign, amount|
      set_values(return_url, notify_url, campaign, amount)
      api_url = "https://www.sandbox.paypal.com/cgi-bin/webscr?"
      api_url + {cmd: '_s-xclick', encrypted: encrypt(@values)}.to_query
    end
  end

  scenario "User registrates video url" do
    sign_in
    goes_to_promote_video
    registrate_video("http://www.youtube.com/watch?v=Dz2xATNazL0")
    check_video_field("http://www.youtube.com/watch?v=Dz2xATNazL0")
  end

  scenario "User can't registrate video without signing in" do
    visit new_video_registration_path
    expect(current_path).to eq new_user_session_path
  end

  scenario "User doesn't go to details without a valid video url" do
    sign_in
    goes_to_promote_video
    registrate_video('')
    expect(current_path).to eq new_video_registration_path(locale: :en)
    expect(page).to have_css 'p.error', text: "Video Url invalid format"
  end

  scenario "User redirects to video url registration if no session[:video_url]" do
    sign_in
    visit new_campaign_path
    expect(current_path).to eq new_video_registration_path(locale: :en)
  end
  
  scenario "User fills campaign details" do
    sign_in
    goes_to_promote_video
    registrate_video("http://www.youtube.com/watch?v=Dz2xATNazL0")
    get_video_info(100, 10)
    expect(find('#campaign_thumbnail').value).to eq 'test.jpg'
    expect(find('#campaign_small_thumbnail').value).to eq 'small_test.jpg'
    expect(find('#campaign_name').value).to eq 'test'
    expect(find('#campaign_description').value).to eq 'test'
    fill_campaign_details('Campaign test', Faker::Internet.url, 'English', Category.first.name, 100 )
  end

  scenario "User fills campaign details without site url" do
    sign_in
    goes_to_promote_video
    registrate_video("http://www.youtube.com/watch?v=Dz2xATNazL0")
    get_video_info(100, 10)
    expect(find('#campaign_thumbnail').value).to eq 'test.jpg'
    expect(find('#campaign_small_thumbnail').value).to eq 'small_test.jpg'
    expect(find('#campaign_name').value).to eq 'test'
    expect(find('#campaign_description').value).to eq 'test'
    fill_campaign_details('Campaign test', '', 'English', Category.first.name, 100, 'test description' )
  end

  context "javascript available", slow: true, js: true do
    scenario "User buy credits" do
      user = create(:user)
      campaign = create(:campaign, user: user)
      sign_in_as(user)
      visit new_campaign_credits_purchase_path(:en, campaign)
      expect(page).to have_css 'h2', text: "Buy Credits"
      expect(page).to have_css 'p', text: "You need to buy credits if you want your campaign to GO VIRAL"
      within '.payment-params' do
        fill_in "Set your Budget", with: 100
        fill_in "Set your cost per view", with: 0.09
        click_button 'Calculate views'
        expect(page).to have_css '.total-views', text: 1111
        expect(find('#budget')[:value]).to eq '99.99'
        expect(page).to have_css 'form button.clipjet-color', text: 'BUY CREDITS'
        expect(page).to_not have_css 'form button', text: 'Calculate views'
        fill_in "Set your Budget", with: 90
        expect(page).to_not have_css 'form button.clipjet-color', text: 'BUY CREDITS'
        click_button 'Calculate views'
        expect(page).to have_css 'form button.clipjet-color', text: 'BUY CREDITS'
        expect(page).to_not have_css 'form button', text: 'Calculate views'
        fill_in "Set your cost per view", with: 0.10
        expect(page).to_not have_css 'form button.clipjet-color', text: 'BUY CREDITS'
        expect(page).to have_css 'form button', text: 'Calculate views'
      end
    end    


    scenario "User tries to enter wrong values to budget" do
      user = create(:user)
      campaign = create(:campaign, user: user)
      sign_in_as(user)
      visit new_campaign_credits_purchase_path(:en, campaign)
      expect(page).to have_css 'h2', text: "Buy Credits"
      expect(page).to have_css 'p', text: "You need to buy credits if you want your campaign to GO VIRAL"
      within '.payment-params' do
        fill_in "Set your Budget", with: 0
        fill_in "Set your cost per view", with: 0.09
        click_button 'Calculate views'
        expect(page).to_not have_css 'form button.clipjet-color', text: 'BUY CREDITS'
        fill_in "Set your Budget", with: 'asgag'
        click_button 'Calculate views'
        expect(page).to_not have_css 'form button.clipjet-color', text: 'BUY CREDITS'
      end
    end 


    scenario "User tries to enter wrong values to hit_cost" do
      user = create(:user)
      campaign = create(:campaign, user: user)
      sign_in_as(user)
      visit new_campaign_credits_purchase_path(:en, campaign)
      expect(page).to have_css 'h2', text: "Buy Credits"
      expect(page).to have_css 'p', text: "You need to buy credits if you want your campaign to GO VIRAL"
      within '.payment-params' do
        fill_in "Set your Budget", with: 10
        fill_in "Set your cost per view", with: 0
        click_button 'Calculate views'
        expect(page).to_not have_css 'form button.clipjet-color', text: 'BUY CREDITS'
        fill_in "Set your cost per view", with: 'asgasfg'
        click_button 'Calculate views'
        expect(page).to_not have_css 'form button.clipjet-color', text: 'BUY CREDITS'
      end
    end 

  end

  scenario "User sees campaign completion with credits" do
    user = create(:user)
    sign_in_as(user)
    campaign = create(:campaign_with_balance, user: user)
    visit new_campaign_completion_path(campaign: campaign.token)
    expect(page).to have_css 'a.active', text: 'Promote Video'
    expect(page).to have_css '.hero', text: 'PROMOTE VIDEO'
    expect(page).to have_css 'h2', text: "Campaign '#{campaign.name}' created"
    expect(page).to have_css 'p', text: "Now you can watch your campaign's stats and buy more credits on Manage Campaigns section"
  end

  scenario "User sees campaign completion without credits" do
    user = create(:user)
    sign_in_as(user)
    campaign = create(:campaign, user: user)
    visit new_campaign_completion_path(campaign: campaign.token)
    expect(page).to have_css 'a.active', text: 'Promote Video'
    expect(page).to have_css '.hero', text: 'PROMOTE VIDEO'
    expect(page).to have_css 'h2', text: "Campaign '#{campaign.name}' created"
    expect(page).to have_css 'p', text: "You've succesfully created your campaign, but you didn't complete credits purchase. If you still want us to run your campaign, you need to buy credits on Manage Campaigns section"
  end

  scenario "User is redirected to login if not signed in" do
    campaign = create(:campaign)
    visit new_campaign_completion_path(campaign: campaign.token)
    expect(current_path).to eq new_user_session_path
  end


  def get_video_info(duration, count)
    expect(page).to have_css '.video_duration', text: Time.at(duration).utc.strftime("%H:%M:%S")
    expect(page).to have_css '#player'
    expect(page).to have_css '.video_views', text: count
  end

  def creates_new_campaign
    sign_in
    goes_to_promote_video
    registrate_video("http://www.youtube.com/watch?v=Dz2xATNazL0")
    get_video_info(100, 10)
    fill_campaign_details('Campaign test', Faker::Internet.url, 'English', Category.first.name, 100 )
  end
end