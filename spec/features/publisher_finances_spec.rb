require 'spec_helper'

feature "Publisher Finances" do
  scenario "User enters paypal email" do
    user = create(:user)
    user.wallet.increase_balance(100)
    goes_to_finances(user)
    check_finances_content(balance: 100)
    expect{
      fill_in "Paypal Email", with: 'test@example.com'
      click_button 'SAVE CHANGES'
      user.reload
    }.to change{user.paypal_email}.from(nil).to('test@example.com')
  end

  scenario "User enters blank paypal email" do
    user = create(:user)
    user.wallet.increase_balance(100)
    goes_to_finances(user)
    check_finances_content(balance: 100)
    expect{
      fill_in "Paypal Email", with: ''
      click_button 'SAVE CHANGES'
      user.reload
    }.to_not change{user.paypal_email}
  end

  scenario "User enters invalid paypal email" do
    user = create(:user)
    user.wallet.increase_balance(100)
    goes_to_finances(user)
    check_finances_content(balance: 100)
    expect{
      fill_in "Paypal Email", with: 'test'
      click_button 'SAVE CHANGES'
      user.reload
    }.to_not change{user.paypal_email}
  end


  scenario "User asks for withdrawal of 50" do
    reset_mailer
    user = create(:user_with_paypal)
    user.wallet.increase_balance(100)
    goes_to_finances(user)
    expect(find('#user_paypal_email').value).to eq user.paypal_email
    expect(page).to have_css 'h2', text: 'Withdrawal Request'
    expect{
      fill_in "Amount", with: 50
      click_button "SEND"
    }.to change(WithdrawalRequest, :count).by(1)
    expect(page).to have_content "Your withdrawal requests was succesfully sent"
    expect(open_last_email_for(user.paypal_email)).to have_subject 'ClipJet withdrawal request'
    expect(open_last_email_for('withdrawal@clipjet.me')).to have_subject 'New withdrawal request'

  end

  scenario "User asks for withdrawal > balance" do
    user = create(:user_with_paypal)
    user.wallet.increase_balance(100)
    goes_to_finances(user)
    expect(find('#user_paypal_email').value).to eq user.paypal_email
    expect(page).to have_css 'h2', text: 'Withdrawal Request'
    expect{
      fill_in "Amount", with: 1000
      click_button "SEND"
    }.to_not change(WithdrawalRequest, :count)
  end



  scenario "User is redirected to login" do
    visit finances_path(:en)
    expect(current_path).to eq new_user_session_path
  end

  def goes_to_finances(user)
    sign_in_as(user)
    click_link 'PROFILE'
    click_link 'Publisher'
    click_link 'Your Finances'    
  end

  def check_finances_content(args)
    balance = args[:balance]
    expect(page).to have_css 'a.active', text: "Your Finances"
    expect(page).to have_css '.hero', text: 'FINANCES'
    expect(page).to have_css 'h2', text: "Current Balance: #{ActionController::Base.helpers.number_to_currency(balance)}"    
  end

end