require 'spec_helper'
require 'faker'
require './lib/yt_client.rb'

feature "Manage Influencer Videos" do
  scenario "User visits his shared videos" do
    hash = {'data' => {'duration' => '100', 'viewCount' => '10', 'title' => 'test', 'description' => 'test desc',
        'thumbnail' => {'hqDefault' => 'test.jpg'} }}
    YT.stub(api_call: hash)

    user = create(:user)
    influencer_video = create(:influencer_video, user: user)
    create_list(:influencer_impression, 3, influencer_video: influencer_video)
    view = create(:influencer_view, influencer_video: influencer_video)
    view.update_attributes(elapsed_time: 30)
    influencer_video.reload
    sign_in_as(user)
    click_link 'PROFILE'
    click_link 'Influencer'
    click_link 'Your shared Videos'
    within 'header' do
      expect(page).to have_css 'a.active', text: 'Your shared Videos'
    end
    expect(page).to have_css '.hero', text: 'YOUR VIDEOS'
    within "#influencer_video_#{influencer_video.id}" do
      find(".small-thumbnail img[src='#{influencer_video.campaign.small_thumbnail}']")
      expect(page).to have_css '.campaign', text: influencer_video.campaign.name
      expect(page).to have_css '.payment', text: (influencer_video.campaign.hit_cost*InfluencerView::INFLUENCER_PERCENTAGE*100).round(1)
      expect(page).to have_css '.remaining-views', text: influencer_video.campaign.remaining_views
      expect(page).to have_css '.remaining-views', text: influencer_video.campaign.remaining_views
      expect(page).to have_css '.impressions', text: 3
      expect(page).to have_css '.views', text: 1
      expect(page).to have_css '.earnings', text: (InfluencerView::INFLUENCER_PERCENTAGE*influencer_video.campaign.hit_cost).round(3)
      click_link influencer_video.campaign.name
    end
    expect(current_path).to eq influencer_video_path(:en, influencer_video)
  end
end