require 'spec_helper'
require './lib/yt_client.rb'

feature "Go Viral" do
  before(:each) do
    YT.stub(api_call: {'data' => {'title' => 'hola2', 'duration' => '1002', 'viewCount' => '102',
       'thumbnail' => {'hqDefault' => 'hq2.jpg'}}})
  end
  scenario "User insert video url and Goes Viral" do
    sign_in
    enter_video_url("http://www.youtube.com/watch?v=RgRWbcebSJ0")
    check_video_field("http://www.youtube.com/watch?v=RgRWbcebSJ0")
  end

  scenario "User is redirected to sign in" do
    user = create(:user)
    visit root_path
    enter_video_url("http://www.youtube.com/watch?v=RgRWbcebSJ0")
    expect(current_path).to eq new_user_session_path
    log_in(user)
    check_video_field("http://www.youtube.com/watch?v=RgRWbcebSJ0")
  end


end