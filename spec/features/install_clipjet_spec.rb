require 'spec_helper'
require 'faker'

feature 'Install Clipjet' do

  scenario "User enters URL on publisher sale site" do
    sign_in
    within 'header' do
      click_link 'Become a Publisher'
    end
    fill_in "site[url]", with: Faker::Internet.url
    click_button 'JOIN US'
    expect(current_path).to eq site_widget_installs_path(:en, Site.last)
  end

  scenario "User registrates Site" do
    goes_to_install_clipjet
    check_content_new_site
    create_new_site(url: Faker::Internet.url)
    check_content_widget_install_for(Site.first)
  end

  scenario "User registrates Site in spanish" do
    goes_to_install_clipjet
    check_content_new_site
    create_new_site(url: Faker::Internet.url)
    visit site_widget_installs_path('es', Site.first)
    check_content_widget_install_for(Site.first, 'es')
  end

  scenario "User is redirected to login if not signed in and visits new site path " do
    visit new_site_path
    expect(current_path).to eq new_user_session_path
  end

  scenario "User is redirected to login if not signed in and visits widget installation path" do
    site = create(:site)
    visit site_widget_installs_path(:en, site)
    expect(current_path).to eq new_user_session_path
  end

  def goes_to_install_clipjet
    sign_in
    click_link "PROFILE"
    click_link "Publisher"
    click_link "Install ClipJet"
    expect(current_path).to eq new_site_path(locale: :en)
  end

  def check_content_new_site
    expect(page).to have_css 'a.active', text: 'Install ClipJet'
    expect(page).to have_css '.hero', text: 'INSTALL CLIPJET'
    expect(page).to have_css 'legend.active', text: '1'
    expect(page).to have_css 'h2', text: 'Add new Site'
  end

  def create_new_site(args)
    url = args[:url]
    fill_in "site[url]", with: url
    expect{
      click_button 'ADD SITE'
    }.to change(Site, :count).by(1)
  end

  def check_content_widget_install_for(site, lang='en')
    expect(current_path).to eq site_widget_installs_path(lang, site)
    expect(page).to have_css 'a.active', text: I18n.t('shared.nav_publisher.install_clipjet')
    expect(page).to have_css '.hero', text: I18n.t('shared.install_clipjet_hero.install_clipjet')
    expect(page).to have_css 'legend.active', text: '2'
    expect(page).to have_css 'h2', text: I18n.t('widget_installs.index.installation_instructions')
    expect(page).to have_css 'h3', text: I18n.t('widget_installs.index.select_a_category')
    expect(page).to have_css 'h3', text: I18n.t('widget_installs.index.select_your_site_language')
    expect(page).to have_css 'h3', text: I18n.t('widget_installs.index.select_width_and_height')
    expect(page).to have_css 'h3', text: I18n.t('widget_installs.index.copy_div_html')
    expect(page).to have_css '#div_language option[selected]', text: LANGUAGES.select{|arry| arry[1] == lang}.flatten[0]
    expect(page).to have_css 'pre', text: "<iframe src=\"//#{API_CONFIG['host']}/sites/#{site.token}/widget?width=200&height=200&lang=#{lang}&category=1\" width=\"200\" height=\"230\" frameborder=\"0\" scrolling=\"no\"></iframe>"
  end
end