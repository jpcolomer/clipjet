require 'spec_helper'

feature "Change Profile type" do
  scenario "by choosing from dropdown selects Promoter" do
    sign_in
    click_link 'PROFILE'
    click_link 'Promoter'
    expect(page).to have_content 'Promote Video'
    expect(page).to have_content 'Manage Campaigns'

  end
  scenario "by choosing from dropdwon selects Publisher" do
    sign_in
    click_link 'PROFILE'
    click_link 'Publisher'
    expect(page).to have_content 'Install ClipJet'
    expect(page).to have_content 'Account Stats'
    expect(page).to have_content 'Your Finances'
  end
end