require 'spec_helper'

feature "Show Site Earnings" do
  scenario "User should see chart" do
    user = create(:user)
    site = create(:site, user: user)
    create_list(:hit_with_end_video, 5, site: site)
    sign_in_as(user)
    within 'header' do
      click_link 'PROFILE'
      click_link 'Publisher'
    end
    click_link 'Account Stats'
    within "#site_#{site.id}" do
      find('.earnings a').click
    end
    expect(current_path).to eq site_earnings_path(:en, site)
    expect(page).to have_css 'a.active', text: 'Account Stats'
    expect(page).to have_css '.hero', text: 'SITE EARNINGS'
    expect(page).to have_css 'h2', text: site.url
  end

  scenario "User doesn't see chart" do
    user = create(:user)
    site = create(:site, user: user)
    sign_in_as(user)
    within 'header' do
      click_link 'PROFILE'
      click_link 'Publisher'
    end
    click_link 'Account Stats'
    within "#site_#{site.id}" do
      find('.earnings a').click
    end
    expect(current_path).to eq site_earnings_path(:en, site)
    expect(page).to have_css 'a.active', text: 'Account Stats'
    expect(page).to have_css '.hero', text: 'SITE EARNINGS'
    expect(page).to have_css 'h2', text: site.url
  end

  scenario "User can't see without signing in" do
    site = create(:site)
    visit site_earnings_path(:en, site)
    expect(current_path).to eq new_user_session_path
  end
end