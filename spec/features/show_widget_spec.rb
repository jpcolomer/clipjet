require 'spec_helper'
require './lib/yt_client.rb'

feature "Show Widget" do
  before :each do
    ip = Faker::Internet.ip_v4_address
    ActionDispatch::Request.any_instance.stub(remote_ip: ip)
    YT.stub(get_id: 'asdfg')
  end
  scenario "user sees player" do
    site = create(:site)
    campaign = create(:campaign_with_balance)
    expect{
      visit site_widget_path(site, width: 200, height: 300, language: 'en', category_id: campaign.category.id)
    }.to change(Impression, :count)
    expect(page).to have_css 'div#clipjet-video'
    expect(find('#clipjet-video')[:'data-video']).to eq 'asdfg'
    expect(find('#clipjet-video')[:'data-campaign']).to eq campaign.token
    expect(find('#clipjet-video')[:'data-site']).to eq site.token
    expect(find('#clipjet-video')[:'data-campaign-url']).to eq campaign.site_url
    expect(find('#clipjet-video')[:width]).to eq '200px'
    expect(find('#clipjet-video')[:height]).to eq '300px'
  end

end