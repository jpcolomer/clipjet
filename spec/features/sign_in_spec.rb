require 'spec_helper'

feature "Sign in as a user" do
  scenario "with email address and password" do
    create(:user, email: 'person@example.com', password: '12345678')
    visit root_path
    click_link 'LOGIN'
    expect(current_path).to eq new_user_session_path(locale: 'en')
    fill_in "user_email", with: "person@example.com"
    fill_in "user_password", with: "12345678"
    click_button 'LET ME IN!'
    expect(current_path).to eq root_path(locale: 'en')
  end
end