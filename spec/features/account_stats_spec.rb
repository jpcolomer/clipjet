require 'spec_helper'

feature "Publisher Account Stats" do
  scenario "User sees he's site stats" do
    user = create(:user)
    site = create(:site, user: user)
    create_list(:hit, 3, site: site)
    create_list(:hit_with_elapsed_time, 2, site: site)
    campaign = create(:campaign_with_balance)
    campaign.hit_cost = 0.09; campaign.save
    create(:hit_with_elapsed_time, site: site, campaign: campaign)
    sign_in_as(user)
    click_link 'PROFILE'
    click_link 'Publisher'
    click_link 'Account Stats'
    expect(current_path).to eq sites_path(locale: :en)
    expect(page).to have_css 'a.active', text: 'Account Stats'
    expect(page).to have_css '.hero', text: 'ACCOUNT STATS'
    within "#site_#{site.id}" do
      expect(page).to have_css '.site', text: site.url
      expect(page).to have_css '.views', text: '3'
      expect(page).to have_css '.earnings', text: "#{((2*80.0+90)/1000*Hit::SITE_PERCENTAGE).round(3)}"
      expect(find(".edit-player a")['href']).to eq site_widget_installs_path(:en, site)
    end
    expect(page).to have_css '.total-earnings', text: "#{((2*80.0+90)/1000*Hit::SITE_PERCENTAGE).round(3)}"
  end

  scenario "User is redirected to login if not signed in" do
    visit sites_path(locale: :en)
    expect(current_path).to eq new_user_session_path
  end
end