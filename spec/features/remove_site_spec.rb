require 'spec_helper'

feature "Remove Publisher Site" do
  scenario "User removes his site" do
    user = create(:user)
    site = create(:site, user: user)
    sign_in_as(user)
    click_link 'PROFILE'
    click_link 'Publisher'
    click_link 'Account Stats'

    within "#site_#{site.id}" do
      expect{
        click_link 'DELETE'
      }.to change{Site.count}.by(-1)
    end

  end

  scenario "User can't remove another's site" do
    user = create(:user)
    site = create(:site)
    sign_in_as(user)
    expect{
      page.driver.submit :delete, site_path(:en, site), {}
    }.to_not change{ Site.count }
  end
end