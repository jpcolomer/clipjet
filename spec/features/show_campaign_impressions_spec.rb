require 'spec_helper'

feature "Show Campaign Impressions" do
  scenario "User sees impressions chart" do
    user = create(:user)
    campaign = create(:campaign_with_balance, user: user)
    create_list(:impression, 5, campaign: campaign)
    sign_in_as(user)
    within 'header' do
      click_link 'PROFILE'
      click_link 'Promoter'
    end
    click_link 'Manage Campaigns'
    within "#campaign_#{campaign.id}" do
      find('.impressions a').click
    end
    expect(current_path).to eq campaign_impressions_path(:en, campaign)
    expect(page).to have_css 'a.active', text: 'Manage Campaigns'
    expect(page).to have_css '.hero', text: 'IMPRESSIONS'
    expect(page).to have_css 'h2', text: campaign.name
  end

  scenario "User doesn't see impressions chart" do
    user = create(:user)
    campaign = create(:campaign_with_balance, user: user)
    sign_in_as(user)
    within 'header' do
      click_link 'PROFILE'
      click_link 'Promoter'
    end
    click_link 'Manage Campaigns'
    within "#campaign_#{campaign.id}" do
      find('.impressions a').click
    end
    expect(current_path).to eq campaign_impressions_path(:en, campaign)
    expect(page).to have_css 'a.active', text: 'Manage Campaigns'
    expect(page).to have_css '.hero', text: 'IMPRESSIONS'
    expect(page).to have_css 'h2', text: campaign.name
  end

  scenario "User can't see without signing in" do
    campaign = create(:campaign_with_balance)
    visit campaign_impressions_path(:en, campaign)
    expect(current_path).to eq new_user_session_path
  end
end