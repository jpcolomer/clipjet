require 'spec_helper'

feature "User signs out" do
  scenario "by clicking link" do
    user = create(:user, email: 'person@example.com', password: '12345678')
    sign_in_as(user)
    click_link 'LOG OUT'
    expect(page).to_not have_content 'LOG OUT'
  end
end