require "spec_helper"

feature "Correct redirect after sign_in" do
	scenario "User watch promoter site, registrates and then is redirected to Promote video" do
		visit root_path(:en)
		click_link 'SIGN UP'
		fill_in "Your Email", with: "test@example.com"
		fill_in "Your Password", with: '12345678'
		fill_in "Password Confirmation", with: '12345678'
		click_button "LET ME IN!"
		expect(current_path).to eq new_video_registration_path(:en)
	end

	scenario "User watch publishers site, registrates and then is redirected to add site" do
		visit publisher_path(:en)
		click_link 'SIGN UP'
		fill_in "Your Email", with: "test@example.com"
		fill_in "Your Password", with: '12345678'
		fill_in "Password Confirmation", with: '12345678'
		click_button "LET ME IN!"
		expect(current_path).to eq new_site_path(:en)
	end

	scenario "User watch influencer site, registrates and then is redirected to choose video" do
		visit influencer_path(:en)
		click_link 'SIGN UP'
		fill_in "Your Email", with: "test@example.com"
		fill_in "Your Password", with: '12345678'
		fill_in "Password Confirmation", with: '12345678'
		click_button "LET ME IN!"
		expect(current_path).to eq influencer_campaigns_path(:en)
	end

	scenario "User has influencer videos, signs in and then is redirected to choose video" do
		user = create(:user)
		create(:influencer_video, user: user)
		visit root_path
		sign_in_as(user)
		expect(current_path).to eq influencer_campaigns_path(:en)
	end

	scenario "User has sites, signs in and then is redirected to add site" do
		user = create(:user)
		create(:site, user: user)
		visit root_path
		sign_in_as(user)
		expect(current_path).to eq sites_path(:en)
	end

	scenario "User has campaigns, signs in and then is redirected to add campaign" do
		user = create(:user)
		create(:campaign, user: user)
		visit root_path
		sign_in_as(user)
		expect(current_path).to eq campaigns_path(:en)
	end

end