require 'spec_helper'

feature "Sign up as a user" do
  scenario "with email address and password" do

    visit root_path
    goes_to_sign_up_page
    create_user("person@example.com")
    returns_to_home('en')
  end

  scenario "without password confirmation" do
    visit root_path
    goes_to_sign_up_page
    fill_form_wo_pwd_confirmation "person@example.com"
    doesnt_create_user
  end

  def goes_to_sign_up_page
    click_link 'SIGN UP'
    expect(current_path).to eq new_user_registration_path(locale: 'en')    
  end


  def fill_form_wo_pwd_confirmation(email)
    fill_in "Your Email", with: email
    fill_in "Your Password", with: "12345678"    
  end

  def doesnt_create_user
    expect{
      click_button 'LET ME IN!'
    }.to_not change { User.count }    
  end

  def create_user(email)
    fill_form_wo_pwd_confirmation email
    fill_in "Password Confirmation", with: "12345678"
    expect{
      click_button 'LET ME IN!'
    }.to change { User.count }.by(1)    
  end

  def returns_to_home(locale)
    expect(current_path).to eq root_path(locale: locale)
  end
end