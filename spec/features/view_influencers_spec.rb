require 'spec_helper'

feature "View influencers site" do
  scenario "User goes to influencers sell site" do
    visit root_path
    within 'header' do
      click_link 'Become an Influencer'
    end
    expect(page).to have_css '.hero h1', text: 'MAKE MONEY SHARING VIDEOS'
  end

  scenario "User goes to influencers sell site" do
    visit root_path
    within 'footer' do
      click_link 'Become an Influencer'
    end
    expect(page).to have_css '.hero h1', text: 'MAKE MONEY SHARING VIDEOS'
  end
end