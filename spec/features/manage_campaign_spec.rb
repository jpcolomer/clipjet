require 'spec_helper'

feature 'Manage campaigns' do
  scenario 'User sees running campaigns' do
    user = create(:user)
    campaign = create(:campaign_with_balance, name: 'test', small_thumbnail: 'sq.jpg' ,user: user)
    create(:impression, campaign: campaign)
    hit = create(:hit, campaign: campaign)
    hit.update_attributes(elapsed_time: 30)
    sign_in_as(user)
    goes_to_manage_campaigns
    user_see_campaign(campaign)
  end

  scenario "User goes to buy more credits" do
    user = create(:user)
    campaign = create(:campaign_with_balance, name: 'test', small_thumbnail: 'sq.jpg' ,user: user)
    sign_in_as(user)
    goes_to_manage_campaigns
    within "#campaign_#{campaign.id} .buy_credits" do
      click_link 'BUY CREDITS'
    end
    expect(current_path).to eq new_campaign_credits_purchase_path(:en, campaign)
  end

end