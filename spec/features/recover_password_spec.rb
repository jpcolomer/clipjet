require 'spec_helper'

feature "Password Recovery" do
  before(:each) do
    reset_mailer
  end
  scenario "User receives email when forgot password" do
    user = create(:user)
    visit new_user_session_path(locale: :en)
    click_link 'Forgot your password?'
    expect(current_path).to eq new_user_password_path(locale: :en)
    fill_in "Email", with: user.email
    click_button 'Send me reset password instructions'
    expect(open_last_email).to be_delivered_from 'noreply@clipjet.me'
    expect(open_last_email).to be_delivered_to user.email
    expect(open_last_email).to have_body_text edit_user_password_url(:reset_password_token => user.reset_password_token)
    click_first_link_in_email
    expect(page).to have_css 'h1', text: 'CHANGE MY PASSWORD'
    fill_in "user[password]", with: "secretsecret"
    fill_in "user[password_confirmation]", with: "secretsecret"
    expect {
      click_button 'Change my password'
      user.reload
    }.to change{user.encrypted_password}
    # expect(page).to have_content 'Your password was changed successfully.'
  end

  scenario "User doesn't receive email for invalid user when forgot password" do
    visit new_user_session_path
    click_link 'Forgot your password?'
    expect(current_path).to eq new_user_password_path(locale: :en)
    fill_in "Email", with: 'invalid@example.com'
    click_button 'Send me reset password instructions'
    expect(ActionMailer::Base.deliveries).to be_empty
  end
end