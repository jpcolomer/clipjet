require 'faker'

FactoryGirl.define do
  factory :impression do
    association :campaign, factory: :campaign_with_balance
    site
    ip { Faker::Internet.ip_v4_address }
  end
end