require 'faker'

FactoryGirl.define do
  factory :influencer_impression do
    influencer_video
    ip { Faker::Internet.ip_v4_address }
  end
end