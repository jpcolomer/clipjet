require 'faker'

FactoryGirl.define do
  factory :payment_notification do
    campaign
    transaction_id { Faker::Lorem.characters(10) }
    status 'Completed'
    amount {[20, 80, 200].sample}
    factory :payment_canceled do
      status 'canceled'
    end
    after(:stub, :build) do |notification|
    	notification.params ||= {}
    	notification.params[:secret] = notification.campaign.secret
    end
    after(:create) do |notification|
    	notification.params ||= {}
    	notification.params[:secret] = notification.campaign.secret
    	notification.save
    end
  end
end