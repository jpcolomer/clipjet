require 'faker'

FactoryGirl.define do
  factory :wallet do
    user
    balance {rand(0..100)}
  end
end