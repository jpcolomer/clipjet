require 'faker'

FactoryGirl.define do
  factory :site do
    url { Faker::Internet.url }
    user
  end
end