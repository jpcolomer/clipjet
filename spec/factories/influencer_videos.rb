require 'faker'

FactoryGirl.define do
  factory :influencer_video do
    association :campaign, factory: :campaign_with_balance
    user
  end
end