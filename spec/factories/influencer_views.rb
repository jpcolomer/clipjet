require 'faker'

FactoryGirl.define do
  factory :influencer_view do
    influencer_video
    ip { Faker::Internet.ip_v4_address }

    factory :iv_with_end_video do
      end_of_video true
      after(:create) do |iv|
        iv.elapsed_time = iv.campaign.video_duration
        iv.save
      end
    end

    factory :iv_with_elapsed_time do
      end_of_video false
      after(:create) do |iv|
        iv.elapsed_time = rand(10..iv.campaign.video_duration-10)
        iv.save
      end      
    end
  end
end