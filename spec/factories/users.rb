require 'faker'

FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    password '12345678'
    factory :user_with_paypal do
      paypal_email { Faker::Internet.email }
    end
    factory :user_with_money do
      paypal_email { Faker::Internet.email }
      after(:create) do |user|
        user.wallet.increase_balance(100)
        user.wallet.reload
      end
    end
  end
end