require 'faker'

FactoryGirl.define do
  factory :hit do
    association :campaign, factory: :campaign_with_balance
    site
    article_url { Faker::Internet.url }
    ip { Faker::Internet.ip_v4_address }
    user_agent 'test agent'

    factory :hit_with_end_video do
      end_of_video true
      after(:create) do |hit|
        hit.elapsed_time = hit.campaign.video_duration
        hit.save
      end
    end

    factory :hit_with_elapsed_time do
      end_of_video false
      after(:create) do |hit|
        hit.elapsed_time = rand(10..hit.campaign.video_duration-10)
        hit.save
      end      
    end
  end
end