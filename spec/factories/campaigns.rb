require 'faker'

FactoryGirl.define do
  factory :campaign do
    category_id {Category.first.id}
    user
    name { Faker::Lorem.characters(10) }
    site_url { Faker::Internet.url }
    thumbnail { Faker::Internet.url }
    small_thumbnail { Faker::Internet.url }
    description { Faker::Lorem.paragraph(3) }
    video_url 'http://www.youtube.com/watch?v=v-YrhySY2RM'
    video_duration { rand(60..300) }
    language { Campaign::LANGS.sample }
    factory :campaign_with_balance do
      after(:create) { |campaign| campaign.balance = 1000; campaign.save }
      after(:stub) { |campaign| campaign.balance = 1000}
    end
  end
end