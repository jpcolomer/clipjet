require 'faker'

FactoryGirl.define do
  factory :withdrawal_request do
    association :user, factory: :user_with_money
    amount 20
  end
end