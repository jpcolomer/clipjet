#=require shared_video
describe 'InfluencerClipjet', ->
  beforeEach ->
    fixture = '''
      <div id="clipjet-video" data-video="videoID" data-influencer-video="influencer_video_token"></div>
    '''
    setFixtures(fixture)

  describe '#init', ->
    clipjet = null
    beforeEach ->
      clipjet = new InfluencerClipjet()
      spyOn(clipjet, 'initPlayer').andCallFake ->
        true

    it 'calls initPlayer', ->
      clipjet.init()
      expect(clipjet.initPlayer).toHaveBeenCalled()

  describe '#readVideoID', ->
    it "reads videoID from div#clipjet-video", ->
      clipjet = new InfluencerClipjet()
      expect(clipjet.readVideoID()).toBe 'videoID'

    it "reads videoID2 from div#clipjet-video", ->
      fixture = '''
        <div id="clipjet-video" data-video="videoID2">
        </div>
      '''
      setFixtures(fixture)
      clipjet = new InfluencerClipjet()
      expect(clipjet.readVideoID()).toBe 'videoID2'

  describe '#readInfluencerVideoToken', ->
    it "reads influencer_video_token from div#clipjet-video", ->
      clipjet = new InfluencerClipjet
      expect(clipjet.readInfluencerVideoToken()).toBe 'influencer_video_token'

    it "reads influencer_video_token2 from div#clipjet-video", ->
      fixture = '''
        <div id="clipjet-video" data-influencer-video="influencer_video_token2">
        </div>
      '''
      setFixtures(fixture)
      clipjet = new InfluencerClipjet
      expect(clipjet.readInfluencerVideoToken()).toBe 'influencer_video_token2'

  describe '#setMinElapsedTime', ->
    clipjet = null
    beforeEach ->
      clipjet = new InfluencerClipjet
      clipjet.player = new Object
      clipjet.player.getDuration = ->
        100

    it "sets MinElapsedTime to 30", ->
      clipjet.setMinElapsedTime()
      expect(clipjet.minElapsedTime).toBe 30

    it "sets MinElapsedTime to Duration for duration < 30", ->
      clipjet.player.getDuration = ->
        10
      clipjet.setMinElapsedTime()
      expect(clipjet.minElapsedTime).toBe 10

  describe "checkVideoStatus", ->
    clipjet = null
    beforeEach ->
      clipjet = new InfluencerClipjet
      spyOn(clipjet, 'notifyVideoStarted').andCallFake ->
        true
      spyOn(clipjet, 'setVideoUpdateInterval').andCallFake ->
        true
      spyOn(clipjet, 'notifyVideoUpdate').andCallFake ->
        true

    it "sets @started to true if is initially false and data is 1", ->
      e = {data: 1}
      clipjet.checkVideoStatus(e)
      expect(clipjet.started).toBe(true)


    it "calls notifyVideoStarted if @started is false and data is 1", ->
      e = {data: 1}
      clipjet.checkVideoStatus(e)
      expect(clipjet.notifyVideoStarted).toHaveBeenCalled()


    it "doesn't call notifyVideoStarted if @started is true", ->
      clipjet.started = true
      e = {data: 1}
      clipjet.checkVideoStatus(e)
      expect(clipjet.notifyVideoStarted).not.toHaveBeenCalled()

    it "doesn't call notifyVideoStarted if data is not 1", ->
      e = {data: 2}
      clipjet.checkVideoStatus(e)
      expect(clipjet.notifyVideoStarted).not.toHaveBeenCalled()

    it "calls setVideoUpdateInterval if @started is false and data is 1", ->
      e = {data: 1}
      clipjet.checkVideoStatus(e)
      expect(clipjet.setVideoUpdateInterval).toHaveBeenCalled()


    it "doesn't call setVideoUpdateInterval if @started is true", ->
      clipjet.started = true
      e = {data: 1}
      clipjet.checkVideoStatus(e)
      expect(clipjet.setVideoUpdateInterval).not.toHaveBeenCalled()

    it "doesn't call setVideoUpdateInterval if data is not 1", ->
      e = {data: 2}
      clipjet.checkVideoStatus(e)
      expect(clipjet.setVideoUpdateInterval).not.toHaveBeenCalled()

    it "calls notifyVideoUpdate if event data is 0 and ended is false", ->
      e = {data: 0}
      clipjet.checkVideoStatus(e)
      expect(clipjet.notifyVideoUpdate).toHaveBeenCalled()

    it "sets @ended to true if is initially false and data is 0", ->
      e = {data: 0}
      clipjet.checkVideoStatus(e)
      expect(clipjet.ended).toBe(true)

  describe '#notifyVideoStarted', ->


  describe '#setVideoUpdateInterval', ->
    clipjet = null
    beforeEach ->
      clipjet = new InfluencerClipjet
      spyOn(window, 'setInterval').andCallFake ->
        'test'

    it "calls every second checkVideoTime", ->
      clipjet.setVideoUpdateInterval()
      expect(window.setInterval).toHaveBeenCalledWith(clipjet.checkVideoTime, 500)

    it "sets clipjet.intervalID", ->
      clipjet.setVideoUpdateInterval()
      expect(clipjet.intervalID).toBe 'test'


  describe '#checkVideoTime', ->
    clipjet = null
    beforeEach ->
      clipjet = new InfluencerClipjet
      spyOn(clipjet, 'notifyVideoUpdate')
      spyOn(window, 'clearInterval')
      clipjet.intervalID = new Object
      clipjet.player = new Object

    it "calls #notifyVideoUpdate if playing & currentime > minElapsedTime", ->
      clipjet.minElapsedTime = 10
      clipjet.player.getCurrentTime = ->
        11
      clipjet.player.getPlayerState = ->
        1
      clipjet.checkVideoTime()
      expect(clipjet.notifyVideoUpdate).toHaveBeenCalled()

    it "clears Interval if playing & currentime > minElapsedTime", ->
      clipjet.minElapsedTime = 10
      clipjet.player.getCurrentTime = ->
        11
      clipjet.player.getPlayerState = ->
        1
      clipjet.checkVideoTime()
      expect(window.clearInterval).toHaveBeenCalledWith(clipjet.intervalID)      

    it "doesn't call #notifyVideoUpdate if not playing", ->
      clipjet.minElapsedTime = 10
      clipjet.player.getCurrentTime = ->
        11
      clipjet.player.getPlayerState = ->
        0
      clipjet.checkVideoTime()
      expect(clipjet.notifyVideoUpdate).not.toHaveBeenCalled()

    it "doesn't clear Interval if not playing", ->
      clipjet.minElapsedTime = 10
      clipjet.player.getCurrentTime = ->
        11
      clipjet.player.getPlayerState = ->
        0
      clipjet.checkVideoTime()
      expect(window.clearInterval).not.toHaveBeenCalledWith(clipjet.intervalID)      


    it "doesn't call #notifyVideoUpdate if currentime < minElapsedTime", ->
      clipjet.minElapsedTime = 12
      clipjet.player.getCurrentTime = ->
        11
      clipjet.player.getPlayerState = ->
        1
      clipjet.checkVideoTime()
      expect(clipjet.notifyVideoUpdate).not.toHaveBeenCalled()

    it "doesn't clear Interval if currentime < minElapsedTime", ->
      clipjet.minElapsedTime = 12
      clipjet.player.getCurrentTime = ->
        11
      clipjet.player.getPlayerState = ->
        1
      clipjet.checkVideoTime()
      expect(window.clearInterval).not.toHaveBeenCalledWith(clipjet.intervalID)

  describe "#setToken", ->
    clipjet = null
    beforeEach ->
      clipjet = new InfluencerClipjet

    it "sets token to 123 if success true", ->
      json = {success: true, token: '123'}
      clipjet.setToken(json)
      expect(clipjet.token).toBe '123'

    it "sets token to 1234 if success true", ->
      json = {success: true, token: '1234'}
      clipjet.setToken(json)
      expect(clipjet.token).toBe '1234'

    it "doesn't set token if success false", ->
      json = {success: false}
      clipjet.setToken(json)
      expect(clipjet.token).not.toBeDefined()

    it "sets success to true", ->
      json = {success: true, token: '1234'}
      clipjet.setToken(json)
      expect(clipjet.success).toBe(true)

    it "sets success to false", ->
      json = {success: false}
      clipjet.setToken(json)
      expect(clipjet.success).toBe(false)

  describe "#notifyVideoStarted", ->
    clipjet = null
    beforeEach ->
      clipjet = new InfluencerClipjet

    it "calls $.ajax with right URL", ->
      spyOn($, "ajax")
      clipjet.notifyVideoStarted()
      expect($.ajax.mostRecentCall.args[0]["url"]).toEqual("/influencer_views")

    it "calls $.ajax with type POST", ->
      spyOn($, "ajax")
      clipjet.notifyVideoStarted()
      expect($.ajax.mostRecentCall.args[0]["type"]).toEqual("POST")

    it "calls $.ajax with dataType json", ->
      spyOn($, "ajax")
      clipjet.notifyVideoStarted()
      expect($.ajax.mostRecentCall.args[0]["dataType"]).toEqual("json")

    it "calls $.ajax with data influencer video token", ->
      spyOn($, "ajax")
      clipjet.influencerVideo = 'abc'
      clipjet.notifyVideoStarted()
      expect($.ajax.mostRecentCall.args[0]["data"]).toEqual({influencer_view: {influencer_video_token: 'abc'}})

    it "execute setToken on success", ->
      json = JSON.stringify({success: true, token: '123'})
      spyOn($, "ajax").andCallFake (options) ->
        options.success(json)
      spyOn(clipjet, 'setToken')
      clipjet.notifyVideoStarted()
      expect(clipjet.setToken).toHaveBeenCalledWith(json)

  describe "#notifyVideoUpdate", ->
    clipjet = null
    beforeEach ->
      clipjet = new InfluencerClipjet
      clipjet.token = 'abc'
      clipjet.player = new Object()
      clipjet.player.getCurrentTime = ->
        30
      clipjet.success = true

    it "calls $.ajax if success is true", ->
      spyOn($, "ajax")
      clipjet.notifyVideoUpdate()
      expect($.ajax).toHaveBeenCalled()

    it "doens't call $.ajax if success is false", ->
      spyOn($, "ajax")
      clipjet.success = false
      clipjet.notifyVideoUpdate()
      expect($.ajax).not.toHaveBeenCalled()

    it "calls $.ajax with right URL for view abc", ->
      spyOn($, "ajax")
      clipjet.notifyVideoUpdate()
      expect($.ajax.mostRecentCall.args[0]["url"]).toEqual("/influencer_views/abc")

    it "calls $.ajax with right URL for view abcd", ->
      spyOn($, "ajax")
      clipjet.token = 'abcd'
      clipjet.notifyVideoUpdate()
      expect($.ajax.mostRecentCall.args[0]["url"]).toEqual("/influencer_views/abcd")

    it "calls $.ajax with type PUT", ->
      spyOn($, "ajax")
      clipjet.notifyVideoUpdate()
      expect($.ajax.mostRecentCall.args[0]["type"]).toEqual("PUT")

    it "calls $.ajax with dataType json", ->
      spyOn($, "ajax")
      clipjet.notifyVideoUpdate()
      expect($.ajax.mostRecentCall.args[0]["dataType"]).toEqual("json")

    it "calls $.ajax with data influencer view for end_of_video and elapsed_time 30", ->
      spyOn($, "ajax")
      clipjet.ended = true
      clipjet.notifyVideoUpdate()
      expect($.ajax.mostRecentCall.args[0]["data"]).toEqual({influencer_view: {end_of_video: 1, elapsed_time: 30}})

    it "calls $.ajax with data influencer view for end_of_video false and elapsed_time 20", ->
      spyOn($, "ajax")
      clipjet.player.getCurrentTime = ->
        20
      clipjet.ended = false
      clipjet.notifyVideoUpdate()
      expect($.ajax.mostRecentCall.args[0]["data"]).toEqual({influencer_view: {end_of_video: 0, elapsed_time: 20}})

  describe "#onYTReady", ->
    it "calls setMinElapsedTime", ->
      clipjet = new InfluencerClipjet
      spyOn(clipjet, 'setMinElapsedTime')
      clipjet.onYTReady()
      expect(clipjet.setMinElapsedTime).toHaveBeenCalled()