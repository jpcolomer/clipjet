describe 'Clipjet', ->
  beforeEach ->
    fixture = '''
      <div id="clipjet-video" width="200px" height="200px" data-video="videoId" data-campaign="campaign_token" data-site="site_token" data-campaign-url="http://www.clipjet.me"></div>
    '''
    setFixtures(fixture)


  describe '.makeUrlFromNestedJson', ->
    it "returns nested[nest]=value", ->
      json = {nested : {nest : 'value'}}
      expect(Clipjet.makeUrlFromNestedJson(json)).toBe('nested[nest]=value')
    it "returns nested1[nest1]=value1", ->
      json = {nested1 : {nest1 : 'value1'}}
      expect(Clipjet.makeUrlFromNestedJson(json)).toBe('nested1[nest1]=value1')

  describe '.getVideoStartedUrl', ->
    beforeEach ->
      Clipjet.url = "http://localhost:3000/api"
    it 'calls makeUrlFromNestedJson', ->
      spyOn(Clipjet, 'makeUrlFromNestedJson').andCallFake ->
        '1234'
      Clipjet.getVideoStartedUrl('asdf')
      expect(Clipjet.makeUrlFromNestedJson).toHaveBeenCalledWith('asdf')

    it 'returns http://localhost:3000/api/hit/create?1234', ->
      spyOn(Clipjet, 'makeUrlFromNestedJson').andCallFake ->
        '1234'
      expect(Clipjet.getVideoStartedUrl('asdf')).toBe 'http://localhost:3000/api/hit/create?1234'

    it 'returns http://localhost:3000/api/hit/create?12345', ->
      spyOn(Clipjet, 'makeUrlFromNestedJson').andCallFake ->
        '12345'
      expect(Clipjet.getVideoStartedUrl('asdf')).toBe 'http://localhost:3000/api/hit/create?12345'


  describe ".getVideoUpdateUrl", ->
    beforeEach ->
      Clipjet.url = "http://localhost:3000/api"

    it "calls makeUrlFromNestedJson", ->
      spyOn(Clipjet, 'makeUrlFromNestedJson').andCallFake ->
        '12345'
      Clipjet.getVideoUpdateUrl('token','test_json')
      expect(Clipjet.makeUrlFromNestedJson).toHaveBeenCalledWith('test_json')

    it "returns http://localhost:3000/api/hit/update?token=1234&12345", ->
      spyOn(Clipjet, 'makeUrlFromNestedJson').andCallFake ->
        '12345'
      expect(Clipjet.getVideoUpdateUrl('1234','test_json')).toBe "http://localhost:3000/api/hit/update?token=1234&12345"


    it "returns http://localhost:3000/api/hit/update?token=123456&12345", ->
      spyOn(Clipjet, 'makeUrlFromNestedJson').andCallFake ->
        '12345'
      expect(Clipjet.getVideoUpdateUrl('123456','test_json')).toBe "http://localhost:3000/api/hit/update?token=123456&12345"


  describe '#initPlayer', ->
    clipjet = null
    beforeEach ->
      Clipjet.url = "http://localhost:3000/api"
      clipjet = new Clipjet()
      spyOn(clipjet, 'initYTPlayer')

    it "calls initYTPlayer", ->
      clipjet.initPlayer()
      expect(clipjet.initYTPlayer).toHaveBeenCalled()

  describe '#init', ->
    clipjet = null
    beforeEach ->
      clipjet = new Clipjet()
      spyOn(clipjet, 'initPlayer').andCallFake ->
        true

    it 'calls initPlayer', ->
      clipjet.init()
      expect(clipjet.initPlayer).toHaveBeenCalled()


  describe '#readSiteTokenFromDiv', ->
    it "reads site_token from div#clipjet-video", ->
      clipjet = new Clipjet
      expect(clipjet.readSiteTokenFromDiv()).toBe 'site_token'

    it "reads site_token2 from div#clipjet-video", ->
      fixture = '''
        <div id="clipjet-video" data-site="site_token2">
        </div>
      '''
      setFixtures(fixture)
      clipjet = new Clipjet
      expect(clipjet.readSiteTokenFromDiv()).toBe 'site_token2'



  describe '#readCampaignTokenFromDiv', ->
    it "reads campaign_token from div#clipjet-video", ->
      clipjet = new Clipjet
      expect(clipjet.readCampaignTokenFromDiv()).toBe 'campaign_token'

    it "reads 123456 from div#clipjet-video", ->
      fixture = '''
        <div id="clipjet-video" data-campaign="123456">
        </div>
      '''
      setFixtures(fixture)
      clipjet = new Clipjet
      expect(clipjet.readCampaignTokenFromDiv()).toBe '123456'

  describe '#readVideoIdFromDiv', ->
    it "reads videoId from div#clipjet-video", ->
      clipjet = new Clipjet()
      expect(clipjet.readVideoIdFromDiv()).toBe 'videoId'

    it "reads videoId2 from div#clipjet-video", ->
      fixture = '''
        <div id="clipjet-video" data-video="videoId2">
        </div>
      '''
      setFixtures(fixture)
      clipjet = new Clipjet()
      expect(clipjet.readVideoIdFromDiv()).toBe 'videoId2'

  describe '#readSiteURLFromDiv', ->
    it "reads http://www.clipjet.me from div#clipjet-video", ->
      clipjet = new Clipjet()
      expect(clipjet.readSiteURLFromDiv()).toBe 'http://www.clipjet.me'

    it "reads http://www.clipjet.com from div#clipjet-video", ->
      fixture = '''
        <div id="clipjet-video" data-campaign-url="http://www.clipjet.com">
        </div>
      '''
      setFixtures(fixture)
      clipjet = new Clipjet()
      expect(clipjet.readSiteURLFromDiv()).toBe 'http://www.clipjet.com'

    it "sets '' to siteURL from div#clipjet-video", ->
      fixture = '''
        <div id="clipjet-video" data-campaign-url="">
        </div>
      '''
      setFixtures(fixture)
      clipjet = new Clipjet()
      expect(clipjet.readSiteURLFromDiv()).toBe ''

  describe '#readWidthFromDiv', ->
    it "reads 200px from div#clipjet-video", ->
      clipjet = new Clipjet()
      expect(clipjet.readWidthFromDiv()).toBe '200px'

    it "reads width 300px from div#clipjet-video", ->
      fixture = '''
        <div id="clipjet-video" width="300px">
        </div>
      '''
      setFixtures(fixture)
      clipjet = new Clipjet()
      expect(clipjet.readWidthFromDiv()).toBe '300px'

  describe '#readHeightFromDiv', ->
    it "reads 200px from div#clipjet-video", ->
      clipjet = new Clipjet()
      expect(clipjet.readHeightFromDiv()).toBe '200px'

    it "reads height 300px from div#clipjet-video", ->
      fixture = '''
        <div id="clipjet-video" height="300px">
        </div>
      '''
      setFixtures(fixture)
      clipjet = new Clipjet()
      expect(clipjet.readHeightFromDiv()).toBe '300px'

  describe 'setToken', ->
    clipjet = null
    beforeEach ->
      clipjet = new Clipjet

    it "sets token to 123", ->
      clipjet.setToken('123')
      expect(clipjet.token).toBe '123'

    it "sets campaign_token to 1234", ->
      clipjet.setToken('1234')
      expect(clipjet.token).toBe '1234'

  describe '#setMinElapsedTime', ->
    clipjet = null
    beforeEach ->
      clipjet = new Clipjet
      clipjet.player = new Object
      clipjet.player.getDuration = ->
        100

    it "sets MinElapsedTime to 30", ->
      clipjet.setMinElapsedTime()
      expect(clipjet.minElapsedTime).toBe 30

    it "sets MinElapsedTime to Duration for duration < 30", ->
      clipjet.player.getDuration = ->
        10
      clipjet.setMinElapsedTime()
      expect(clipjet.minElapsedTime).toBe 10

  describe '#setVideoUpdateInterval', ->
    clipjet = null
    beforeEach ->
      clipjet = new Clipjet
      spyOn(window, 'setInterval').andCallFake ->
        'test'

    it "calls every second checkVideoTime", ->
      clipjet.setVideoUpdateInterval()
      expect(window.setInterval).toHaveBeenCalledWith(clipjet.checkVideoTime, 500)

    it "sets clipjet.intervalID", ->
      clipjet.setVideoUpdateInterval()
      expect(clipjet.intervalID).toBe 'test'


  describe '#getYtURLFromId', ->
    clipjet = null
    beforeEach ->
      clipjet = new Clipjet

    it "returns http://www.youtube.com/watch?v=LnejjEYsYWI", ->
      clipjet.videoId = 'LnejjEYsYWI'
      expect(clipjet.getYtURLFromId()).toEqual('http://www.youtube.com/watch?v=LnejjEYsYWI')

    it "returns http://www.youtube.com/watch?v=COkudtssUGs", ->
      clipjet.videoId = 'COkudtssUGs'
      expect(clipjet.getYtURLFromId()).toEqual('http://www.youtube.com/watch?v=COkudtssUGs')

  describe '#insertSocialTab', ->
    clipjet = null
    beforeEach ->
      clipjet = new Clipjet
      spyOn(clipjet, 'instantiateSocialTab').andCallFake ->
        'blah'

    it "calls insert", ->
      clipjet.socialTab = new Object
      clipjet.socialTab.insert= () ->
        'ble'
      spyOn(clipjet.socialTab, 'insert').andCallFake ->
        'ble'
      clipjet.insertSocialTab()
      expect(clipjet.socialTab.insert).toHaveBeenCalled()


  describe '#checkVideoTime', ->
    clipjet = null
    beforeEach ->
      clipjet = new Clipjet
      spyOn(clipjet, 'notifyVideoUpdate')
      spyOn(window, 'clearInterval')
      clipjet.intervalID = new Object
      clipjet.player = new Object

    it "calls #notifyVideoUpdate if playing & currentime > minElapsedTime", ->
      clipjet.minElapsedTime = 10
      clipjet.player.getCurrentTime = ->
        11
      clipjet.player.getPlayerState = ->
        1
      clipjet.checkVideoTime()
      expect(clipjet.notifyVideoUpdate).toHaveBeenCalled()

    it "clears Interval if playing & currentime > minElapsedTime", ->
      clipjet.minElapsedTime = 10
      clipjet.player.getCurrentTime = ->
        11
      clipjet.player.getPlayerState = ->
        1
      clipjet.checkVideoTime()
      expect(window.clearInterval).toHaveBeenCalledWith(clipjet.intervalID)      

    it "doesn't call #notifyVideoUpdate if not playing", ->
      clipjet.minElapsedTime = 10
      clipjet.player.getCurrentTime = ->
        11
      clipjet.player.getPlayerState = ->
        0
      clipjet.checkVideoTime()
      expect(clipjet.notifyVideoUpdate).not.toHaveBeenCalled()

    it "doesn't clear Interval if not playing", ->
      clipjet.minElapsedTime = 10
      clipjet.player.getCurrentTime = ->
        11
      clipjet.player.getPlayerState = ->
        0
      clipjet.checkVideoTime()
      expect(window.clearInterval).not.toHaveBeenCalledWith(clipjet.intervalID)      


    it "doesn't call #notifyVideoUpdate if currentime < minElapsedTime", ->
      clipjet.minElapsedTime = 12
      clipjet.player.getCurrentTime = ->
        11
      clipjet.player.getPlayerState = ->
        1
      clipjet.checkVideoTime()
      expect(clipjet.notifyVideoUpdate).not.toHaveBeenCalled()

    it "doesn't clear Interval if currentime < minElapsedTime", ->
      clipjet.minElapsedTime = 12
      clipjet.player.getCurrentTime = ->
        11
      clipjet.player.getPlayerState = ->
        1
      clipjet.checkVideoTime()
      expect(window.clearInterval).not.toHaveBeenCalledWith(clipjet.intervalID)      

  describe '#onYTReady', ->
    clipjet = null
    beforeEach ->
      clipjet = new Clipjet
      spyOn(clipjet, 'insertSocialTab').andCallFake ->
      spyOn(clipjet, 'setMinElapsedTime').andCallFake ->

    it "calls insertSocialTab", ->
      clipjet.onYTReady()
      expect(clipjet.insertSocialTab).toHaveBeenCalled()

    it "calls setMinElapsedTime", ->
      clipjet.onYTReady()
      expect(clipjet.setMinElapsedTime).toHaveBeenCalled()

  describe '#checkVideoStatus', ->
    clipjet = null
    beforeEach ->
      clipjet = new Clipjet
    it "calls @notifyVideoStarted when event.data 1 and started false", ->
      spyOn(clipjet, 'notifyVideoStarted')
      clipjet.checkVideoStatus({data: 1})
      expect(clipjet.notifyVideoStarted).toHaveBeenCalled()
      expect(clipjet.started).toBe true

    it "calls @setVideoUpdateInterval when even.data 1", ->
      spyOn(clipjet, 'setVideoUpdateInterval')
      clipjet.checkVideoStatus({data: 1})
      expect(clipjet.setVideoUpdateInterval).toHaveBeenCalled()

    it "calls @notifyVideoUpdate with 1 when event.data 0", ->
      spyOn(clipjet, 'notifyVideoUpdate')
      clipjet.checkVideoStatus({data: 0})
      expect(clipjet.notifyVideoUpdate).toHaveBeenCalledWith(1)

  describe "#notifyVideoUpdate", ->
    clipjet = null
    beforeEach ->
      clipjet = new Clipjet
      clipjet.player = new Object
      clipjet.player.getCurrentTime = ->
        10
      spyOn(Clipjet, 'getVideoUpdateUrl').andCallFake ->
        'test'
      spyOn(clipjet, 'sendUpdateRequest').andCallFake ->
        true

    it "doesn't do anything if success is false", ->
      clipjet.sucess = true
      clipjet.notifyVideoUpdate(1)
      expect(Clipjet.getVideoUpdateUrl).toHaveBeenCalled()
      expect(clipjet.sendUpdateRequest).toHaveBeenCalled()
      
    it "calls .getVideoUpdateUrl with 'token','json'", ->
      clipjet.token = 'token'
      clipjet.notifyVideoUpdate(1)
      json = {hit : {elapsed_time : 10, end_of_video : 1}}
      expect(Clipjet.getVideoUpdateUrl).toHaveBeenCalledWith('token', json)

    it 'calls sendUpdateRequest', ->
      clipjet.notifyVideoUpdate(1)
      expect(clipjet.sendUpdateRequest).toHaveBeenCalledWith('test')

  describe '#notifyVideoStarted', ->
    clipjet = null
    beforeEach ->
      clipjet = new Clipjet
      clipjet.site_token = '123'
      clipjet.campaign_token = '1234'
      spyOn(Clipjet, 'getPathname').andCallFake ->
        'asdf'

    it "calls .getVideoStartedUrl", ->
      spyOn(Clipjet, 'getVideoStartedUrl')
      clipjet.notifyVideoStarted()
      expect(Clipjet.getVideoStartedUrl).toHaveBeenCalledWith({hit : { site_token : '123', article_url : 'asdf' , campaign_token : '1234' }})

    it 'calls #xss_ajax', ->
      spyOn(Clipjet, 'getVideoStartedUrl').andCallFake ->
        'test'
      spyOn(clipjet, 'xss_ajax')
      clipjet.notifyVideoStarted()
      expect(clipjet.xss_ajax).toHaveBeenCalledWith('test')

  describe '#sendUpdateRequest', ->
    it "adds an img to body", ->
      clipjet = new Clipjet
      clipjet.sendUpdateRequest('test')
      expect($('body')).toContain 'img.update-clipjet'

    it "img is hidden", ->
      clipjet = new Clipjet
      clipjet.sendUpdateRequest('test')
      expect($('.update-clipjet')).toHaveCss({display: "none"})

    it "img has src test", ->
      clipjet = new Clipjet
      clipjet.sendUpdateRequest('test')
      expect($('.update-clipjet')).toHaveAttr('src', 'test' )
