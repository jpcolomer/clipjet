describe 'SocialTab', ->
  beforeEach ->
    fixture = '''
      <div id="clipjet-video" width="200px" height="200px" data-video="videoId" data-campaign="campaign_token"></div>
    '''
    setFixtures(fixture)


  describe '.new', ->
    it "sets url to url1", ->
      socialTab = new SocialTab('url1')
      expect(socialTab.url).toBe 'url1'
    it "sets url to url2", ->
      socialTab = new SocialTab('url2')
      expect(socialTab.url).toBe 'url2'
 
  describe '#insert', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      spyOn(socialTab, 'insertDiv')
      spyOn(socialTab, 'insertClipjetLogo')
      spyOn(socialTab, 'insertFBShare')
      spyOn(socialTab, 'insertTwitterShare')
      spyOn(socialTab, 'insertMoreInfo')

    it "calls insertDiv", ->
      socialTab.insert()
      expect(socialTab.insertDiv).toHaveBeenCalled()    

    it "calls insertClipjetLogo", ->
      socialTab.insert()
      expect(socialTab.insertClipjetLogo).toHaveBeenCalled()

    it "calls insertFBShare", ->
      socialTab.insert()
      expect(socialTab.insertFBShare).toHaveBeenCalled()

    it "calls insertTwitterShare", ->
      socialTab.insert()
      expect(socialTab.insertTwitterShare).toHaveBeenCalled()

    it "calls insertMoreInfo", ->
      socialTab.siteURL = '#'
      socialTab.insert()
      expect(socialTab.insertMoreInfo).toHaveBeenCalled()

    it "doesn't call insertMoreInfo for siteURL=null", ->
      socialTab.siteURL = null
      socialTab.insert()
      expect(socialTab.insertMoreInfo).not.toHaveBeenCalled()

  describe '#insertDiv', ->
    it "inserts bottom div", ->
      spyOn(SocialTab, 'getIframeHeight').andReturn(230)
      socialTab = new SocialTab('bla')
      socialTab.insertDiv()
      expect(socialTab.top).toEqual('200px')
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({height: '29px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({width: '200px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({margin: '0px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({padding: '0px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({padding: '0px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({position: 'absolute', top: '200px'})

    it "inserts bottom div", ->
      spyOn(SocialTab, 'getIframeHeight').andReturn(430)
      socialTab = new SocialTab('bla', '300px', '400px')
      socialTab.insertDiv()
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({height: '29px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({width: '300px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({margin: '0px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({padding: '0px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({padding: '0px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({position: 'absolute', top: '400px'})

    it "inserts bottom div top: 0 for iframe height < player height + 30", ->
      spyOn(SocialTab, 'getIframeHeight').andReturn(400)
      socialTab = new SocialTab('bla', '300px', '400px')
      socialTab.insertDiv()
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({height: '29px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({width: '300px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({margin: '0px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({padding: '0px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({padding: '0px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({position: 'absolute', top: '0px'})

  describe "#insertClipjetLogo", ->
    it "inserts anchor with image of Clipjet", ->
      socialTab = new SocialTab('bla')
      socialTab.insertDiv()
      socialTab.insertClipjetLogo()
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo')).toHaveCss({margin: '0px 0px 0px 8px'})
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo')).toHaveCss({padding: '0px'})
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo')).toHaveCss({position: 'absolute', top: '14px'})
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo')).toHaveAttr('href', 'http://www.clipjet.me/?utm_source=widget&utm_medium=video&utm_campaign=widget+logo')
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo img')).toHaveCss({padding: '0px'})
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo img')).toHaveCss({margin: '0px'})
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo img')).toHaveCss({'vertical-align': 'bottom'})
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo img')).toHaveAttr('src', 'http://widget.clipjet.me/logo/clipjet.png')

  describe '#insertFBShare', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      socialTab.insertDiv()
      spyOn(socialTab, 'setFBShare').andCallThrough()
      spyOn(socialTab, 'setFBImage')

    it "calls setFBShare", ->
      socialTab.insertFBShare()
      expect(socialTab.setFBShare).toHaveBeenCalled()

    it "calls setFBImage", ->
      socialTab.insertFBShare()
      expect(socialTab.setFBImage).toHaveBeenCalled()

    it "inserts FB Link", ->
      socialTab.insertDiv()
      socialTab.insertFBShare()
      expect($('#clipjet-video .clipjet-socialTab')).toContain('.clipjet-fb')

  describe '#setFBShare', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      socialTab.insertDiv()
      socialTab.setFBShare()
      spyOn(socialTab, 'setFBImage')

    it "sets margin 0", ->
      expect(socialTab.fb).toHaveCss(margin: '2px')

    it "sets padding 0", ->
      expect(socialTab.fb).toHaveCss(padding: '0px')

    it "sets float to right", ->
      expect(socialTab.fb).toHaveCss(float: 'right')

    it "sets float to right", ->
      expect(socialTab.fb).toHaveCss(border: 'none')

    it "sets class to clipjet-fb", ->
      expect(socialTab.fb.getAttribute('class')).toEqual('clipjet-fb')

    it 'sets href to #', ->
      expect(socialTab.fb.getAttribute('href')).toEqual("https://www.facebook.com/sharer/sharer.php?u=#{encodeURIComponent(socialTab.url)}")

    it 'sets onclick to https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DASRn1c_Jj24', ->
      expect(socialTab.fb.getAttribute('onclick')).toContain('https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DASRn1c_Jj24')

    it 'sets onclick to https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DCOkudtssUGs', ->
      socialTab.url = 'http://www.youtube.com/watch?v=COkudtssUGs'
      socialTab.setFBShare()
      expect(socialTab.fb.getAttribute('onclick')).toContain('https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DCOkudtssUGs')

  describe '#setFBImage', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      socialTab.insertDiv()
      socialTab.insertFBShare()

    it "appends image to fb", ->
      expect(socialTab.fb).toContain('img')
      expect($('.clipjet-fb img')).toHaveAttr('src', 'http://widget.clipjet.me/social/fb.png')


  describe '#insertTwitterShare', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      socialTab.insertDiv()
      spyOn(socialTab, 'setTwitterShare').andCallThrough()
      spyOn(socialTab, 'setTwitterImage')

    it "calls setTwitterShare", ->
      socialTab.insertTwitterShare()
      expect(socialTab.setTwitterShare).toHaveBeenCalled()

    it "calls setTwitterImage", ->
      socialTab.insertTwitterShare()
      expect(socialTab.setTwitterImage).toHaveBeenCalled()

    it "inserts Twitter Link", ->
      socialTab.insertDiv()
      socialTab.insertTwitterShare()
      expect($('#clipjet-video .clipjet-socialTab')).toContain('.clipjet-twitter')


  describe '#setTwitterShare', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      socialTab.insertDiv()
      socialTab.setTwitterShare()
      spyOn(socialTab, 'setTwitterImage')

    it "sets margin 0", ->
      expect(socialTab.twitter).toHaveCss(margin: '2px')

    it "sets padding 0", ->
      expect(socialTab.twitter).toHaveCss(padding: '0px')

    it "sets float to right", ->
      expect(socialTab.twitter).toHaveCss(float: 'right')

    it "sets border to none", ->
      expect(socialTab.twitter).toHaveCss(border: 'none')

    it "sets class to clipjet-twitter", ->
      expect(socialTab.twitter.getAttribute('class')).toEqual('clipjet-twitter')

    it 'sets href to #', ->
      expect(socialTab.twitter.getAttribute('href')).toEqual("https://twitter.com/share?via=clipjet&url=#{encodeURIComponent(socialTab.url)}")

    it 'sets onclick to https://twitter.com/share?via=clipjet&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DASRn1c_Jj24', ->
      expect(socialTab.twitter.getAttribute('onclick')).toContain('https://twitter.com/share?via=clipjet&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DASRn1c_Jj24')

    it 'sets onclick to https://twitter.com/share?via=clipjet&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DCOkudtssUGs', ->
      socialTab.url = 'http://www.youtube.com/watch?v=COkudtssUGs'
      socialTab.setTwitterShare()
      expect(socialTab.twitter.getAttribute('onclick')).toContain('https://twitter.com/share?via=clipjet&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DCOkudtssUGs')

  describe '#setTwitterImage', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      socialTab.insertDiv()
      socialTab.insertTwitterShare()

    it "appends image to twitter", ->
      expect(socialTab.twitter).toContain('img')
      expect($('.clipjet-twitter img')).toHaveAttr('src', 'http://widget.clipjet.me/social/twitter.png')


  describe '#insertMoreInfo', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24', 'http://www.clipjet.me')
    beforeEach ->
      socialTab.insertDiv()
      spyOn(socialTab, 'setMoreInfo').andCallThrough()

    it "calls setMoreInfo", ->
      socialTab.insertMoreInfo()
      expect(socialTab.setMoreInfo).toHaveBeenCalled()

    it "inserts More Info Link", ->
      socialTab.insertMoreInfo()
      expect($('#clipjet-video .clipjet-socialTab')).toContain('.clipjet-more-info')


  describe '#setMoreInfo', ->
    socialTab = null
    beforeEach ->
      socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24', '100px', '100px','http://www.clipjet.me')
      socialTab.insertDiv()
      socialTab.setMoreInfo()

    it "has text +info", ->
      expect(socialTab.moreInfo).toHaveText('+INFO')

    it "sets margin 2", ->
      expect(socialTab.moreInfo).toHaveCss(margin: '2px')

    it "sets float to right", ->
      expect(socialTab.moreInfo).toHaveCss(float: 'right')

    it "sets border to none", ->
      expect(socialTab.moreInfo).toHaveCss(border: 'none')

    it "sets color to black", ->
      expect(socialTab.moreInfo).toHaveCss(color: 'rgb(0, 0, 0)')      

    it "sets font family to arial", ->
      expect(socialTab.moreInfo).toHaveCss('font-family': 'arial, sans-serif')  

    it "sets font size to 12px", ->
      expect(socialTab.moreInfo).toHaveCss('font-size': '12px')

    it "sets font weight to bold", ->
      expect(socialTab.moreInfo).toHaveCss('font-weight': 'bold')

    it "sets text-align to center", ->
      expect(socialTab.moreInfo).toHaveCss('text-align': 'center')

    it "sets height to 25px", ->
      expect(socialTab.moreInfo).toHaveCss('height': '25px')

    it "sets line-height to 25px", ->
      expect(socialTab.moreInfo).toHaveCss('line-height': '25px')

    it "sets text-decoration to none", ->
      expect(socialTab.moreInfo).toHaveCss('text-decoration': 'none')

    it "sets backgroundColor to #ffd73e", ->
      expect(socialTab.moreInfo).toHaveCss('background-color': 'rgb(255, 215, 62)')

    it "sets padding to 0px 6px", ->
      expect(socialTab.moreInfo).toHaveCss('padding': '0px 6px')

    it "sets class to clipjet-more-info", ->
      expect(socialTab.moreInfo.getAttribute('class')).toEqual('clipjet-more-info')

    it 'sets href to http://www.clipjet.me', ->
      expect(socialTab.moreInfo.getAttribute('href')).toEqual('http://www.clipjet.me')

    it 'sets href to http://www.clipjet.com', ->
      socialTab.siteURL = 'http://www.clipjet.com'
      socialTab.setMoreInfo()
      expect(socialTab.moreInfo.getAttribute('href')).toEqual('http://www.clipjet.com')