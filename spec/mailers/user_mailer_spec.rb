require "spec_helper"

describe UserMailer do
  describe "withdrawal_request" do
    let(:user) { build_stubbed(:user, paypal_email: 'paypal@example.com') }
    let(:withdrawal) { build_stubbed(:withdrawal_request, user: user) }
    let(:mail) { UserMailer.withdrawal_request(withdrawal) }

    it "renders the headers" do
      expect(mail.subject).to eq("ClipJet withdrawal request")
      expect(mail.to).to eq(['paypal@example.com'])
      expect(mail.from).to eq(["withdrawal@clipjet.me"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to eq("Hello, you have requested to withdraw #{ActionController::Base.helpers.number_to_currency(withdrawal.amount)} from your ClipJet Balance")
    end
  end

end
