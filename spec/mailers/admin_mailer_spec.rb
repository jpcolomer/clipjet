require "spec_helper"

describe AdminMailer do
  describe "withdrawal_mailer" do
    let(:withdrawal) { build_stubbed(:withdrawal_request) }
    let(:mail) { AdminMailer.withdrawal_request(withdrawal) }

    it "renders the headers" do
      mail.subject.should eq("New withdrawal request")
      mail.to.should eq(["withdrawal@clipjet.me"])
      mail.from.should eq(["withdrawal@clipjet.me"])
    end

    it "renders the body" do
      mail.body.encoded.should eq("#{withdrawal.user.email}: #{withdrawal.amount}; Paypal email: #{withdrawal.user.paypal_email} ;Payment Day: #{withdrawal.created_at + 30.days}")
    end
  end

end
