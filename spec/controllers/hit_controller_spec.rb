require 'spec_helper'
require 'faker'

describe HitController do
  describe "GET create" do
    let(:site) { create(:site) }
    let(:campaign) { create(:campaign_with_balance) }

    before :each do
      @ip = Faker::Internet.ip_v4_address
      request.stub(:remote_ip).and_return(@ip)
      @user_agent = 'test agent'
      request.stub(:user_agent).and_return(@user_agent)
    end

    context "given a site" do      
      context "given a campaign" do

        it "saves the hit to the DB" do
          expect{ 
            get :create, hit: { site_token: site.token, article_url: "#{site.url}/article", campaign_token: campaign.token}
          }.to change{ Hit.count }.from(0).to(1)
        end

        it "saves hit to the DB if last hit of ip is < 1hour and hit.elapsed_time < minimum and another campaign" do
          campaign2 = create(:campaign_with_balance)
          get :create, hit: { site_token: site.token, article_url: "#{site.url}/article", campaign_token: campaign.token}
          expect{ 
            get :create, hit: { site_token: site.token, article_url: "#{site.url}/article", campaign_token: campaign2.token}
          }.to change{ Hit.count }.by(1)
        end

        # it "send a file as response" do
        #   controller.should_receive(:send_file).with("#{Rails.root}/public/images/vi.png", disposition: 'inline').and_return{controller.render :nothing => true}
        #   get :create, hit: { site_token: site.token, article_url: "#{site.url}/article", campaign_token: campaign.token}
        # end

        it 'runs js as response' do
          Hit.any_instance.stub(:token).and_return('123456')
          get :create, hit: { site_token: site.token, article_url: "#{site.url}/article", campaign_token: campaign.token}, format: :js
          expect(response.body).to have_content "clipjet.setToken('123456')"
        end

        it 'saves remote ip' do
          get :create, hit: { site_token: site.token, article_url: "#{site.url}/article", campaign_token: campaign.token}
          expect(assigns(:hit).ip).to eq @ip
        end

        it 'saves user agent' do
          get :create, hit: { site_token: site.token, article_url: "#{site.url}/article", campaign_token: campaign.token}
          expect(assigns(:hit).user_agent).to eq @user_agent
        end
      end

      context "given no campaign" do
        it "doesn't change DB" do
          expect{ 
            get :create, hit: { site_token: site.token, article_url: "#{site.url}/article"}
          }.to_not change{ Hit.count }                   
        end

        it "sets clipjet.success false" do
          get :create, hit: { site_token: site.token, article_url: "#{site.url}/article"}, format: :js
          expect(response.body).to have_content "clipjet.success = false"
        end
      end

      context "given invalid campaign" do
        it "doesn't change DB" do
          expect{ 
            get :create, hit: { site_token: site.token, article_url: "#{site.url}/article", campaign_token: campaign.token+'10'}
          }.to_not change{ Hit.count }                   
        end

        it "sets clipjet.success false" do
          get :create, hit: { site_token: site.token, article_url: "#{site.url}/article", campaign_token: campaign.token+'10'}, format: :js
          expect(response.body).to have_content "clipjet.success = false"
        end
      end
    end

    context "given invalid site" do
      it "doesn't change DB" do
        expect{ 
          get :create, hit: { site_token: site.token+'10' , article_url: "#{site.url}/article", campaign_token: campaign.token}
        }.to_not change{ Hit.count }                   
      end

      it "sets clipjet.success false" do
        get :create, hit: { site_token: site.token+'10', article_url: "#{site.url}/article", campaign_token: campaign.token}, format: :js
        expect(response.body).to have_content "clipjet.success = false"
      end          
    end

    context "given no site" do
      it "doesn't change DB" do
        expect{ 
          get :create, hit: { article_url: "#{site.url}/article", campaign_token: campaign.token}
        }.to_not change{ Hit.count }                   
      end

      it "sets clipjet.success false" do
        get :create, hit: { article_url: "#{site.url}/article", campaign_token: campaign.token}, format: :js
        expect(response.body).to have_content "clipjet.success = false"
      end        
    end

    context "given repeated request ip in less than 1hour" do

      before :each do
        create(:hit, ip: @ip, site_token: site.token, article_url: "#{site.url}/article", created_at: 1.hour.ago + 10, elapsed_time: 40)
      end

      it "doesn't change DB" do
        expect{ 
          get :create, hit: { site_token: site.token, article_url: "#{site.url}/article2", campaign_token: campaign.token}
        }.to_not change{ Hit.count }                   
      end
 
      it "returns success false" do
        get :create, hit: { site_token: site.token, article_url: "#{site.url}/article2", campaign_token: campaign.token}, format: :js
        expect(response.body).to have_content "clipjet.success = false"
      end     
    end

    context "given repeated request ip in more than 1hour" do

      before :each do
        create(:hit, ip: @ip, article_url: "#{site.url}/article", created_at: 1.hour.ago - 10)
      end

      it "saves the hit to the DB" do
        expect{ 
          get :create, hit: { site_token: site.token, article_url: "#{site.url}/article", campaign_token: campaign.token}
        }.to change{ Hit.count }.by(1)
      end

      # it "send a file as response" do
      #   controller.should_receive(:send_file).with("#{Rails.root}/public/images/vi.png", disposition: 'inline').and_return{controller.render :nothing => true}
      #   get :create, hit: { site_token: site.token, article_url: "#{site.url}/article", campaign_token: campaign.token}
      # end
      
      it 'runs js as response' do
        Hit.any_instance.stub(:token).and_return('123456')
        get :create, hit: { site_token: site.token, article_url: "#{site.url}/article", campaign_token: campaign.token}, format: :js
        expect(response.body).to have_content "clipjet.setToken('123456')"
      end

    end
  end


  describe "GET update" do
    let(:token) { Faker::Lorem.characters(10) }

    before :each do
      @ip = Faker::Internet.ip_v4_address
      request.stub(:remote_ip).and_return(@ip)
    end

    context "given a token" do
      let(:hit) { create(:hit,ip: @ip) }
      context "given same hit ip" do
        it "updates hit on the DB" do
          hit
          expect{
            get :update, token: hit.token, hit: {end_of_video: 0, elapsed_time: 23}
            hit.reload
          }.to change { hit.elapsed_time }.from(nil).to(23)
        end

        it "calls proper callbacks" do
          hit
          Hit.any_instance.should_receive(:charge_pay_update_count)
          get :update, token: hit.token, hit: {end_of_video: 0, elapsed_time: 30}
        end

        it "increase user balance" do
          hit
          Campaign.any_instance.stub(hit_cost: 1)
          Campaign.any_instance.stub(minimum_time: 1)
          expect {
              get :update, token: hit.token, hit: {end_of_video: 0, elapsed_time: 30}
              hit.site_owner_wallet.reload
          }.to change{ hit.site_owner_wallet.balance }.by(0.7)
        end

        it "reduce campaign balance" do
          Campaign.any_instance.stub(minimum_time: 1)
          Campaign.any_instance.stub(hit_cost: 1)
          hit
          expect {
              get :update, token: hit.token, hit: {end_of_video: 0, elapsed_time: 23}
              hit.campaign.reload
          }.to change{ hit.campaign.balance }.by(-1)
        end


        it "send a png as response" do
          hit
          controller.should_receive(:send_file).with("#{Rails.root}/public/images/vi.png", disposition: 'inline').and_return{controller.render :nothing => true}
          get :update, token: hit.token, hit: {end_of_video: 0, elapsed_time: 23}
        end
      end

      context "given different request ip" do
        before(:each) do
          @ip1 = Faker::Internet.ip_v4_address
          request.stub(:remote_ip).and_return(@ip1)
          hit   
        end

        it "doesn't update DB" do
          expect{
            get :update, token: hit.token, hit: {end_of_video: 0, elapsed_time: 23}
          }.to_not change{Hit.last.end_of_video}
        end

        it "returns success false" do
          controller.should_receive(:send_file).with("#{Rails.root}/public/images/vi.png", disposition: 'inline').and_return{controller.render :nothing => true}
          get :update, token: hit.token, hit: {end_of_video: 0, elapsed_time: 23}
        end
      end

    end


    context "given wrong token" do
      let(:hit) {create(:hit, ip: @ip)}
      before(:each) do
        hit   
      end      

      it "hit is nil" do
        get :update, token: token, hit: {end_of_video: 0, elapsed_time: 23}
        expect(assigns(:hit)).to be_nil
      end

      it "returns success false" do
        controller.should_receive(:send_file).with("#{Rails.root}/public/images/vi.png", disposition: 'inline').and_return{controller.render :nothing => true}          
        get :update, token: token, hit: {end_of_video: 0, elapsed_time: 23}
      end
      
    end

  end

end