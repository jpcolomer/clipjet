require 'spec_helper'
require 'faker'

describe InfluencerViewsController do
  describe 'POST create' do

    before :each do
      @ip = Faker::Internet.ip_v4_address
      request.stub(:remote_ip).and_return(@ip)
      @user_agent = 'test agent'
      request.stub(:user_agent).and_return(@user_agent)
      @referer = 'test referer'
      request.stub(:referer).and_return(@referer)
    end

    let(:iv) { create(:influencer_video) }

    it "saves influencer view to the db" do
      expect {
        post :create, influencer_view: {influencer_video_token: iv.token}
      }.to change(InfluencerView, :count).by(1)
    end

    it "doesn't save influencer view to the db" do
      expect {
        post :create, influencer_view: {influencer_video_token: 'abcd'}
      }.to_not change(InfluencerView, :count)
    end


    it 'returns json for valid influencer view' do
      InfluencerView.any_instance.stub(:token).and_return('123456')
      post :create, influencer_view: {influencer_video_token: iv.token}
      expect(response.body).to have_content ({success: true, token: '123456'}.to_json)
    end

    it 'returns json for another valid influencer view' do
      InfluencerView.any_instance.stub(:token).and_return('1234567')
      post :create, influencer_view: {influencer_video_token: iv.token}
      expect(response.body).to have_content ({success: true, token: '1234567'}.to_json)
    end

    it 'returns json for unsuccesful post, by repeated ip for campaign' do
      iv2 = create(:influencer_video, campaign: iv.campaign)
      create(:influencer_view, ip: @ip, elapsed_time: 31, influencer_video: iv2)
      InfluencerView.any_instance.stub(:token).and_return('1234567')
      post :create, influencer_view: {influencer_video_token: iv.token}
      expect(response.body).to have_content ({success: false}.to_json)
    end

    it 'returns json for unsuccesful post, by non existes influencer video' do
      post :create, influencer_view: {influencer_video_token: 'abcd'}
      expect(response.body).to have_content ({success: false}.to_json)
    end

    it 'saves ip' do
      post :create, influencer_view: {influencer_video_token: 'abcd'}
      expect(assigns(:influencer_view).ip).to eq @ip
    end


    it 'saves user_agent' do
      post :create, influencer_view: {influencer_video_token: 'abcd'}
      expect(assigns(:influencer_view).user_agent).to eq @user_agent
    end

    it 'saves referer' do
      post :create, influencer_view: {influencer_video_token: 'abcd'}
      expect(assigns(:influencer_view).referer).to eq @referer
    end

  end

  describe 'PUT update' do
    let(:token) { Faker::Lorem.characters(10) }
    before :each do
      @ip = Faker::Internet.ip_v4_address
      request.stub(:remote_ip).and_return(@ip)
    end

    let(:influencer_view) { create(:influencer_view,ip: @ip) }
    
    context "given same influencer_view ip" do
      it "updates influencer_view on the DB" do          
        influencer_view
        expect{
          put :update, id: influencer_view.token, influencer_view: {end_of_video: 0, elapsed_time: 23}
          influencer_view.reload
        }.to change { influencer_view.elapsed_time }.from(nil).to(23)
      end

      it "updates influencer_view on the DB" do          
        influencer_view
        expect{
          put :update, id: influencer_view.token, influencer_view: {end_of_video: 0, elapsed_time: 23}
          influencer_view.reload
        }.to change { influencer_view.end_of_video }.from(nil).to(false)
      end

      it "updates influencer_view on the DB" do          
        influencer_view
        expect{
          put :update, id: influencer_view.token, influencer_view: {end_of_video: 1, elapsed_time: 100}
          influencer_view.reload
        }.to change { influencer_view.end_of_video }.from(nil).to(true)
      end


      it "calls proper callbacks" do
        influencer_view
        InfluencerView.any_instance.should_receive(:charge_pay_update_count)
        put :update, id: influencer_view.token, influencer_view: {end_of_video: 0, elapsed_time: 30}
      end

    end

    context "given different ip" do
      it "doesn't update influencer_view on the DB" do  
        ip = Faker::Internet.ip_v4_address
        request.stub(:remote_ip).and_return(ip)        
        influencer_view
        expect{
          put :update, id: influencer_view.token, influencer_view: {end_of_video: 0, elapsed_time: 23}
          influencer_view.reload
        }.to_not change { influencer_view.elapsed_time }
      end  
    end
  end
end