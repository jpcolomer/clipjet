require 'spec_helper'

describe VideoController do
  describe "GET #show" do
    let(:site) { create(:site) }
    let(:first_category_id) { Category.first.id }

    before :each do
      @ip = Faker::Internet.ip_v4_address
      request.stub(:remote_ip).and_return(@ip)
    end

    context 'with valid params' do
      let(:campaign) { create(:campaign_with_balance, category_id: first_category_id, language: 'en') }

      it "creates impression" do
        expect {
          get :show, category_id: campaign.category_id, site_token: site.token, language: 'en', format: :js
        }.to change(Impression, :count).by(1)
      end

      it 'saves remote ip' do
        get :show, category_id: campaign.category_id, site_token: site.token, language: 'en', format: :js
        expect(assigns(:impression).ip).to eq @ip
      end


      it 'responds with js' do
        get :show, category_id: campaign.category_id, site_token: site.token, language: 'en', format: :js
        expect(response.header['Content-Type']).to include 'text/javascript'
      end

      context "given a category and language" do
        it "doesn't find video if campaign balance is 0" do
          create(:campaign, balance: 0)
          get :show, category_id: first_category_id+1, language: 'en', format: :js
          expect(response.body).to have_content "clipjet.success = false"
        end
        
        it "evaluates on JS function, json with a direct campaign" do
          message = {video_url: campaign.video_url, campaign_token: campaign.token}
          get :show, category_id: campaign.category_id, site_token: site.token, language: 'en', format: :js
          expect(response.body).to have_content "clipjet.insertVideo(#{message.to_json})"
        end
        
        it "evaluates on JS function, json of similar category campaign when has no direct campaign" do
          create(:campaign, category_id: first_category_id+2)
          message = {video_url: campaign.video_url, campaign_token: campaign.token}
          get :show, category_id: first_category_id+1,site_token: site.token, language: 'en', format: :js
          expect(response.body).to have_content "clipjet.insertVideo(#{message.to_json})"
        end

        it "evaluates on JS function, json of random campaign when has no direct or indirect campaign" do
          message = {video_url: campaign.video_url, campaign_token: campaign.token}
          get :show, category_id: first_category_id+2, site_token: site.token, language: 'en', format: :js
          expect(response.body).to have_content "clipjet.insertVideo(#{message.to_json})"
        end

      end
    end


    context "with repeated valid params" do
      it "calls find_video" do
        Campaign.should_receive(:find_video).with(hash_including(ip: @ip, language: 'en', category_id: 1))
        get :show, category_id: 1, site_token: site.token, language: 'en', format: :js
      end

      it "shows a different video with no hits if one is already seen for certain ip" do
        campaign1 = create(:campaign_with_balance, category_id: first_category_id, language: 'en')
        create(:campaign_with_balance, category_id: first_category_id+1, language: 'en')
        campaign2 = create(:campaign_with_balance, category_id: first_category_id, language: 'en')
        hit = create(:hit, campaign: campaign1, ip: @ip)
        get :show, category_id: first_category_id, site_token: site.token, language: 'en', format: :js
        expect(assigns(:campaign)).to eq campaign2
      end

      it "shows a different video with no hits from ip if one is already seen for certain ip" do
        campaign1 = create(:campaign_with_balance, category_id: first_category_id, language: 'en')
        create(:campaign_with_balance, category_id: first_category_id+1, language: 'en')
        campaign2 = create(:campaign_with_balance, category_id: first_category_id, language: 'en')
        hit = create(:hit, campaign: campaign1, ip: @ip)
        create(:hit, campaign: campaign2)
        get :show, category_id: first_category_id, site_token: site.token, language: 'en', format: :js
        expect(assigns(:campaign)).to eq campaign2
      end
    end

    context "with invalid params" do
      let(:campaign) { create(:campaign_with_balance, category_id: Category.first.id) }

      it "evaluates on JS function, json of random campaign" do
        message = {video_url: campaign.video_url, campaign_token: campaign.token}
        get :show, category_id: 50000000, site_token: site.token, language: 'en', format: :js
        expect(response.body).to have_content "clipjet.insertVideo(#{message.to_json})"
      end

      it "evaluates on JS function, json of random campaign with no category_id param" do
        message = {video_url: campaign.video_url, campaign_token: campaign.token}
        get :show, site_token: site.token, language: 'en', format: :js
        expect(response.body).to have_content "clipjet.insertVideo(#{message.to_json})"
      end

      it "evaluates on JS function, json of campaign with same category with no language param" do
        create(:campaign, category_id: first_category_id+2)
        message = {video_url: campaign.video_url, campaign_token: campaign.token}
        get :show, category_id: first_category_id,site_token: site.token, format: :js
        expect(response.body).to have_content "clipjet.insertVideo(#{message.to_json})"
      end


      it "evaluates on JS function, json of random campaign with no language param and category" do
        message = {video_url: campaign.video_url, campaign_token: campaign.token}
        get :show, site_token: site.token, format: :js
        expect(response.body).to have_content "clipjet.insertVideo(#{message.to_json})"
      end


      it "sets clipjet.success false if no site" do
        get :show, category_id: campaign.category_id, site_token: site.token + "a", language: 'en', format: :js
        expect(response.body).to have_content "clipjet.success = false"
      end

      it "sets clipjet.success false if no site_token" do
        get :show, category_id: campaign.category_id, language: 'en', format: :js
        expect(response.body).to have_content "clipjet.success = false"
      end

    end

  end
end