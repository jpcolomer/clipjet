require 'spec_helper'

describe PaymentNotificationsController do
  describe 'POST create' do
    context "given good secret" do
      it "saves payment notification to DB  corresponding secret" do
        campaign = create(:campaign)
        expect {
          post :create, secret: campaign.secret ,txn_id: 'asdf', custom: campaign.id, payment_status: 'Completed', mc_gross: 80
        }.to change(PaymentNotification, :count).by(1)
      end

      it "increases campaign balance by 20 if mc_gross 20" do
        campaign = create(:campaign)
        expect {
          post :create, secret: campaign.secret, txn_id: 'asdf', custom: campaign.id, payment_status: 'Completed', mc_gross: 20
          campaign.reload
        }.to change{ campaign.balance }.by(20)        
      end

      it "increases campaign balance by 30 if mc_gross 30" do
        campaign = create(:campaign)
        expect {
          post :create, secret: campaign.secret, txn_id: 'asdf', custom: campaign.id, payment_status: 'Completed', mc_gross: 30
          campaign.reload
        }.to change{ campaign.balance }.by(30)  
      end

      it "increases campaign balance by 300 if mc_gross 300" do
        campaign = create(:campaign)
        expect {
          post :create, secret: campaign.secret, txn_id: 'asdf', custom: campaign.id, payment_status: 'Completed', mc_gross: 300
          campaign.reload
        }.to change{ campaign.balance }.by(300)
      end


      it "saves hit_cost" do
        campaign = create(:campaign, hit_cost: 0)
        expect {
          post :create, secret: campaign.secret, hit_cost: 0.09,txn_id: 'asdf', custom: campaign.id, payment_status: 'Completed', mc_gross: 20
          campaign.reload
        }.to change{ campaign.hit_cost }.from(0.08).to(0.09)
      end
    end

    context "given wrong mc_gross" do
      it "doesn't increase campaign balance secret" do
        campaign = create(:campaign)
        expect {
          post :create, secret: campaign.secret ,txn_id: 'asdf', custom: campaign.id, payment_status: 'Completed', mc_gross: 10
          campaign.reload
        }.to_not change{ campaign.balance }
      end
    end


    context "given wrong secret" do
      it "doesn't increase campaign balance secret" do
        campaign = create(:campaign)
        expect {
          post :create, secret: 'asdfg' ,txn_id: 'asdf', custom: campaign.id, payment_status: 'Completed'
          campaign.reload
        }.to_not change{ campaign.balance }
      end
    end
  end
end